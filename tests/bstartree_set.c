#define BSTAR
#include "btree_test.h"

SIMPLE_TEST(bstartree_set)
{
	return bstartree_set_test();
}

RANDOM_TEST(bstartree_set_random, random_seed, 2)
{
	return bstartree_set_random_test(random_seed);
}

SIMPLE_TEST(bstartree_set_allocated_strings)
{
	return bstartree_set_allocated_strings_test();
}

SIMPLE_TEST(bstartree_set_alignment)
{
	return bstartree_set_alignment_test();
}

SIMPLE_TEST(bstartree_set_bulk_loading)
{
	return bstartree_set_bulk_loading_test();
}
