#define MAP
#include "hashtable.h"

RANDOM_TEST(hashmap_random, random_seed, 64)
{
	return random_test(random_seed);
}

SIMPLE_TEST(hashmap_insert)
{
	return insert_test();
}

SIMPLE_TEST(hashmap_lookup)
{
	return lookup_test();
}

SIMPLE_TEST(hashmap_remove)
{
	return remove_test();
}

SIMPLE_TEST(hashmap_insert_unchecked)
{
	return insert_unchecked_test();
}

SIMPLE_TEST(hashmap_iterator)
{
	return iterator_test();
}

SIMPLE_TEST(hashmap_init_destroy_clear)
{
	return init_destroy_clear_test();
}

SIMPLE_TEST(hashmap_resize)
{
	return resize_test();
}

SIMPLE_TEST(hashmap_destructors)
{
	return destructors_test();
}

NEGATIVE_SIMPLE_TEST(hashmap_fortify1)
{
	return fortify1_test();
}

NEGATIVE_SIMPLE_TEST(hashmap_fortify2)
{
	return fortify2_test();
}

NEGATIVE_SIMPLE_TEST(hashmap_fortify3)
{
	return fortify3_test();
}

NEGATIVE_SIMPLE_TEST(hashmap_fortify4)
{
	return fortify4_test();
}

NEGATIVE_SIMPLE_TEST(hashmap_fortify5)
{
	return fortify5_test();
}

NEGATIVE_SIMPLE_TEST(hashmap_fortify6)
{
	return fortify6_test();
}

NEGATIVE_SIMPLE_TEST(hashmap_fortify7)
{
	return fortify7_test();
}
