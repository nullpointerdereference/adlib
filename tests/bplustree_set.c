#define BPLUS
#include "btree_test.h"

SIMPLE_TEST(bplustree_set)
{
	return bplustree_set_test();
}

RANDOM_TEST(bplustree_set_random, random_seed, 2)
{
	return bplustree_set_random_test(random_seed);
}

SIMPLE_TEST(bplustree_set_allocated_strings)
{
	return bplustree_set_allocated_strings_test();
}

SIMPLE_TEST(bplustree_set_alignment)
{
	return bplustree_set_alignment_test();
}

SIMPLE_TEST(bplustree_set_bulk_loading)
{
	return bplustree_set_bulk_loading_test();
}
