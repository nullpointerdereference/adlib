#pragma once

#include <inttypes.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>

#include "bplusstartree.h"
#include "bplustree.h"
#include "bstartree.h"
#include "btree.h"
#include "compiler.h"
#include "hash.h"
#include "hashset.h"
#include "random.h"
#include "testing.h"
#include "utils.h"

// #define MAP
// #define BPLUS
// #define BSTAR

#ifdef MAP
# if defined(BPLUS) && defined(BSTAR)
DEFINE_BPLUSSTARTREE_MAP(btree, char *, uint32_t, NULL, NULL, 128, strcmp(a, b));
# elif defined(BPLUS)
DEFINE_BPLUSTREE_MAP(btree, char *, uint32_t, NULL, NULL, 128, strcmp(a, b));
# elif defined(BSTAR)
DEFINE_BSTARTREE_MAP(btree, char *, uint32_t, NULL, NULL, 128, strcmp(a, b));
# else
DEFINE_BTREE_MAP(btree, char *, uint32_t, NULL, NULL, 128, strcmp(a, b));
# endif
#else
# if defined(BPLUS) && defined(BSTAR)
DEFINE_BPLUSSTARTREE_SET(btree, int64_t, NULL, 127, (a < b) ? -1 : (a > b));
# elif defined(BPLUS)
DEFINE_BPLUSTREE_SET(btree, int64_t, NULL, 127, (a < b) ? -1 : (a > b));
# elif defined(BSTAR)
DEFINE_BSTARTREE_SET(btree, int64_t, NULL, 127, (a < b) ? -1 : (a > b));
# else
DEFINE_BTREE_SET(btree, int64_t, NULL, 127, (a < b) ? -1 : (a > b));
# endif
#endif

#if defined(BPLUS) && defined(BSTAR)
typedef struct _bpstartree_node node_type;
typedef struct bplusstartree_info info_type;
typedef struct _bpstartree tree_type;
#define INITIALIZER BPLUSSTARTREE_EMPTY
#define GET_CHILD _bpstartree_debug_node_get_child
#define GET_KEY _bpstartree_debug_node_key
#define ITER_FIND_KEY              BPLUSSTARTREE_ITER_FIND_KEY
#define ITER_LOWER_BOUND_INCLUSIVE BPLUSSTARTREE_ITER_LOWER_BOUND_INCLUSIVE
#define ITER_LOWER_BOUND_EXCLUSIVE BPLUSSTARTREE_ITER_LOWER_BOUND_EXCLUSIVE
#define ITER_UPPER_BOUND_INCLUSIVE BPLUSSTARTREE_ITER_UPPER_BOUND_INCLUSIVE
#define ITER_UPPER_BOUND_EXCLUSIVE BPLUSSTARTREE_ITER_UPPER_BOUND_EXCLUSIVE
#elif defined(BPLUS)
typedef struct _bplustree_node node_type;
typedef struct bplustree_info info_type;
typedef struct _bplustree tree_type;
#define INITIALIZER BPLUSTREE_EMPTY
#define GET_CHILD _bplustree_debug_node_get_child
#define GET_KEY _bplustree_debug_node_key
#define ITER_FIND_KEY              BPLUSTREE_ITER_FIND_KEY
#define ITER_LOWER_BOUND_INCLUSIVE BPLUSTREE_ITER_LOWER_BOUND_INCLUSIVE
#define ITER_LOWER_BOUND_EXCLUSIVE BPLUSTREE_ITER_LOWER_BOUND_EXCLUSIVE
#define ITER_UPPER_BOUND_INCLUSIVE BPLUSTREE_ITER_UPPER_BOUND_INCLUSIVE
#define ITER_UPPER_BOUND_EXCLUSIVE BPLUSTREE_ITER_UPPER_BOUND_EXCLUSIVE
#elif defined(BSTAR)
typedef struct _bstartree_node node_type;
typedef struct bstartree_info info_type;
typedef struct _bstartree tree_type;
#define INITIALIZER BSTARTREE_EMPTY
#define GET_CHILD _bstartree_debug_node_get_child
#define GET_KEY _bstartree_debug_node_item
#define ITER_FIND_KEY              BSTARTREE_ITER_FIND_KEY
#define ITER_LOWER_BOUND_INCLUSIVE BSTARTREE_ITER_LOWER_BOUND_INCLUSIVE
#define ITER_LOWER_BOUND_EXCLUSIVE BSTARTREE_ITER_LOWER_BOUND_EXCLUSIVE
#define ITER_UPPER_BOUND_INCLUSIVE BSTARTREE_ITER_UPPER_BOUND_INCLUSIVE
#define ITER_UPPER_BOUND_EXCLUSIVE BSTARTREE_ITER_UPPER_BOUND_EXCLUSIVE
#else
typedef struct _btree_node node_type;
typedef struct btree_info info_type;
typedef struct _btree tree_type;
#define INITIALIZER BTREE_EMPTY
#define GET_CHILD _btree_debug_node_get_child
#define GET_KEY _btree_debug_node_item
#define ITER_FIND_KEY              BTREE_ITER_FIND_KEY
#define ITER_LOWER_BOUND_INCLUSIVE BTREE_ITER_LOWER_BOUND_INCLUSIVE
#define ITER_LOWER_BOUND_EXCLUSIVE BTREE_ITER_LOWER_BOUND_EXCLUSIVE
#define ITER_UPPER_BOUND_INCLUSIVE BTREE_ITER_UPPER_BOUND_INCLUSIVE
#define ITER_UPPER_BOUND_EXCLUSIVE BTREE_ITER_UPPER_BOUND_EXCLUSIVE
#endif

static bool btree_check_node(node_type *node, void *prev_key, unsigned int height, const info_type *info);

static bool btree_check_children(node_type *node, void *prev_key, unsigned int height, const info_type *info)
{
	if (height == 0) {
		return true;
	}

	for (unsigned int i = 0; i < node->num_items + 1u; i++) {
		if (!btree_check_node(GET_CHILD(node, i, info), prev_key, height - 1, info)) {
			return false;
		}
		if (i != node->num_items + 1u) {
			prev_key = GET_KEY(node, i, info);
		}
	}
	return true;
}

static bool btree_check_node_sorted(node_type *node, void *prev_key, bool leaf, const info_type *info)
{
	(void)leaf;
	void *first_key = GET_KEY(node, 0, info);
	if (prev_key) {
		int cmp = info->cmp(prev_key, first_key);
		cmp = cmp < 0 ? -1 : (cmp > 0);
		int expected = -1;
#ifdef BPLUS
		if (leaf) {
			expected = 0;
		}
#endif
		CHECK(cmp == expected);
	}
	prev_key = first_key;
	for (unsigned int i = 1; i < node->num_items; i++) {
		void *key = GET_KEY(node, i, info);
		CHECK(info->cmp(prev_key, key) < 0);
		prev_key = key;
	}
	return true;
}

static bool btree_check_node(node_type *node, void *prev_key, unsigned int height, const info_type *info)
{
	CHECK(node->num_items >= info->min_items && node->num_items <= info->max_items);
	return btree_check_node_sorted(node, prev_key, height == 0, info) &&
	       btree_check_children(node, prev_key, height, info);
}

static _attr_nodiscard bool btree_check(const tree_type *btree, const info_type *info)
{
	node_type *root = btree->root;
	if (btree->height == 0) {
		CHECK(!root);
		return true;
	}
#ifdef BSTAR
	CHECK(root->num_items >= 1 && root->num_items <= 2 * info->max_items);
#else
	CHECK(root->num_items >= 1 && root->num_items <= info->max_items);
#endif
	return btree_check_children(root, NULL, btree->height - 1, info);
}

static void btree_node_debug_print(node_type *node, FILE *file, char key_type, unsigned int *idx,
				   unsigned int height, const info_type *info)
{
	unsigned int node_idx = (*idx)++;
	fprintf(file, "%u [label=\"", node_idx);

	if (node->num_items == 0) {
		fprintf(file, "(empty)");
	}
	for (unsigned int i = 0; i < node->num_items; i++) {
		if (i != 0) {
			fprintf(file, ", ");
		}
		void *key = GET_KEY(node, i, info);
		switch (key_type) {
		case 'i':
			fprintf(file, "%d", *(int *)key);
			break;
		case 's':
			fprintf(file, "%s", *(const char **)key);
			break;
		default:
			assert(false);
		}
	}
	fprintf(file, "\"];\n");

	if (height == 0) {
		return;
	}
	for (unsigned int i = 0; i < node->num_items + 1u; i++) {
		fprintf(file, "%u -> %u;\n", node_idx, *idx);
		btree_node_debug_print(GET_CHILD(node, i, info), file, key_type, idx, height - 1, info);
	}

}

static _attr_unused void btree_debug_print(const tree_type *btree, const char *filename, char key_type,
					   const info_type *info)
{
	node_type *root = btree->root;
	FILE *file = fopen(filename, "w");
#if defined(BPLUS) && defined(BSTAR)
	const char *name = "bplusstartree";
#elif defined(BPLUS)
	const char *name = "bplustree";
#elif defined(BSTAR)
	const char *name = "bstartree";
#else
	const char *name = "btree";
#endif
	fprintf(file, "digraph %s {\n", name);
	if (btree->height != 0) {
		unsigned int idx = 0;
		btree_node_debug_print(root, file, key_type, &idx, btree->height - 1, info);
	}
	fprintf(file, "}");
	fclose(file);
}

#ifdef MAP
#define __KEY_SIZE 20
#define KEY_SIZE (__KEY_SIZE + 1)
#define KEY_FORMAT __KEY_FORMAT(__KEY_SIZE)
#define __KEY_FORMAT(S) ___KEY_FORMAT(S)
#define ___KEY_FORMAT(S) "%0" #S "zu"

static btree_key_t *create_keys(size_t num_keys)
{
	btree_key_t *keys = malloc(num_keys * sizeof(keys[0]));
	for (size_t i = 0; i < num_keys; i++) {
		keys[i] = malloc(KEY_SIZE);
		snprintf(keys[i], KEY_SIZE, KEY_FORMAT, i);
	}
	return keys;
}

static void destroy_keys(btree_key_t *keys, size_t num_keys)
{
	for (size_t i = 0; i < num_keys; i++) {
		free(keys[i]);
	}
	free(keys);
}

static uint32_t get_hash(const btree_key_t key)
{
	return mx3_hash(key, KEY_SIZE, 0);
}

static btree_key_t get_key(btree_key_t *keys, size_t x)
{
	return keys[x];
}
#else
static btree_key_t *create_keys(size_t num_keys)
{
	(void)num_keys;
	return NULL;
}
static void destroy_keys(btree_key_t *keys, size_t num_keys)
{
	(void)keys;
	(void)num_keys;
}
static uint32_t get_hash(btree_key_t key)
{
	return (uint32_t)key;
}
static btree_key_t get_key(btree_key_t *keys, size_t x)
{
	(void)keys;
	return (btree_key_t)x;
}
#endif

DEFINE_HASHSET(btable, btree_key_t, NULL, get_hash, a == b);

// TODO split this up into multiple test functions?
// (would probably have to make keys global with pthread_once)

#ifdef MAP
# if defined(BPLUS) && defined(BSTAR)
static bool bplusstartree_map_random_test(uint64_t random)
# elif defined(BPLUS)
static bool bplustree_map_random_test(uint64_t random)
# elif defined(BSTAR)
static bool bstartree_map_random_test(uint64_t random)
# else
static bool btree_map_random_test(uint64_t random)
# endif
#else
# if defined(BPLUS) && defined(BSTAR)
static bool bplusstartree_set_random_test(uint64_t random)
# elif defined(BPLUS)
static bool bplustree_set_random_test(uint64_t random)
# elif defined(BSTAR)
static bool bstartree_set_random_test(uint64_t random)
# else
static bool btree_set_random_test(uint64_t random)
# endif
#endif
{
#define CHECK_TREE() CHECK(btree_check(&btree._impl, &btree_info))

	struct random_state rng;
	random_state_init(&rng, random);

	const size_t N = 1 << 13;
	const size_t LIMIT = N << 2;

	size_t num_keys = 2 * (N > LIMIT ? N : LIMIT);
	btree_key_t *keys = create_keys(num_keys);

	struct btree btree = INITIALIZER;
	btree_init(&btree);

	struct btable btable;
	btable_init_with_capacity(&btable, N);

	for (size_t i = 0; i < N; i++) {
		size_t x = random_next_u64(&rng) % LIMIT;
		btree_key_t key = get_key(keys, x);
		bool inserted = btable_insert(&btable, key);
#ifdef MAP
		bool inserted2 = btree_insert(&btree, key, x);
#else
		bool inserted2 = btree_insert(&btree, key);
#endif
		CHECK(inserted == inserted2);
		CHECK_TREE();
		CHECK(btree_find(&btree, key));
	}
	for (btable_iter_t iter = btable_iter_start(&btable);
	     btable_iter_advance(&iter);) {
		CHECK(btree_find(&btree, *btable_iter_key(&iter)));
	}
	{
		btree_iter_t iter;
#ifdef MAP
		btree_key_t prev_key, key;
		btree_value_t *value = btree_iter_start_leftmost(&iter, &btree, &key);
		CHECK(value && strtoumax(key, NULL, 10) == *value);
		CHECK(btable_lookup(&btable, key));
#else
		const btree_key_t *prev_key, *key;
		key = btree_iter_start_leftmost(&iter, &btree);
		CHECK(key);
		CHECK(btable_lookup(&btable, *key));
#endif
		prev_key = key;
#ifdef MAP
		while ((value = btree_iter_next(&iter, &key))) {
			CHECK(strtoumax(key, NULL, 10) == *value);
			CHECK(btable_lookup(&btable, key));
			CHECK(btree_info.cmp(&prev_key, &key) < 0);
			prev_key = key;
		}
#else
		while ((key = btree_iter_next(&iter))) {
			CHECK(btable_lookup(&btable, *key));
			CHECK(btree_info.cmp(prev_key, key) < 0);
			prev_key = key;
		}
#endif
	}
	for (size_t i = LIMIT; i < LIMIT + 1000; i++) {
		CHECK(!btree_find(&btree, get_key(keys, i)));
	}
	for (size_t i = 0; i < N; i++) {
		btree_key_t key = get_key(keys, random_next_u64(&rng) % LIMIT);
		bool exists = btable_remove(&btable, key, NULL);
#ifdef MAP
		btree_key_t k;
		btree_value_t v;
		bool removed = btree_delete(&btree, key, &k, &v);
		CHECK(!removed || strtoumax(k, NULL, 10) == v);
#else
		btree_key_t k;
		bool removed = btree_delete(&btree, key, &k);
#endif
		CHECK(exists == removed);
		CHECK(!removed || (k == key));
		CHECK_TREE();
		CHECK(!btree_find(&btree, key));
	}

	for (btable_iter_t iter = btable_iter_start(&btable);
	     btable_iter_advance(&iter);) {
#ifdef MAP
		btree_key_t key;
		btree_value_t value;
		bool removed = btree_delete(&btree, *btable_iter_key(&iter), &key, &value);
		CHECK(removed && strtoumax(key, NULL, 10) == value);
#else
		btree_key_t key;
		bool removed = btree_delete(&btree, *btable_iter_key(&iter), &key);
		CHECK(removed);
#endif
		CHECK(key == *btable_iter_key(&iter));
		CHECK_TREE();
	}
	btable_clear(&btable);

	CHECK(btree._impl.height == 0); // TODO expose height?
	CHECK_TREE();

	for (size_t i = 0; i < N; i++) {
		btree_key_t key = get_key(keys, i);
#ifdef MAP
		btree_insert_sequential(&btree, key, i);
#else
		btree_insert_sequential(&btree, key);
#endif
		CHECK_TREE();
		CHECK(btree_find(&btree, key));
	}

	for (size_t i = 0; i < N; i++) {
		btree_key_t key = get_key(keys, i);
#ifdef MAP
		bool inserted = btree_set(&btree, key, i + 1);
#else
		bool inserted = btree_set(&btree, key);
#endif
		CHECK(!inserted);
		CHECK_TREE();
		CHECK(btree_find(&btree, key));
	}

	{
		btree_iter_t iter;
#ifdef MAP
		btree_key_t key;
		btree_value_t *value = btree_iter_start_leftmost(&iter, &btree, &key);
		for (size_t i = 0; i < N; i++) {
			CHECK(value);
			CHECK(*value == i + 1);
			*value = i + 2;
			CHECK(key == get_key(keys, i));
			value = btree_iter_next(&iter, &key);
		}
		CHECK(!value);
		CHECK(!btree_iter_next(&iter, &key));
#else
		const btree_key_t *key = btree_iter_start_leftmost(&iter, &btree);
		for (size_t i = 0; i < N; i++) {
			CHECK(key);
			CHECK(*key == get_key(keys, i));
			key = btree_iter_next(&iter);
		}
		CHECK(!key);
		CHECK(!btree_iter_next(&iter));
#endif
#ifdef MAP
		value = btree_iter_start_rightmost(&iter, &btree, &key);
		for (size_t i = N; i-- > 0;) {
			CHECK(value);
			CHECK(*value == i + 2);
			CHECK(key == get_key(keys, i));
			value = btree_iter_prev(&iter, &key);
		}
		CHECK(!value);
		CHECK(!btree_iter_prev(&iter, &key));
#else
		key = btree_iter_start_rightmost(&iter, &btree);
		for (size_t i = N; i-- > 0;) {
			CHECK(key);
			CHECK(*key == get_key(keys, i));
			key = btree_iter_prev(&iter);
		}
		CHECK(!key);
		CHECK(!btree_iter_prev(&iter));
#endif
		size_t i = 0;
#ifdef MAP
		for (btree_value_t *value = btree_iter_start_leftmost(&iter, &btree, &key);
		     value;
		     value = btree_iter_next(&iter, &key)) {
			CHECK(i < N);
			CHECK(*value == i + 2);
			CHECK(key == get_key(keys, i));
			i++;
		}
#else
		for (const btree_key_t *key = btree_iter_start_leftmost(&iter, &btree);
		     key;
		     key = btree_iter_next(&iter)) {
			CHECK(i < N);
			CHECK(*key == get_key(keys, i));
			i++;
		}
#endif
		CHECK(i == N);

#ifdef MAP
		for (size_t i = 0; i < N; i++) {
			value = btree_iter_start_at(&iter, &btree, get_key(keys, i), &key, ITER_FIND_KEY);
			CHECK(key == get_key(keys, i));
			CHECK(*value == i + 2);
			value = btree_iter_next(&iter, &key);
			if (i == N - 1) {
				CHECK(!value);
			} else {
				CHECK(key == get_key(keys, i + 1));
				CHECK(*value == i + 3);
			}
		}
#else
		for (size_t i = 0; i < N; i++) {
			key = btree_iter_start_at(&iter, &btree, get_key(keys, i), ITER_FIND_KEY);
			CHECK(*key == get_key(keys, i));
			key = btree_iter_next(&iter);
			if (i == N - 1) {
				CHECK(!key);
			} else {
				CHECK(*key == get_key(keys, i + 1));
			}
		}
#endif

#ifdef MAP
		value = btree_iter_start_at(&iter, &btree, get_key(keys, N / 2), &key, ITER_FIND_KEY);
		CHECK(key == get_key(keys, N / 2));
		value = btree_iter_next(&iter, &key);
		CHECK(key == get_key(keys, N / 2 + 1));
		value = btree_iter_prev(&iter, &key);
		CHECK(key == get_key(keys, N / 2));
		value = btree_iter_prev(&iter, &key);
		CHECK(key == get_key(keys, N / 2 - 1));
		value = btree_iter_next(&iter, &key);
		CHECK(key == get_key(keys, N / 2));
#else
		key = btree_iter_start_at(&iter, &btree, get_key(keys, N / 2), ITER_FIND_KEY);
		CHECK(*key == get_key(keys, N / 2));
		key = btree_iter_next(&iter);
		CHECK(*key == get_key(keys, N / 2 + 1));
		key = btree_iter_prev(&iter);
		CHECK(*key == get_key(keys, N / 2));
		key = btree_iter_prev(&iter);
		CHECK(*key == get_key(keys, N / 2 - 1));
		key = btree_iter_next(&iter);
		CHECK(*key == get_key(keys, N / 2));
#endif

#ifdef MAP
		for (size_t i = 0; i < N; i++) {
			value = btree_iter_start_at(&iter, &btree, get_key(keys, i), &key, ITER_LOWER_BOUND_INCLUSIVE);
			CHECK(value && key == get_key(keys, i) && *value == i + 2);
			value = btree_iter_start_at(&iter, &btree, get_key(keys, i), &key, ITER_LOWER_BOUND_EXCLUSIVE);
			CHECK(i == N - 1 ? !value : value && key == get_key(keys, i + 1) && *value == i + 3);
			bool deleted = btree_delete(&btree, get_key(keys, i), NULL, NULL);
			CHECK(deleted);
			CHECK(!btree_iter_start_at(&iter, &btree, get_key(keys, i), NULL, ITER_FIND_KEY));
			value = btree_iter_start_at(&iter, &btree, get_key(keys, i), &key, ITER_LOWER_BOUND_INCLUSIVE);
			CHECK(i == N - 1 ? !value : value && key == get_key(keys, i + 1) && *value == i + 3);
			value = btree_iter_start_at(&iter, &btree, get_key(keys, i), &key, ITER_LOWER_BOUND_EXCLUSIVE);
			CHECK(i == N - 1 ? !value : value && key == get_key(keys, i + 1) && *value == i + 3);
			btree_insert(&btree, get_key(keys, i), i + 2);
		}

		for (size_t i = 0; i < N; i++) {
			value = btree_iter_start_at(&iter, &btree, get_key(keys, i), &key, ITER_UPPER_BOUND_INCLUSIVE);
			CHECK(value && key == get_key(keys, i) && *value == i + 2);
			value = btree_iter_start_at(&iter, &btree, get_key(keys, i), &key, ITER_UPPER_BOUND_EXCLUSIVE);
			CHECK(i == 0 ? !value : value && key == get_key(keys, i - 1) && *value == i + 1);
			bool deleted = btree_delete(&btree, get_key(keys, i), NULL, NULL);
			CHECK(deleted);
			CHECK(!btree_iter_start_at(&iter, &btree, get_key(keys, i), NULL, ITER_FIND_KEY));
			value = btree_iter_start_at(&iter, &btree, get_key(keys, i), &key, ITER_UPPER_BOUND_INCLUSIVE);
			CHECK(i == 0 ? !value : value && key == get_key(keys, i - 1) && *value == i + 1);
			value = btree_iter_start_at(&iter, &btree, get_key(keys, i), &key, ITER_UPPER_BOUND_EXCLUSIVE);
			CHECK(i == 0 ? !value : value && key == get_key(keys, i - 1) && *value == i + 1);
			btree_insert(&btree, get_key(keys, i), i + 2);
		}
#else
		for (size_t i = 0; i < N; i++) {
			key = btree_iter_start_at(&iter, &btree, get_key(keys, i), ITER_LOWER_BOUND_INCLUSIVE);
			CHECK(*key == get_key(keys, i));
			key = btree_iter_start_at(&iter, &btree, get_key(keys, i), ITER_LOWER_BOUND_EXCLUSIVE);
			CHECK(i == N - 1 ? !key : *key == get_key(keys, i + 1));
			bool deleted = btree_delete(&btree, get_key(keys, i), NULL);
			CHECK(deleted);
			CHECK(!btree_iter_start_at(&iter, &btree, get_key(keys, i), ITER_FIND_KEY));
			key = btree_iter_start_at(&iter, &btree, get_key(keys, i), ITER_LOWER_BOUND_INCLUSIVE);
			CHECK(i == N - 1 ? !key : *key == get_key(keys, i + 1));
			key = btree_iter_start_at(&iter, &btree, get_key(keys, i), ITER_LOWER_BOUND_EXCLUSIVE);
			CHECK(i == N - 1 ? !key : *key == get_key(keys, i + 1));
			btree_insert(&btree, get_key(keys, i));
		}

		for (size_t i = 0; i < N; i++) {
			key = btree_iter_start_at(&iter, &btree, get_key(keys, i), ITER_UPPER_BOUND_INCLUSIVE);
			CHECK(*key == get_key(keys, i));
			key = btree_iter_start_at(&iter, &btree, get_key(keys, i), ITER_UPPER_BOUND_EXCLUSIVE);
			CHECK(i == 0 ? !key : *key == get_key(keys, i - 1));
			bool deleted = btree_delete(&btree, get_key(keys, i), NULL);
			CHECK(deleted);
			CHECK(!btree_iter_start_at(&iter, &btree, get_key(keys, i), ITER_FIND_KEY));
			key = btree_iter_start_at(&iter, &btree, get_key(keys, i), ITER_UPPER_BOUND_INCLUSIVE);
			CHECK(i == 0 ? !key : *key == get_key(keys, i - 1));
			key = btree_iter_start_at(&iter, &btree, get_key(keys, i), ITER_UPPER_BOUND_EXCLUSIVE);
			CHECK(i == 0 ? !key : *key == get_key(keys, i - 1));
			btree_insert(&btree, get_key(keys, i));
		}
#endif

		CHECK_TREE();
	}

	for (size_t i = 0; i < N; i++) {
		size_t x = random_next_u64(&rng) % LIMIT;
		btree_key_t key = get_key(keys, x);
#ifdef MAP
		btree_insert_sequential(&btree, key, x);
#else
		btree_insert_sequential(&btree, key);
#endif
		CHECK_TREE();
	}

	btree_destroy(&btree);

	for (size_t i = 0; i < N; i++) {
		size_t x = random_next_u64(&rng) % LIMIT;
		btree_key_t key = get_key(keys, x);
		bool inserted = btable_insert(&btable, key);
#ifdef MAP
		bool inserted2 = btree_insert(&btree, key, x);
#else
		bool inserted2 = btree_insert(&btree, key);
#endif
		CHECK(inserted == inserted2);
		CHECK_TREE();
		CHECK(btree_find(&btree, key));
	}

	btree_destroy(&btree);
	CHECK(btree._impl.height == 0);
	CHECK_TREE();

	btable_destroy(&btable);

	for (size_t i = 0; i < N; i++) {
		btree_key_t key = get_key(keys, i);
#ifdef MAP
		bool inserted = btree_insert(&btree, key, i);
#else
		bool inserted = btree_insert(&btree, key);
#endif
		CHECK(inserted);
	}

	CHECK_TREE();

	for (size_t i = 0; i < N; i++) {
		btree_key_t key = get_key(keys, i);
#ifdef MAP
		btree_key_t min;
		btree_value_t *value = btree_get_leftmost(&btree, &min);
		CHECK(value && strtoumax(min, NULL, 10) == *value);
		CHECK(btree_info.cmp(&min, &key) == 0);
		btree_value_t val;
		bool removed = btree_delete_min(&btree, &min, &val);
		CHECK(removed && strtoumax(min, NULL, 10) == val);
		CHECK(btree_info.cmp(&min, &key) == 0);
#else
		const btree_key_t *min = btree_get_leftmost(&btree);
		CHECK(min);
		CHECK(btree_info.cmp(min, &key) == 0);
		btree_key_t min_val;
		bool removed = btree_delete_min(&btree, &min_val);
		CHECK(removed);
		CHECK(btree_info.cmp(&min_val, &key) == 0);
#endif
	}

	for (size_t i = 0; i < N; i++) {
		btree_key_t key = get_key(keys, i);
#ifdef MAP
		bool inserted = btree_insert(&btree, key, i);
#else
		bool inserted = btree_insert(&btree, key);
#endif
		CHECK(inserted);
	}

	CHECK_TREE();

	for (size_t i = 0; i < N; i++) {
		btree_key_t key = get_key(keys, N - 1 - i);
#ifdef MAP
		btree_key_t max;
		btree_value_t *value = btree_get_rightmost(&btree, &max);
		CHECK(value && strtoumax(max, NULL, 10) == *value);
		CHECK(btree_info.cmp(&max, &key) == 0);
		btree_value_t val;
		bool removed = btree_delete_max(&btree, &max, &val);
		CHECK(removed && strtoumax(max, NULL, 10) == val);
		CHECK(btree_info.cmp(&max, &key) == 0);
#else
		const btree_key_t *max = btree_get_rightmost(&btree);
		CHECK(max);
		CHECK(btree_info.cmp(max, &key) == 0);
		btree_key_t max_val;
		bool removed = btree_delete_max(&btree, &max_val);
		CHECK(removed);
		CHECK(btree_info.cmp(&max_val, &key) == 0);
#endif
	}

	CHECK(btree._impl.height == 0);

	destroy_keys(keys, num_keys);

	return true;

#undef CHECK_TREE
}

#ifdef MAP
# if defined(BPLUS) && defined(BSTAR)
DEFINE_BPLUSSTARTREE_MAP(inttree, int, int, NULL, NULL, 4, (a < b) ? -1 : (a > b));
# elif defined(BPLUS)
DEFINE_BPLUSTREE_MAP(inttree, int, int, NULL, NULL, 2, (a < b) ? -1 : (a > b));
# elif defined(BSTAR)
DEFINE_BSTARTREE_MAP(inttree, int, int, NULL, NULL, 4, (a < b) ? -1 : (a > b));
# else
DEFINE_BTREE_MAP(inttree, int, int, NULL, NULL, 2, (a < b) ? -1 : (a > b));
# endif
#else
# if defined(BPLUS) && defined(BSTAR)
DEFINE_BPLUSSTARTREE_SET(inttree, int, NULL, 3, (a < b) ? -1 : (a > b));
# elif defined(BPLUS)
DEFINE_BPLUSTREE_SET(inttree, int, NULL, 3, (a < b) ? -1 : (a > b));
# elif defined(BSTAR)
DEFINE_BSTARTREE_SET(inttree, int, NULL, 3, (a < b) ? -1 : (a > b));
# else
DEFINE_BTREE_SET(inttree, int, NULL, 3, (a < b) ? -1 : (a > b));
# endif
#endif

static uint32_t identity_hash_int(int x)
{
	return x;
}

DEFINE_HASHSET(inttable, int, NULL, identity_hash_int, a == b);

#ifdef MAP
# if defined(BPLUS) && defined(BSTAR)
static bool bplusstartree_map_test(void)
# elif defined(BPLUS)
static bool bplustree_map_test(void)
# elif defined(BSTAR)
static bool bstartree_map_test(void)
# else
static bool btree_map_test(void)
# endif
#else
# if defined(BPLUS) && defined(BSTAR)
static bool bplusstartree_set_test(void)
# elif defined(BPLUS)
static bool bplustree_set_test(void)
# elif defined(BSTAR)
static bool bstartree_set_test(void)
# else
static bool btree_set_test(void)
# endif
#endif
{
#define CHECK_TREE() CHECK(btree_check(&tree._impl, &inttree_info))
	{
		struct inttree tree;
		inttree_init(&tree);

		for (int i = 0; i < 100; i++) {
#ifdef MAP
			bool inserted = inttree_insert(&tree, i, i);
#else
			bool inserted = inttree_insert(&tree, i);
#endif
			CHECK(inserted);

			for (int j = 0; j <= i; j++) {
				const int *key = inttree_find(&tree, j);
				CHECK(key);
				CHECK(*key == j);
			}

			CHECK_TREE();
		}

		for (int i = -1; i > -100; i--) {
#ifdef MAP
			bool inserted = inttree_insert(&tree, i, i);
#else
			bool inserted = inttree_insert(&tree, i);
#endif
			CHECK(inserted);

			for (int j = 99; j >= i; j--) {
				const int *key = inttree_find(&tree, j);
				CHECK(key);
				CHECK(*key == j);
			}

			CHECK_TREE();
		}

#ifdef MAP
		int key, *value;
		value = inttree_get_leftmost(&tree, &key);
		CHECK(key == *value);
		CHECK(key == -99);
		value = inttree_get_rightmost(&tree, &key);
		CHECK(key == *value);
		CHECK(key == 99);
#else
		const int *key;
		key = inttree_get_leftmost(&tree);
		CHECK(*key == -99);
		key = inttree_get_rightmost(&tree);
		CHECK(*key == 99);
#endif

		{
			int last = -100;
			inttree_iter_t iter;
#ifdef MAP
			for (const int *p = inttree_iter_start_leftmost(&iter, &tree, &key);
			     p;
			     p = inttree_iter_next(&iter, &key)) {
				CHECK(key == *p);
#else
			for (const int *p = inttree_iter_start_leftmost(&iter, &tree);
			     p;
			     p = inttree_iter_next(&iter)) {
#endif
				CHECK(*p == last + 1);
				last = *p;
			}
			CHECK(last == 99);
		}
		{
			int last = 100;
			inttree_iter_t iter;
#ifdef MAP
			for (const int *p = inttree_iter_start_rightmost(&iter, &tree, &key);
			     p;
			     p = inttree_iter_prev(&iter, &key)) {
				CHECK(key == *p);
#else
			for (const int *p = inttree_iter_start_rightmost(&iter, &tree);
			     p;
			     p = inttree_iter_prev(&iter)) {
#endif
				CHECK(*p == last - 1);
				last = *p;
			}
			CHECK(last == -99);
		}

		struct {
			int mode;
			int expected;
		} iter_start_at_tests[] = {
			{ ITER_FIND_KEY, 0 },
			{ ITER_LOWER_BOUND_INCLUSIVE, 0 },
			{ ITER_LOWER_BOUND_EXCLUSIVE, 1 },
			{ ITER_UPPER_BOUND_INCLUSIVE, 0 },
			{ ITER_UPPER_BOUND_EXCLUSIVE, -1 },
			{ -1, 123 },
			{ ITER_LOWER_BOUND_INCLUSIVE, 1 },
			{ ITER_LOWER_BOUND_EXCLUSIVE, 1 },
			{ ITER_UPPER_BOUND_INCLUSIVE, -1 },
			{ ITER_UPPER_BOUND_EXCLUSIVE, -1 },
		};

		for (size_t i = 0; i < sizeof(iter_start_at_tests) / sizeof(iter_start_at_tests[0]); i++) {
			int mode = iter_start_at_tests[i].mode;
			int expected = iter_start_at_tests[i].expected;

			if (mode == -1) {
#ifdef MAP
				inttree_delete(&tree, 0, NULL, NULL);
#else
				inttree_delete(&tree, 0, NULL);
#endif
				continue;
			}

			inttree_iter_t iter;
#ifdef MAP
			const int *p = inttree_iter_start_at(&iter, &tree, 0, &key, mode);
			CHECK(key == *p);
#else
			const int *p = inttree_iter_start_at(&iter, &tree, 0, mode);
#endif
			CHECK(*p == expected);
		}
		{
			inttree_iter_t iter;
#ifdef MAP
			key = 12345;
			const int *p = inttree_iter_start_at(&iter, &tree, 0, &key, ITER_FIND_KEY);
			CHECK(key == 12345);
#else
			const int *p = inttree_iter_start_at(&iter, &tree, 0, ITER_FIND_KEY);
#endif
			CHECK(!p);
		}

		CHECK_TREE();

		inttree_destroy(&tree);

		CHECK_TREE();
	}

	{
		struct inttree tree;
		inttree_init(&tree);

		for (int i = 99; i >= 0; i--) {
#ifdef MAP
			bool inserted = inttree_insert(&tree, i, i);
#else
			bool inserted = inttree_insert(&tree, i);
#endif
			CHECK(inserted);

			for (int j = 99; j >= i; j--) {
				const int *key = inttree_find(&tree, j);
				CHECK(key);
				CHECK(*key == j);
			}

			CHECK_TREE();
		}

		for (int i = -99; i < 0; i++) {
#ifdef MAP
			bool inserted = inttree_insert(&tree, i, i);
#else
			bool inserted = inttree_insert(&tree, i);
#endif
			CHECK(inserted);

			for (int j = -99; j <= i; j++) {
				const int *key = inttree_find(&tree, j);
				CHECK(key);
				CHECK(*key == j);
			}

			CHECK_TREE();
		}

		for (int j = -99; j <= 99; j++) {
			const int *key = inttree_find(&tree, j);
			CHECK(key);
			CHECK(*key == j);
		}

		for (int i = -99; i < 0; i++) {
			int key;
#ifdef MAP
			const int *min = inttree_get_leftmost(&tree, &key);
			assert(key == i);
#else
			const int *min = inttree_get_leftmost(&tree);
#endif
			assert(*min == i);
#ifdef MAP
			int value;
			inttree_delete_min(&tree, &key, &value);
			assert(value == i);
#else
			inttree_delete_min(&tree, &key);
#endif
			assert(key == i);
			CHECK_TREE();
		}


		for (int i = 100; i-- > 0;) {
			int key;
#ifdef MAP
			const int *max = inttree_get_rightmost(&tree, &key);
			assert(key == i);
#else
			const int *max = inttree_get_rightmost(&tree);
#endif
			assert(*max == i);
#ifdef MAP
			int value;
			inttree_delete_max(&tree, &key, &value);
			assert(value == i);
#else
			inttree_delete_max(&tree, &key);
#endif
			assert(key == i);
			CHECK_TREE();
		}
	}

	{
		struct inttree tree;
		inttree_init(&tree);

		struct inttable table;
		inttable_init(&table);

		struct random_state rng;
		random_state_init(&rng, 123456789);

		const size_t N = 1000;

		for (size_t i = 0; i < N; i++) {
			int key = random_next_u64_in_range(&rng, 0, N);
#ifdef MAP
			bool inserted = inttree_insert(&tree, key, key);
#else
			bool inserted = inttree_insert(&tree, key);
#endif
			bool inserted2 = inttable_insert(&table, key);
			CHECK(inserted == inserted2);

			for (inttable_iter_t iter = inttable_iter_start(&table);
			     inttable_iter_advance(&iter);) {
				int key = *inttable_iter_key(&iter);
				const int *p = inttree_find(&tree, key);
				CHECK(p);
				CHECK(*p == key);
			}

			CHECK_TREE();
		}

		for (;;) {
			inttable_iter_t iter = inttable_iter_start(&table);
			if (!inttable_iter_advance(&iter)) {
				break;
			}
			int cur_key = *inttable_iter_key(&iter);

			int key;
#ifdef MAP
			int value;
			bool deleted = inttree_delete(&tree, cur_key, &key, &value);
			CHECK(key == value);
#else
			bool deleted = inttree_delete(&tree, cur_key, &key);
#endif
			CHECK(deleted);
			CHECK(key == cur_key);
			CHECK(!inttree_find(&tree, cur_key));
			inttable_remove(&table, key, NULL);
			for (inttable_iter_t iter = inttable_iter_start(&table);
			     inttable_iter_advance(&iter);) {
				CHECK(inttree_find(&tree, *inttable_iter_key(&iter)));
			}

			CHECK_TREE();
		}

		CHECK(inttable_num_entries(&table) == 0);

		inttable_destroy(&table);

		inttree_destroy(&tree);
	}

	return true;

#undef CHECK_TREE
}

static void free_string(char *string)
{
	free(string);
}

#ifdef MAP
# if defined(BPLUS) && defined(BSTAR)
DEFINE_BPLUSSTARTREE_MAP(stringtree, char *, char *, free_string, free_string, 3, strcmp(a, b));
# elif defined(BPLUS)
DEFINE_BPLUSTREE_MAP(stringtree, char *, char *, free_string, free_string, 3, strcmp(a, b));
# elif defined(BSTAR)
DEFINE_BSTARTREE_MAP(stringtree, char *, char *, free_string, free_string, 3, strcmp(a, b));
# else
DEFINE_BTREE_MAP(stringtree, char *, char *, free_string, free_string, 3, strcmp(a, b));
# endif
#else
# if defined(BPLUS) && defined(BSTAR)
DEFINE_BPLUSSTARTREE_SET(stringtree, char *, free_string, 4, strcmp(a, b));
# elif defined(BPLUS)
DEFINE_BPLUSTREE_SET(stringtree, char *, free_string, 2, strcmp(a, b));
# elif defined(BSTAR)
DEFINE_BSTARTREE_SET(stringtree, char *, free_string, 4, strcmp(a, b));
# else
DEFINE_BTREE_SET(stringtree, char *, free_string, 2, strcmp(a, b));
# endif
#endif

#ifdef MAP
# if defined(BPLUS) && defined(BSTAR)
static bool bplusstartree_map_allocated_strings_test(void)
# elif defined(BPLUS)
static bool bplustree_map_allocated_strings_test(void)
# elif defined(BSTAR)
static bool bstartree_map_allocated_strings_test(void)
# else
static bool btree_map_allocated_strings_test(void)
# endif
#else
# if defined(BPLUS) && defined(BSTAR)
static bool bplusstartree_set_allocated_strings_test(void)
# elif defined(BPLUS)
static bool bplustree_set_allocated_strings_test(void)
# elif defined(BSTAR)
static bool bstartree_set_allocated_strings_test(void)
# else
static bool btree_set_allocated_strings_test(void)
# endif
#endif
{
#define CHECK_TREE() CHECK(btree_check(&tree._impl, &stringtree_info))
	struct stringtree tree;
	stringtree_init(&tree);

#define STRING_SIZE 16
#define STRING_FMT "%015u"
	const unsigned int N = 4000;

	for (unsigned int i = 0; i < N; i++) {
		char *string = malloc(STRING_SIZE);
		snprintf(string, STRING_SIZE, STRING_FMT, i);
#ifdef MAP
		bool inserted = stringtree_insert_sequential(&tree, string, strdup(string));
#else
		bool inserted = stringtree_insert_sequential(&tree, string);
#endif
		CHECK(inserted);
	}

	CHECK_TREE();

	for (unsigned int i = 0; i < N / 2; i++) {
		char string[STRING_SIZE];
		snprintf(string, STRING_SIZE, STRING_FMT, i);
		char *key;
#ifdef MAP
		char *value;
		bool deleted = stringtree_delete(&tree, string, &key, &value);
		CHECK(strcmp(string, value) == 0);
		free(value);
#else
		bool deleted = stringtree_delete(&tree, string, &key);
#endif
		CHECK(deleted);
		CHECK(strcmp(string, key) == 0);
		free(key);

		for (unsigned int j = i + 1; j < N; j++) {
			char string[STRING_SIZE];
			snprintf(string, STRING_SIZE, STRING_FMT, j);
			CHECK(stringtree_find(&tree, string));
		}

		if ((i % 1000) == 999) {
			CHECK_TREE();
		}
	}

	CHECK_TREE();

	for (unsigned int i = N / 2; i < N; i++) {
		char string[STRING_SIZE];
		snprintf(string, STRING_SIZE, STRING_FMT, i);
#ifdef MAP
		bool deleted = stringtree_delete(&tree, string, NULL, NULL);
#else
		bool deleted = stringtree_delete(&tree, string, NULL);
#endif
		CHECK(deleted);

		if ((i % 1000) == 999) {
			CHECK_TREE();
		}
	}

	CHECK_TREE();

	CHECK(tree._impl.height == 0); // TODO implement an is_empty function and use that here?

	for (unsigned int i = 0; i < N; i++) {
		char *string = malloc(STRING_SIZE);
		snprintf(string, STRING_SIZE, STRING_FMT, i);
#ifdef MAP
		bool inserted = stringtree_insert(&tree, string, strdup(string));
#else
		bool inserted = stringtree_insert(&tree, string);
#endif
		CHECK(inserted);
	}

	CHECK_TREE();

	for (unsigned int i = 0; i < N; i++) {
		char string[STRING_SIZE];
		snprintf(string, STRING_SIZE, STRING_FMT, i);
		char *key;
#ifdef MAP
		char *value;
		bool deleted = stringtree_delete_min(&tree, &key, &value);
		CHECK(strcmp(string, value) == 0);
		free(value);
#else
		bool deleted = stringtree_delete_min(&tree, &key);
#endif
		CHECK(deleted);
		CHECK(strcmp(string, key) == 0);
		free(key);

		if ((i % 1000) == 999) {
			CHECK_TREE();
		}
	}

	CHECK(tree._impl.height == 0);

	for (unsigned int i = 0; i < N; i++) {
		char *string = malloc(STRING_SIZE);
		snprintf(string, STRING_SIZE, STRING_FMT, i);
#ifdef MAP
		bool inserted = stringtree_insert(&tree, string, strdup(string));
#else
		bool inserted = stringtree_insert(&tree, string);
#endif
		CHECK(inserted);
	}

	CHECK_TREE();

	for (unsigned int i = 0; i < N; i++) {
#ifdef MAP
		bool deleted = stringtree_delete_max(&tree, NULL, NULL);
#else
		bool deleted = stringtree_delete_max(&tree, NULL);
#endif
		CHECK(deleted);

		if ((i % 1000) == 999) {
			CHECK_TREE();
		}
	}

	CHECK(tree._impl.height == 0);

	for (unsigned int i = 0; i < N; i++) {
		char *string = malloc(STRING_SIZE);
		snprintf(string, STRING_SIZE, STRING_FMT, i);
#ifdef MAP
		bool inserted = stringtree_insert(&tree, string, strdup(string));
#else
		bool inserted = stringtree_insert(&tree, string);
#endif
		CHECK(inserted);
	}

	struct random_state rng;
	random_state_init(&rng, 123456789);

	for (unsigned int i = 0; i < 2 * N; i++) {
		char string[STRING_SIZE];
		snprintf(string, STRING_SIZE, STRING_FMT, (int)random_next_u64_in_range(&rng, 0, N - 1));
#ifdef MAP
		stringtree_delete(&tree, string, NULL, NULL);
#else
		stringtree_delete(&tree, string, NULL);
#endif

		if ((i % 1000) == 999) {
			CHECK_TREE();
		}
	}

	CHECK_TREE();

	stringtree_destroy(&tree);

	CHECK(tree._impl.height == 0);

	for (unsigned int i = 0; i < N; i++) {
		char *string = malloc(STRING_SIZE);
		snprintf(string, STRING_SIZE, STRING_FMT, i);
#ifdef MAP
		bool inserted = stringtree_insert(&tree, string, strdup(string));
#else
		bool inserted = stringtree_insert(&tree, string);
#endif
		CHECK(inserted);
	}

	CHECK_TREE();

	stringtree_destroy(&tree);

	CHECK(tree._impl.height == 0);

	for (unsigned int i = N; i-- > 0;) {
		char *string = malloc(STRING_SIZE);
		snprintf(string, STRING_SIZE, STRING_FMT, i);
#ifdef MAP
		bool inserted = stringtree_insert(&tree, string, strdup(string));
#else
		bool inserted = stringtree_insert(&tree, string);
#endif
		CHECK(inserted);
	}

	CHECK_TREE();

	for (unsigned int i = 0; i < N; i++) {
		char string[STRING_SIZE];
		snprintf(string, STRING_SIZE, STRING_FMT, i);
#ifdef MAP
		bool deleted = stringtree_delete(&tree, string, NULL, NULL);
#else
		bool deleted = stringtree_delete(&tree, string, NULL);
#endif
		CHECK(deleted);

		if ((i % 1000) == 999) {
			CHECK_TREE();
		}
	}

	CHECK_TREE();

	for (unsigned int i = N; i-- > 0;) {
		char *string = malloc(STRING_SIZE);
		snprintf(string, STRING_SIZE, STRING_FMT, i);
#ifdef MAP
		bool inserted = stringtree_insert_sequential(&tree, string, strdup(string));
#else
		bool inserted = stringtree_insert_sequential(&tree, string);
#endif
		CHECK(inserted);
	}

	CHECK_TREE();

	for (unsigned int i = N; i-- > 0;) {
		char string[STRING_SIZE];
		snprintf(string, STRING_SIZE, STRING_FMT, i);
#ifdef MAP
		bool deleted = stringtree_delete(&tree, string, NULL, NULL);
#else
		bool deleted = stringtree_delete(&tree, string, NULL);
#endif
		CHECK(deleted);

		if ((i % 1000) == 999) {
			CHECK_TREE();
		}
	}

	CHECK(tree._impl.height == 0);

	random_state_init(&rng, 123456789);
	for (unsigned int i = 0; i < N; i++) {
		char *string = malloc(STRING_SIZE);
		snprintf(string, STRING_SIZE, STRING_FMT, (int)random_next_u64_in_range(&rng, 0, N - 1));
#ifdef MAP
		char *value = strdup(string);
		bool inserted = stringtree_insert(&tree, string, value);
		if (!inserted) {
			free(value);
		}
#else
		bool inserted = stringtree_insert(&tree, string);
#endif
		if (!inserted) {
			free(string);
		}
	}

	CHECK_TREE();

	for (unsigned int i = 0; i < N; i++) {
		char string[STRING_SIZE];
		snprintf(string, STRING_SIZE, STRING_FMT, i);
#ifdef MAP
		stringtree_delete(&tree, string, NULL, NULL);
#else
		stringtree_delete(&tree, string, NULL);
#endif
		if ((i % 1000) == 999) {
			CHECK_TREE();
		}
	}

	CHECK(tree._impl.height == 0);

	random_state_init(&rng, 123456789);
	for (unsigned int i = 0; i < N; i++) {
		char *string = malloc(STRING_SIZE);
		snprintf(string, STRING_SIZE, STRING_FMT, (int)random_next_u64_in_range(&rng, 0, N - 1));
#ifdef MAP
		char *value = strdup(string);
		bool inserted = stringtree_insert_sequential(&tree, string, value);
		if (!inserted) {
			free(value);
		}
#else
		bool inserted = stringtree_insert_sequential(&tree, string);
#endif
		if (!inserted) {
			free(string);
		}
	}

	CHECK_TREE();

	random_state_init(&rng, 123456789);
	for (unsigned int i = 0; i < N; i++) {
		char string[STRING_SIZE];
		snprintf(string, STRING_SIZE, STRING_FMT, (int)random_next_u64_in_range(&rng, 0, N - 1));
#ifdef MAP
		stringtree_delete(&tree, string, NULL, NULL);
#else
		stringtree_delete(&tree, string, NULL);
#endif
		if ((i % 1000) == 999) {
			CHECK_TREE();
		}
	}

	CHECK(tree._impl.height == 0);

	return true;

#undef CHECK_TREE
}

#define KEY_ALIGN   sizeof(void *)
#define VALUE_ALIGN (2 * KEY_ALIGN)

struct key {
	_Alignas(KEY_ALIGN) int i;
};

struct value {
	_Alignas(VALUE_ALIGN) int i;
};

#ifdef MAP
# if defined(BPLUS) && defined(BSTAR)
DEFINE_BPLUSSTARTREE_MAP(structtreeodd, struct key, struct value, NULL, NULL, 7, (a.i < b.i) ? -1 : (a.i > b.i));
DEFINE_BPLUSSTARTREE_MAP(structtreeeven, struct key, struct value, NULL, NULL, 8, (a.i < b.i) ? -1 : (a.i > b.i));
DEFINE_BPLUSSTARTREE_MAP(chartreeodd, char, char, NULL, NULL, 3, (a < b) ? -1 : (a > b));
DEFINE_BPLUSSTARTREE_MAP(chartreeeven, char, char, NULL, NULL, 4, (a < b) ? -1 : (a > b));
# elif defined(BPLUS)
DEFINE_BPLUSTREE_MAP(structtreeodd, struct key, struct value, NULL, NULL, 7, (a.i < b.i) ? -1 : (a.i > b.i));
DEFINE_BPLUSTREE_MAP(structtreeeven, struct key, struct value, NULL, NULL, 8, (a.i < b.i) ? -1 : (a.i > b.i));
DEFINE_BPLUSTREE_MAP(chartreeodd, char, char, NULL, NULL, 3, (a < b) ? -1 : (a > b));
DEFINE_BPLUSTREE_MAP(chartreeeven, char, char, NULL, NULL, 2, (a < b) ? -1 : (a > b));
# elif defined(BSTAR)
DEFINE_BSTARTREE_MAP(structtreeodd, struct key, struct value, NULL, NULL, 7, (a.i < b.i) ? -1 : (a.i > b.i));
DEFINE_BSTARTREE_MAP(structtreeeven, struct key, struct value, NULL, NULL, 8, (a.i < b.i) ? -1 : (a.i > b.i));
DEFINE_BSTARTREE_MAP(chartreeodd, char, char, NULL, NULL, 3, (a < b) ? -1 : (a > b));
DEFINE_BSTARTREE_MAP(chartreeeven, char, char, NULL, NULL, 4, (a < b) ? -1 : (a > b));
# else
DEFINE_BTREE_MAP(structtreeodd, struct key, struct value, NULL, NULL, 7, (a.i < b.i) ? -1 : (a.i > b.i));
DEFINE_BTREE_MAP(structtreeeven, struct key, struct value, NULL, NULL, 8, (a.i < b.i) ? -1 : (a.i > b.i));
DEFINE_BTREE_MAP(chartreeodd, char, char, NULL, NULL, 3, (a < b) ? -1 : (a > b));
DEFINE_BTREE_MAP(chartreeeven, char, char, NULL, NULL, 2, (a < b) ? -1 : (a > b));
# endif
#else
# if defined(BPLUS) && defined(BSTAR)
DEFINE_BPLUSSTARTREE_SET(structtreeodd, struct key, NULL, 7, (a.i < b.i) ? -1 : (a.i > b.i));
DEFINE_BPLUSSTARTREE_SET(structtreeeven, struct key, NULL, 8, (a.i < b.i) ? -1 : (a.i > b.i));
DEFINE_BPLUSSTARTREE_SET(chartreeodd, char, NULL, 3, (a < b) ? -1 : (a > b));
DEFINE_BPLUSSTARTREE_SET(chartreeeven, char, NULL, 4, (a < b) ? -1 : (a > b));
# elif defined(BPLUS)
DEFINE_BPLUSTREE_SET(structtreeodd, struct key, NULL, 7, (a.i < b.i) ? -1 : (a.i > b.i));
DEFINE_BPLUSTREE_SET(structtreeeven, struct key, NULL, 8, (a.i < b.i) ? -1 : (a.i > b.i));
DEFINE_BPLUSTREE_SET(chartreeodd, char, NULL, 3, (a < b) ? -1 : (a > b));
DEFINE_BPLUSTREE_SET(chartreeeven, char, NULL, 2, (a < b) ? -1 : (a > b));
# elif defined(BSTAR)
DEFINE_BSTARTREE_SET(structtreeodd, struct key, NULL, 7, (a.i < b.i) ? -1 : (a.i > b.i));
DEFINE_BSTARTREE_SET(structtreeeven, struct key, NULL, 8, (a.i < b.i) ? -1 : (a.i > b.i));
DEFINE_BSTARTREE_SET(chartreeodd, char, NULL, 3, (a < b) ? -1 : (a > b));
DEFINE_BSTARTREE_SET(chartreeeven, char, NULL, 4, (a < b) ? -1 : (a > b));
# else
DEFINE_BTREE_SET(structtreeodd, struct key, NULL, 7, (a.i < b.i) ? -1 : (a.i > b.i));
DEFINE_BTREE_SET(structtreeeven, struct key, NULL, 8, (a.i < b.i) ? -1 : (a.i > b.i));
DEFINE_BTREE_SET(chartreeodd, char, NULL, 3, (a < b) ? -1 : (a > b));
DEFINE_BTREE_SET(chartreeeven, char, NULL, 2, (a < b) ? -1 : (a > b));
# endif
#endif

#ifdef MAP
# if defined(BPLUS) && defined(BSTAR)
static bool bplusstartree_map_alignment_test(void)
# elif defined(BPLUS)
static bool bplustree_map_alignment_test(void)
# elif defined(BSTAR)
static bool bstartree_map_alignment_test(void)
# else
static bool btree_map_alignment_test(void)
# endif
#else
# if defined(BPLUS) && defined(BSTAR)
static bool bplusstartree_set_alignment_test(void)
# elif defined(BPLUS)
static bool bplustree_set_alignment_test(void)
# elif defined(BSTAR)
static bool bstartree_set_alignment_test(void)
# else
static bool btree_set_alignment_test(void)
# endif
#endif
{
// #if defined(BPLUS)
// #define children_alignment_offset value_alignment_offset
// #define root_children_alignment_offset root_value_alignment_offset
// #endif
// 	test_log("%u\n", structtreeodd_info.key_alignment_offset);
// 	test_log("%u\n", structtreeodd_info.children_alignment_offset);
// #ifdef BSTAR
// 	test_log("%u\n", structtreeodd_info.root_children_alignment_offset);
// #endif
// 	test_log("\n");
// 	test_log("%u\n", structtreeeven_info.key_alignment_offset);
// 	test_log("%u\n", structtreeeven_info.children_alignment_offset);
// #ifdef BSTAR
// 	test_log("%u\n", structtreeeven_info.root_children_alignment_offset);
// #endif
// 	test_log("\n");
// 	test_log("%u\n", chartreeodd_info.key_alignment_offset);
// 	test_log("%u\n", chartreeodd_info.children_alignment_offset);
// #ifdef BSTAR
// 	test_log("%u\n", chartreeodd_info.root_children_alignment_offset);
// #endif
// 	test_log("\n");
// 	test_log("%u\n", chartreeeven_info.key_alignment_offset);
// 	test_log("%u\n", chartreeeven_info.children_alignment_offset);
// #ifdef BSTAR
// 	test_log("%u\n", chartreeeven_info.root_children_alignment_offset);
// #endif
// 	return false;

	int N = 1000;

	{
		struct structtreeodd otree;
		structtreeodd_init(&otree);
		for (int i = 0; i < N; i++) {
#ifdef MAP
			structtreeodd_insert(&otree, (struct key){ i }, (struct value){ i });
#else
			structtreeodd_insert(&otree, (struct key){ i });
#endif
		}

		CHECK(btree_check(&otree._impl, &structtreeodd_info));

		for (int i = 0; i < N; i++) {
#ifdef MAP
			size_t align = VALUE_ALIGN;
			const void *p = structtreeodd_find(&otree, (struct key){ i });
#else
			size_t align = KEY_ALIGN;
			const void *p = structtreeodd_find(&otree, (struct key){ i });
#endif
			CHECK(p);
			CHECK(((uintptr_t)p % align) == 0);
		}

		structtreeodd_destroy(&otree);
	}

	{
		struct structtreeeven etree;
		structtreeeven_init(&etree);
		for (int i = 0; i < N; i++) {
#ifdef MAP
			structtreeeven_insert(&etree, (struct key){ i }, (struct value){ i });
#else
			structtreeeven_insert(&etree, (struct key){ i });
#endif
		}

		CHECK(btree_check(&etree._impl, &structtreeeven_info));

		for (int i = 0; i < N; i++) {
#ifdef MAP
			// there is no function that returns a pointer to a key for maps,
			// but the alignment is still checked by ASan
			size_t align = VALUE_ALIGN;
			const void *p = structtreeeven_find(&etree, (struct key){ i });
#else
			size_t align = KEY_ALIGN;
			const void *p = structtreeeven_find(&etree, (struct key){ i });
#endif
			CHECK(p);
			CHECK(((uintptr_t)p % align) == 0);
		}

		structtreeeven_destroy(&etree);
	}

	N = 128;

	{
		struct chartreeodd cotree;
		chartreeodd_init(&cotree);
		for (int i = 0; i < N; i++) {
#ifdef MAP
			chartreeodd_insert(&cotree, i, i);
#else
			chartreeodd_insert(&cotree, i);
#endif
		}

		CHECK(btree_check(&cotree._impl, &chartreeodd_info));

		for (int i = 0; i < N; i++) {
#ifdef MAP
			const char *p = chartreeodd_find(&cotree, i);
#else
			const char *p = chartreeodd_find(&cotree, i);
#endif
			CHECK(p);
			CHECK(*p == i);
		}

		chartreeodd_destroy(&cotree);
	}

	{
		struct chartreeeven cetree;
		chartreeeven_init(&cetree);
		for (int i = 0; i < N; i++) {
#ifdef MAP
			chartreeeven_insert(&cetree, i, i);
#else
			chartreeeven_insert(&cetree, i);
#endif
		}

		CHECK(btree_check(&cetree._impl, &chartreeeven_info));

		for (int i = 0; i < N; i++) {
#ifdef MAP
			const char *p = chartreeeven_find(&cetree, i);
#else
			const char *p = chartreeeven_find(&cetree, i);
#endif
			CHECK(p);
			CHECK(*p == i);
		}

		chartreeeven_destroy(&cetree);
	}

	return true;
}

#ifdef MAP
# if defined(BPLUS) && defined(BSTAR)
DEFINE_BPLUSSTARTREE_MAP(itree3, int, int, NULL, NULL, 3, (a < b) ? -1 : (a > b));
DEFINE_BPLUSSTARTREE_MAP(itree4, int, int, NULL, NULL, 4, (a < b) ? -1 : (a > b));
DEFINE_BPLUSSTARTREE_MAP(itree5, int, int, NULL, NULL, 5, (a < b) ? -1 : (a > b));
DEFINE_BPLUSSTARTREE_MAP(itree2, int, int, NULL, NULL, 6, (a < b) ? -1 : (a > b));
# elif defined(BPLUS)
DEFINE_BPLUSTREE_MAP(itree2, int, int, NULL, NULL, 2, (a < b) ? -1 : (a > b));
DEFINE_BPLUSTREE_MAP(itree3, int, int, NULL, NULL, 3, (a < b) ? -1 : (a > b));
DEFINE_BPLUSTREE_MAP(itree4, int, int, NULL, NULL, 4, (a < b) ? -1 : (a > b));
DEFINE_BPLUSTREE_MAP(itree5, int, int, NULL, NULL, 5, (a < b) ? -1 : (a > b));
# elif defined(BSTAR)
DEFINE_BSTARTREE_MAP(itree3, int, int, NULL, NULL, 3, (a < b) ? -1 : (a > b));
DEFINE_BSTARTREE_MAP(itree4, int, int, NULL, NULL, 4, (a < b) ? -1 : (a > b));
DEFINE_BSTARTREE_MAP(itree5, int, int, NULL, NULL, 5, (a < b) ? -1 : (a > b));
DEFINE_BSTARTREE_MAP(itree2, int, int, NULL, NULL, 6, (a < b) ? -1 : (a > b));
# else
DEFINE_BTREE_MAP(itree2, int, int, NULL, NULL, 2, (a < b) ? -1 : (a > b));
DEFINE_BTREE_MAP(itree3, int, int, NULL, NULL, 3, (a < b) ? -1 : (a > b));
DEFINE_BTREE_MAP(itree4, int, int, NULL, NULL, 4, (a < b) ? -1 : (a > b));
DEFINE_BTREE_MAP(itree5, int, int, NULL, NULL, 5, (a < b) ? -1 : (a > b));
# endif
#else
# if defined(BPLUS) && defined(BSTAR)
DEFINE_BPLUSSTARTREE_SET(itree2, int, NULL, 3, (a < b) ? -1 : (a > b));
DEFINE_BPLUSSTARTREE_SET(itree3, int, NULL, 4, (a < b) ? -1 : (a > b));
DEFINE_BPLUSSTARTREE_SET(itree4, int, NULL, 5, (a < b) ? -1 : (a > b));
DEFINE_BPLUSSTARTREE_SET(itree5, int, NULL, 6, (a < b) ? -1 : (a > b));
# elif defined(BPLUS)
DEFINE_BPLUSTREE_SET(itree2, int, NULL, 2, (a < b) ? -1 : (a > b));
DEFINE_BPLUSTREE_SET(itree3, int, NULL, 3, (a < b) ? -1 : (a > b));
DEFINE_BPLUSTREE_SET(itree4, int, NULL, 4, (a < b) ? -1 : (a > b));
DEFINE_BPLUSTREE_SET(itree5, int, NULL, 5, (a < b) ? -1 : (a > b));
# elif defined(BSTAR)
DEFINE_BSTARTREE_SET(itree2, int, NULL, 3, (a < b) ? -1 : (a > b));
DEFINE_BSTARTREE_SET(itree3, int, NULL, 4, (a < b) ? -1 : (a > b));
DEFINE_BSTARTREE_SET(itree4, int, NULL, 5, (a < b) ? -1 : (a > b));
DEFINE_BSTARTREE_SET(itree5, int, NULL, 6, (a < b) ? -1 : (a > b));
# else
DEFINE_BTREE_SET(itree2, int, NULL, 2, (a < b) ? -1 : (a > b));
DEFINE_BTREE_SET(itree3, int, NULL, 3, (a < b) ? -1 : (a > b));
DEFINE_BTREE_SET(itree4, int, NULL, 4, (a < b) ? -1 : (a > b));
DEFINE_BTREE_SET(itree5, int, NULL, 5, (a < b) ? -1 : (a > b));
# endif
#endif

#ifdef MAP
# if defined(BPLUS) && defined(BSTAR)
static bool bplusstartree_map_bulk_loading_test(void)
# elif defined(BPLUS)
static bool bplustree_map_bulk_loading_test(void)
# elif defined(BSTAR)
static bool bstartree_map_bulk_loading_test(void)
# else
static bool btree_map_bulk_loading_test(void)
# endif
#else
# if defined(BPLUS) && defined(BSTAR)
static bool bplusstartree_set_bulk_loading_test(void)
# elif defined(BPLUS)
static bool bplustree_set_bulk_loading_test(void)
# elif defined(BSTAR)
static bool bstartree_set_bulk_loading_test(void)
# else
static bool btree_set_bulk_loading_test(void)
# endif
#endif
{
	for (int n = 0; n <= 100; n++) {
		itree2_bulk_load_ctx_t ctx2 = itree2_bulk_load_start();
		itree3_bulk_load_ctx_t ctx3 = itree3_bulk_load_start();
		itree4_bulk_load_ctx_t ctx4 = itree4_bulk_load_start();
		itree5_bulk_load_ctx_t ctx5 = itree5_bulk_load_start();

		for (int i = 0; i < n; i++) {
#ifdef MAP
			itree2_bulk_load_next(&ctx2, i, i);
			itree3_bulk_load_next(&ctx3, i, i);
			itree4_bulk_load_next(&ctx4, i, i);
			itree5_bulk_load_next(&ctx5, i, i);
#else
			itree2_bulk_load_next(&ctx2, i);
			itree3_bulk_load_next(&ctx3, i);
			itree4_bulk_load_next(&ctx4, i);
			itree5_bulk_load_next(&ctx5, i);
#endif
		}

		struct itree2 tree2 = itree2_bulk_load_end(&ctx2);
		struct itree3 tree3 = itree3_bulk_load_end(&ctx3);
		struct itree4 tree4 = itree4_bulk_load_end(&ctx4);
		struct itree5 tree5 = itree5_bulk_load_end(&ctx5);

		CHECK(btree_check(&tree2._impl, &itree2_info));
		CHECK(btree_check(&tree3._impl, &itree3_info));
		CHECK(btree_check(&tree4._impl, &itree4_info));
		CHECK(btree_check(&tree5._impl, &itree5_info));
		for (int i = 0; i < n; i++) {
			const int *p = itree2_find(&tree2, i);
			CHECK(p);
			CHECK(*p == i);
			p = itree3_find(&tree3, i);
			CHECK(p);
			CHECK(*p == i);
			p = itree4_find(&tree4, i);
			CHECK(p);
			CHECK(*p == i);
			p = itree5_find(&tree5, i);
			CHECK(p);
			CHECK(*p == i);
		}

		// make sure the linked list was constructed correctly for B+ trees
		itree2_iter_t iter2;
		itree3_iter_t iter3;
		itree4_iter_t iter4;
		itree5_iter_t iter5;
#ifdef MAP
		const int *p2 = itree2_iter_start_leftmost(&iter2, &tree2, NULL);
		const int *p3 = itree3_iter_start_leftmost(&iter3, &tree3, NULL);
		const int *p4 = itree4_iter_start_leftmost(&iter4, &tree4, NULL);
		const int *p5 = itree5_iter_start_leftmost(&iter5, &tree5, NULL);
#else
		const int *p2 = itree2_iter_start_leftmost(&iter2, &tree2);
		const int *p3 = itree3_iter_start_leftmost(&iter3, &tree3);
		const int *p4 = itree4_iter_start_leftmost(&iter4, &tree4);
		const int *p5 = itree5_iter_start_leftmost(&iter5, &tree5);
#endif
		for (int i = 0; i < n; i++) {
			CHECK(p2 && *p2 == i);
			CHECK(p3 && *p3 == i);
			CHECK(p4 && *p4 == i);
			CHECK(p5 && *p5 == i);
#ifdef MAP
			p2 = itree2_iter_next(&iter2, NULL);
			p3 = itree3_iter_next(&iter3, NULL);
			p4 = itree4_iter_next(&iter4, NULL);
			p5 = itree5_iter_next(&iter5, NULL);
#else
			p2 = itree2_iter_next(&iter2);
			p3 = itree3_iter_next(&iter3);
			p4 = itree4_iter_next(&iter4);
			p5 = itree5_iter_next(&iter5);
#endif
		}
		CHECK(!p2);
		CHECK(!p3);
		CHECK(!p4);
		CHECK(!p5);

		itree2_destroy(&tree2);
		itree3_destroy(&tree3);
		itree4_destroy(&tree4);
		itree5_destroy(&tree5);
	}

	return true;
}
