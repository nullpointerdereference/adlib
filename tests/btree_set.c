#include "btree_test.h"

SIMPLE_TEST(btree_set)
{
	return btree_set_test();
}

RANDOM_TEST(btree_set_random, random_seed, 2)
{
	return btree_set_random_test(random_seed);
}

SIMPLE_TEST(btree_set_allocated_strings)
{
	return btree_set_allocated_strings_test();
}

SIMPLE_TEST(btree_set_alignment)
{
	return btree_set_alignment_test();
}

SIMPLE_TEST(btree_set_bulk_loading)
{
	return btree_set_bulk_loading_test();
}
