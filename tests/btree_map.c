#define MAP
#include "btree_test.h"

SIMPLE_TEST(btree_map)
{
	return btree_map_test();
}

RANDOM_TEST(btree_map_random, random_seed, 2)
{
	return btree_map_random_test(random_seed);
}

SIMPLE_TEST(btree_map_allocated_strings)
{
	return btree_map_allocated_strings_test();
}

SIMPLE_TEST(btree_map_alignment)
{
	return btree_map_alignment_test();
}

SIMPLE_TEST(btree_map_bulk_loading)
{
	return btree_map_bulk_loading_test();
}
