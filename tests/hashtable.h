#include <math.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include "bplustree.h"
#include "hashmap.h"
#include "hashset.h"
#include "hash.h"
#include "random.h"
#include "testing.h"

static inline uint32_t integer_hash(int x)
{
	return int_hash32(x);
}

#ifdef MAP
DEFINE_HASHMAP(ihash, int, int, NULL, NULL, integer_hash, a == b);
#else
DEFINE_HASHSET(ihash, int, NULL, integer_hash, a == b);
#endif
DEFINE_BPLUSTREE_SET(itree, int, NULL, 16, a < b ? -1 : (a != b));

#ifdef MAP
#define ihash_insert_unchecked(hash, key, value)			\
	do { *ihash_insert_unchecked((hash), (key)) = (value); } while (0)
#define ihash_remove(hash, key, ret_key, ret_value) ihash_remove((hash), (key), (ret_key), (ret_value))
#define CHECK_ihash_lookup(hash, key, value)				\
	do {								\
		const int *_entry = ihash_lookup((hash), (key));	\
		CHECK(_entry);						\
		CHECK(*_entry == (value));				\
	} while (0)
#else
#define ihash_insert_unchecked(hash, key, value) ihash_insert_unchecked((hash), (key))
#define ihash_remove(hash, key, ret_key, ret_value) ((void)(ret_value), ihash_remove((hash), (key), (ret_key)))
#define CHECK_ihash_lookup(hash, key, value)				\
	do {								\
		(void)(value);						\
		const int *_entry = ihash_lookup((hash), (key));	\
		CHECK(_entry);						\
		CHECK(*_entry == (key));				\
	} while (0)
#endif

static bool random_test(uint64_t random_seed)
{
	struct ihash ihash;
	ihash_init(&ihash);

	struct itree itree;
	itree_init(&itree);

	struct random_state rng;
	random_state_init(&rng, random_seed);

	size_t num_entries = 0;
	_attr_unused size_t num_inserted = 0;
	_attr_unused size_t num_removed = 0;

	for (unsigned int iteration = 0; iteration < (1u << 14); iteration++) {
		uint64_t random = random_next_u64(&rng);
		int32_t x = random % (1 << 14);
		int32_t r = random >> 32;
		if (r > 0) {
			bool inserted = itree_insert(&itree, x);
#ifdef MAP
			struct ihash_insert_result res = ihash_insert(&ihash, x);
			CHECK(inserted == res.inserted_new_entry);
			if (res.inserted_new_entry) {
				*res.value = x;
			}
			CHECK(*res.value == x);
#else
			bool inserted2 = ihash_insert(&ihash, x);
			CHECK(inserted2 == inserted);
#endif
			if (inserted) {
				num_entries++;
				num_inserted++;
			}
		} else {
			bool removed = itree_delete(&itree, x, NULL);
			int key, value;
			bool removed2 = ihash_remove(&ihash, x, &key, &value);
			CHECK(removed == removed2);
			if (removed) {
				CHECK(key == x);
#ifdef MAP
				CHECK(value == x);
#endif
				num_entries--;
				num_removed++;
			}
		}

		CHECK(ihash_num_entries(&ihash) == num_entries);

		const unsigned int interval = 128;
		if (iteration % interval == interval - 1) {
			itree_iter_t iter;
			for (const int *key = itree_iter_start_leftmost(&iter, &itree);
			     key;
			     key = itree_iter_next(&iter)) {
				const int *entry = ihash_lookup(&ihash, *key);
				CHECK(entry);
				CHECK(*entry == *key);
			}
			for (ihash_iter_t iter = ihash_iter_start(&ihash);
			     ihash_iter_advance(&iter);) {
#ifdef MAP
				CHECK(*ihash_iter_value(&iter) == *ihash_iter_key(&iter));
#endif
				CHECK(itree_find(&itree, *ihash_iter_key(&iter)));
			}
			// test_log("%u: %zu %zu %zu\n", iteration, num_entries, num_inserted, num_removed);
		}
	}

	ihash_destroy(&ihash);
	itree_destroy(&itree);

	return true;
}

static bool insert_test(void)
{
	struct ihash hash;
	ihash_init(&hash);

	CHECK(ihash_num_entries(&hash) == 0);

#ifdef MAP
	struct ihash_insert_result res = ihash_insert(&hash, 0);
	CHECK(res.inserted_new_entry);
	*res.value = 0;
#else
	bool inserted = ihash_insert(&hash, 0);
	CHECK(inserted);
#endif
	CHECK(ihash_num_entries(&hash) == 1);

#ifdef MAP
	res = ihash_insert(&hash, 0);
	CHECK(!res.inserted_new_entry);
	CHECK(*res.value == 0);
	*res.value = 1;
#else
	inserted = ihash_insert(&hash, 0);
	CHECK(!inserted);
#endif
	CHECK(ihash_num_entries(&hash) == 1);

	for (int i = 1; i < 10000; i++) {
#ifdef MAP
		res = ihash_insert(&hash, i);
		CHECK(res.inserted_new_entry);
		*res.value = i + 1;
		res = ihash_insert(&hash, i);
		CHECK(!res.inserted_new_entry);
		CHECK(*res.value == i + 1);
#else
		inserted = ihash_insert(&hash, i);
		CHECK(inserted);
		inserted = ihash_insert(&hash, i);
		CHECK(!inserted);
#endif
		CHECK(ihash_num_entries(&hash) == i + 1u);
	}

	for (int i = 0; i < 10000; i++) {
#ifdef MAP
		res = ihash_insert(&hash, i);
		CHECK(!res.inserted_new_entry);
		CHECK(*res.value == i + 1);
#else
		inserted = ihash_insert(&hash, i);
		CHECK(!inserted);
#endif
		CHECK_ihash_lookup(&hash, i, i + 1);

		CHECK(ihash_num_entries(&hash) == 10000);
	}

	ihash_destroy(&hash);
	return true;
}

static bool lookup_test(void)
{
	struct ihash hash;
	ihash_init(&hash);

	for (int i = 0; i < 10000; i++) {
		const int *value = ihash_lookup(&hash, i);
		CHECK(!value);
	}

	for (int i = 0; i < 10000; i++) {
#ifdef MAP
		struct ihash_insert_result res = ihash_insert(&hash, i);
		CHECK(res.inserted_new_entry);
		*res.value = i;
#else
		bool inserted = ihash_insert(&hash, i);
		CHECK(inserted);
#endif
		const int *entry = ihash_lookup(&hash, i);
		CHECK(entry);
		CHECK(*entry == i);
	}

	for (int i = 0; i < 10000; i++) {
		const int *entry = ihash_lookup(&hash, i);
		CHECK(entry);
		CHECK(*entry == i);
	}

	for (int i = 10000; i < 20000; i++) {
		const int *entry = ihash_lookup(&hash, i);
		CHECK(!entry);
	}

	ihash_destroy(&hash);
	return true;
}

static bool remove_test(void)
{
	struct ihash hash;
	ihash_init(&hash);

	for (int i = 0; i < 10000; i++) {
		bool removed = ihash_remove(&hash, i, NULL, NULL);
		CHECK(!removed);
	}

	CHECK(ihash_num_entries(&hash) == 0);

	for (int i = 0; i < 10000; i++) {
#ifdef MAP
		struct ihash_insert_result res = ihash_insert(&hash, i);
		CHECK(res.inserted_new_entry);
		*res.value = i;
#else
		bool inserted = ihash_insert(&hash, i);
		CHECK(inserted);
#endif
	}

	CHECK(ihash_num_entries(&hash) == 10000);

	for (int i = 10000; i < 20000; i++) {
		bool removed = ihash_remove(&hash, i, NULL, NULL);
		CHECK(!removed);
	}

	CHECK(ihash_num_entries(&hash) == 10000);

	for (int i = 0; i < 10000; i++) {
		int key;
		int value;
		bool removed = ihash_remove(&hash, i, &key, &value);
		CHECK(removed);
		CHECK(key == i);
#ifdef MAP
		CHECK(value == i);
#endif
		CHECK(!ihash_lookup(&hash, i));
		CHECK(ihash_num_entries(&hash) == 10000 - i - 1u);
	}

	for (int i = 0; i < 10000; i++) {
		CHECK(!ihash_lookup(&hash, i));
	}

	CHECK(ihash_num_entries(&hash) == 0);

	for (int i = 0; i < 10000; i++) {
#ifdef MAP
		struct ihash_insert_result res = ihash_insert(&hash, i);
		CHECK(res.inserted_new_entry);
		*res.value = i;
#else
		bool inserted = ihash_insert(&hash, i);
		CHECK(inserted);
#endif

		int key;
		int value;
		bool removed = ihash_remove(&hash, i, &key, &value);
		CHECK(removed);
		CHECK(key == i);
#ifdef MAP
		CHECK(value == i);
#endif
		CHECK(!ihash_lookup(&hash, i));

		CHECK(ihash_num_entries(&hash) == 0);
	}

	ihash_destroy(&hash);
	return true;
}

static bool insert_unchecked_test(void)
{
	struct ihash hash;
	ihash_init_with_capacity(&hash, 100);

	CHECK(ihash_num_entries(&hash) == 0);

	for (int i = 0; i < 20000; i++) {
		ihash_insert_unchecked(&hash, i, i);
		CHECK_ihash_lookup(&hash, i, i);
		CHECK(ihash_num_entries(&hash) == i + 1u);
	}

	for (int i = 0; i < 20000; i++) {
		CHECK_ihash_lookup(&hash, i, i);
	}

	for (int i = 0; i < 10000; i++) {
		bool removed = ihash_remove(&hash, i, NULL, NULL);
		CHECK(removed);
	}

	for (int i = 0; i < 10000; i++) {
		ihash_insert_unchecked(&hash, i, i);
	}

	for (int i = 0; i < 20000; i++) {
		CHECK_ihash_lookup(&hash, i, i);
	}

	ihash_destroy(&hash);
	return true;
}

static bool iterator_test(void)
{
	struct ihash hash;
	ihash_init_with_capacity(&hash, 100);

	size_t n = 0;
	ihash_iter_t iter;
	for (iter = ihash_iter_start(&hash);
	     ihash_iter_advance(&iter);) {
		n++;
	}
	CHECK(n == 0);
	CHECK(!ihash_iter_advance(&iter));

	ihash_insert_unchecked(&hash, 123, 123);

	for (iter = ihash_iter_start(&hash);
	     ihash_iter_advance(&iter);) {
		CHECK(*ihash_iter_key(&iter) == 123);
#ifdef MAP
		CHECK(*ihash_iter_value(&iter) == 123);
#endif
		n++;
	}
	CHECK(n == 1);
	CHECK(!ihash_iter_advance(&iter));

	ihash_clear(&hash);

	n = 0;
	for (iter = ihash_iter_start(&hash);
	     ihash_iter_advance(&iter);) {
		n++;
	}
	CHECK(n == 0);
	CHECK(!ihash_iter_advance(&iter));
	CHECK(!ihash_iter_advance(&iter));

	for (int i = 0; i < 20000; i++) {
		ihash_insert_unchecked(&hash, i, i + 1);
	}

	bool *found = calloc(20000, sizeof(bool));
	n = 0;
	for (iter = ihash_iter_start(&hash);
	     ihash_iter_advance(&iter);) {
#ifdef MAP
		CHECK(*ihash_iter_key(&iter) == *ihash_iter_value(&iter) - 1);
		*ihash_iter_value(&iter) = -123;
#endif
		found[*ihash_iter_key(&iter)] = true;
		n++;
	}
	CHECK(!ihash_iter_advance(&iter));
	CHECK(!ihash_iter_advance(&iter));
	CHECK(n == 20000);
	for (int i = 0; i < 20000; i++) {
		CHECK(found[i]);
		const int *entry = ihash_lookup(&hash, i);
		CHECK(entry);
#ifdef MAP
		CHECK(*entry == -123);
#endif
	}
	free(found);

	ihash_destroy(&hash);
	return true;
}

static bool init_destroy_clear_test(void)
{
	struct ihash hash;
	ihash_init(&hash);
	ihash_destroy(&hash);
	ihash_init_with_capacity(&hash, 100);
	CHECK(ihash_capacity(&hash) >= 100);
	ihash_destroy(&hash);
	ihash_init_with_capacity(&hash, 0);
	ihash_destroy(&hash);

	ihash_init(&hash);
	ihash_insert_unchecked(&hash, 123, 456);
	CHECK_ihash_lookup(&hash,123, 456);
	CHECK(ihash_capacity(&hash) > 0);
	ihash_destroy(&hash);

	ihash_init_with_capacity(&hash, 2);
	ihash_uint_t capacity = ihash_capacity(&hash);
	CHECK(capacity >= 2);
	ihash_insert_unchecked(&hash, 123, 456);
	CHECK_ihash_lookup(&hash, 123, 456);
	CHECK(ihash_capacity(&hash) == capacity);
	ihash_destroy(&hash);

	ihash_init_with_capacity(&hash, 0);
	ihash_insert_unchecked(&hash, 123, 456);
	CHECK_ihash_lookup(&hash, 123, 456);
	CHECK(ihash_capacity(&hash) >= 1);
	ihash_destroy(&hash);

	ihash_init(&hash);
	ihash_insert_unchecked(&hash, 123, 456);
	CHECK_ihash_lookup(&hash, 123, 456);
	capacity = ihash_capacity(&hash);
	ihash_clear(&hash);
	CHECK(ihash_num_entries(&hash) == 0);
	CHECK(ihash_capacity(&hash) == capacity);
	CHECK(!ihash_lookup(&hash, 123));
	ihash_destroy(&hash);

	ihash_init_with_capacity(&hash, 10000);
	CHECK(ihash_capacity(&hash) >= 10000);
	for (int i = 0; i < 20000; i++) {
		ihash_insert_unchecked(&hash, i, i + 1);
	}
	CHECK(ihash_num_entries(&hash) == 20000);
	capacity = ihash_capacity(&hash);
	CHECK(capacity >= 20000);
	ihash_clear(&hash);
	CHECK(ihash_num_entries(&hash) == 0);
	CHECK(ihash_capacity(&hash) == capacity);
	for (int i = 0; i < 20000; i++) {
		CHECK(!ihash_lookup(&hash, i));
	}
	ihash_destroy(&hash);

	return true;
}

static bool resize_test(void)
{
	struct ihash hash;
	ihash_init(&hash);
	ihash_resize(&hash, 0);
	ihash_insert_unchecked(&hash, 123, 456);
	CHECK_ihash_lookup(&hash, 123, 456);
	ihash_destroy(&hash);

	ihash_init_with_capacity(&hash, 1000);
	ihash_insert_unchecked(&hash, 1, 2);
	ihash_resize(&hash, 200);
	CHECK_ihash_lookup(&hash, 1, 2);
	ihash_insert_unchecked(&hash, 2, 3);
	CHECK_ihash_lookup(&hash, 2, 3);
	ihash_resize(&hash, 100);
	CHECK_ihash_lookup(&hash, 1, 2);
	CHECK_ihash_lookup(&hash, 2, 3);
	ihash_destroy(&hash);

	ihash_init_with_capacity(&hash, 100);
	ihash_insert_unchecked(&hash, 1, 2);
	ihash_resize(&hash, 1000);
	CHECK_ihash_lookup(&hash, 1, 2);
	ihash_insert_unchecked(&hash, 2, 3);
	CHECK_ihash_lookup(&hash, 2, 3);
	ihash_resize(&hash, 200);
	CHECK_ihash_lookup(&hash, 1, 2);
	CHECK_ihash_lookup(&hash, 2, 3);
	ihash_destroy(&hash);

	ihash_init(&hash);
	ihash_resize(&hash, 10000);
	ihash_uint_t capacity = ihash_capacity(&hash);
	CHECK(capacity >= 10000 && capacity < 20000);
	for (int i = 0; i < 10000; i++) {
		ihash_insert_unchecked(&hash, i, i + 1);
	}
	CHECK(ihash_capacity(&hash) == capacity);
	ihash_resize(&hash, 1000);
	CHECK(ihash_capacity(&hash) == capacity);
	for (int i = 0; i < 10000; i++) {
		CHECK_ihash_lookup(&hash, i, i + 1);
	}
	ihash_resize(&hash, 20000);
	capacity = ihash_capacity(&hash);
	CHECK(capacity >= 20000 && capacity < 40000);
	for (int i = 10000; i < 20000; i++) {
		ihash_insert_unchecked(&hash, i, i + 1);
	}
	ihash_resize(&hash, 20000);
	CHECK(ihash_capacity(&hash) == capacity);
	for (int i = 0; i < 20000; i++) {
		CHECK_ihash_lookup(&hash, i, i + 1);
	}
	for (int i = 0; i < 10000; i++) {
		CHECK(ihash_remove(&hash, i, NULL, NULL));
	}
	ihash_resize(&hash, 20000);
	CHECK(ihash_capacity(&hash) == capacity);
	for (int i = 10000; i < 20000; i++) {
		CHECK_ihash_lookup(&hash, i, i + 1);
	}
	ihash_resize(&hash, 10000);
	capacity = ihash_capacity(&hash);
	CHECK(capacity >= 10000 && capacity < 20000);
	for (int i = 0; i < 10000; i++) {
		ihash_insert_unchecked(&hash, i, i + 1);
	}
	capacity = ihash_capacity(&hash);
	CHECK(capacity >= 20000 && capacity < 40000);
	for (int i = 0; i < 20000; i += 2) {
		CHECK(ihash_remove(&hash, i, NULL, NULL));
	}
	ihash_resize(&hash, 0);
	capacity = ihash_capacity(&hash);
	CHECK(capacity >= 10000 && capacity < 20000);
	for (int i = 0; i < 20000; i++) {
		if (i % 2 == 0) {
			CHECK(!ihash_lookup(&hash, i));
		} else {
			CHECK_ihash_lookup(&hash, i, i + 1);
		}
	}
	ihash_destroy(&hash);
	return true;
}

static uint32_t string_hash(const char *s)
{
	const char *hash_key = "0123456789abcdef";
	return siphash13_64(s, strlen(s), (const unsigned char *)hash_key).u64;
}

static void string_free(char *s)
{
	free(s);
}

#ifdef MAP
DEFINE_HASHMAP(shash, char *, char *, string_free, string_free, string_hash, strcmp(a, b) == 0);
#else
DEFINE_HASHSET(shash, char *, string_free, string_hash, strcmp(a, b) == 0);
#endif

#ifdef MAP
#define shash_insert_unchecked(hash, key, value)			\
	do { *shash_insert_unchecked(&shash, (key)) = (value); } while (0)
#define shash_remove(hash, key, ret_key, ret_value) shash_remove((hash), (key), (ret_key), (ret_value))
#else
#define shash_insert_unchecked(hash, key, value) shash_insert_unchecked(&shash, (key))
#define shash_remove(hash, key, ret_key, ret_value) ((void)(ret_value), shash_remove((hash), (key), (ret_key)))
#endif

static bool destructors_test(void)
{
	struct shash shash;

	shash_init(&shash);
	shash_destroy(&shash);

	shash_init(&shash);
	shash_insert_unchecked(&shash, strdup("a"), strdup("b"));
	shash_destroy(&shash);

	shash_init(&shash);
	shash_insert_unchecked(&shash, strdup("a"), strdup("d"));
	shash_insert_unchecked(&shash, strdup("b"), strdup("e"));
	shash_insert_unchecked(&shash, strdup("c"), strdup("f"));
	shash_destroy(&shash);

	shash_init(&shash);
	shash_insert_unchecked(&shash, strdup("a"), strdup("b"));
	shash_remove(&shash, "a", NULL, NULL);
	shash_insert_unchecked(&shash, strdup("a"), strdup("b"));
	char *key;
	shash_remove(&shash, "a", &key, NULL);
	CHECK(strcmp(key, "a") == 0);
	free(key);
	shash_insert_unchecked(&shash, strdup("a"), strdup("b"));
	char *value;
	shash_remove(&shash, "a", NULL, &value);
#ifdef MAP
	CHECK(strcmp(value, "b") == 0);
	free(value);
#endif
	shash_insert_unchecked(&shash, strdup("a"), strdup("b"));
	shash_remove(&shash, "a", &key, &value);
	CHECK(strcmp(key, "a") == 0);
	free(key);
#ifdef MAP
	CHECK(strcmp(value, "b") == 0);
	free(value);
#endif
	shash_destroy(&shash);

	shash_init(&shash);
	shash_insert_unchecked(&shash, strdup("a"), strdup("d"));
	shash_insert_unchecked(&shash, strdup("b"), strdup("e"));
	shash_insert_unchecked(&shash, strdup("c"), strdup("f"));
	shash_clear(&shash);
	shash_destroy(&shash);

	return true;
}

static bool fortify1_test(void)
{
	struct ihash ihash;
	ihash_init(&ihash);

	ihash_insert_unchecked(&ihash, 1, 1);
	ihash_insert_unchecked(&ihash, 2, 2);
	ihash_insert_unchecked(&ihash, 3, 3);

	for (ihash_iter_t iter = ihash_iter_start(&ihash);
	     ihash_iter_advance(&iter);) {
		ihash_insert_unchecked(&ihash, 4, 4);
	}

	ihash_destroy(&ihash);
	return true;
}

static bool fortify2_test(void)
{
	struct ihash ihash;
	ihash_init(&ihash);

	ihash_insert_unchecked(&ihash, 1, 1);
	ihash_insert_unchecked(&ihash, 2, 2);
	ihash_insert_unchecked(&ihash, 3, 3);

	for (ihash_iter_t iter = ihash_iter_start(&ihash);
	     ihash_iter_advance(&iter);) {
		ihash_insert_unchecked(&ihash, 4, 4);
		CHECK(*ihash_iter_key(&iter) <= 3);
		break;
	}

	ihash_destroy(&ihash);
	return true;
}

static bool fortify3_test(void)
{
	struct ihash ihash;
	ihash_init(&ihash);

	ihash_insert_unchecked(&ihash, 1, 1);
	ihash_insert_unchecked(&ihash, 2, 2);
	ihash_insert_unchecked(&ihash, 3, 3);

	for (ihash_iter_t iter = ihash_iter_start(&ihash);
	     ihash_iter_advance(&iter);) {
		ihash_remove(&ihash, 1, NULL, NULL);
	}

	ihash_destroy(&ihash);
	return true;
}

static bool fortify4_test(void)
{
	struct ihash ihash;
	ihash_init(&ihash);

	ihash_insert_unchecked(&ihash, 1, 1);
	ihash_insert_unchecked(&ihash, 2, 2);
	ihash_insert_unchecked(&ihash, 3, 3);

	for (ihash_iter_t iter = ihash_iter_start(&ihash);
	     ihash_iter_advance(&iter);) {
		ihash_insert(&ihash, 1);
	}

	ihash_destroy(&ihash);
	return true;
}

static bool fortify5_test(void)
{
	struct ihash ihash;
	ihash_init(&ihash);

	ihash_insert_unchecked(&ihash, 1, 1);
	ihash_insert_unchecked(&ihash, 2, 2);
	ihash_insert_unchecked(&ihash, 3, 3);

	for (ihash_iter_t iter = ihash_iter_start(&ihash);
	     ihash_iter_advance(&iter);) {
		ihash_clear(&ihash);
	}

	ihash_destroy(&ihash);
	return true;
}

static bool fortify6_test(void)
{
	struct ihash ihash;
	ihash_init(&ihash);

	ihash_insert_unchecked(&ihash, 1, 1);
	ihash_insert_unchecked(&ihash, 2, 2);
	ihash_insert_unchecked(&ihash, 3, 3);

	for (ihash_iter_t iter = ihash_iter_start(&ihash);
	     ihash_iter_advance(&iter);) {
		ihash_resize(&ihash, 100);
	}

	ihash_destroy(&ihash);
	return true;
}

static bool fortify7_test(void)
{
	struct ihash ihash;
	ihash_init(&ihash);

	ihash_insert_unchecked(&ihash, 1, 1);

	for (ihash_iter_t iter = ihash_iter_start(&ihash);
	     ihash_iter_advance(&iter);) {
		ihash_destroy(&ihash);
	}

	return true;
}
