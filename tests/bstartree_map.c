#define BSTAR
#define MAP
#include "btree_test.h"

SIMPLE_TEST(bstartree_map)
{
	return bstartree_map_test();
}

RANDOM_TEST(bstartree_map_random, random_seed, 2)
{
	return bstartree_map_random_test(random_seed);
}

SIMPLE_TEST(bstartree_map_allocated_strings)
{
	return bstartree_map_allocated_strings_test();
}

SIMPLE_TEST(bstartree_map_alignment)
{
	return bstartree_map_alignment_test();
}

SIMPLE_TEST(bstartree_map_bulk_loading)
{
	return bstartree_map_bulk_loading_test();
}
