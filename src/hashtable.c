/*
 * Copyright (C) 2024 Fabian Hügel
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <assert.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include "hashtable.h"
#include "compiler.h"

// TODO make an ordered hashtable (insertion order) (see python dict)

// TODO make a linked hashtable with inline links like:
// struct hash_node {
//     struct rb_node rb_node;
//     struct list_head list_head;
// };

/* Memory layout:
 * For in-place resizing the memory layout needs to look like this (e=entry, m=metadata):
 * eeeeemmmmm
 * So that when we double the capacity we get this:
 * eeeeemmmmm
 * eeeeeeeeeemmmmmmmmmm
 * Notice how the old entries remain in the same place!
 * (The old metadata gets copied to the back early on, so it's fine to overwrite it.
 *  But we don't want to copy any entries since those tend to be bigger.)
 */

#ifdef __HASHTABLE_PROFILING
size_t lookup_found_search_length;
size_t lookup_notfound_search_length;
size_t num_lookups_found;
size_t num_lookups_notfound;
size_t collisions1;
size_t collisions2;
size_t num_inserts;
#endif

// TODO put this into utils
static _hashtable_uint_t hashtable_round_capacity(_hashtable_uint_t capacity)
{
	// round to next power of 2
	capacity--;
	capacity |= capacity >> 1;
	capacity |= capacity >> 2;
	capacity |= capacity >> 4;
	capacity |= capacity >> 8;
	capacity |= capacity >> 16;
	capacity |= capacity >> (sizeof(capacity) * 4);
	capacity++;
	return capacity;
}

// static void check(const unsigned int t)
// {
// 	for (unsigned int c = 8; c != 0; c++) {
// 		unsigned int n = (c / 10) * t + (c % 10) * t / 10;
// 		unsigned int m = (unsigned int)((double)c * (0.1 * t));
// 		unsigned int c2 = (n / t) * 10 + ((n % t) * 10 + t - 1) / t;
// 		unsigned int d = ceil(10.0 / t * n);
// 		unsigned int n2 = (c2 / 10) * t + (c2 % 10) * t / 10;
// 		// printf("%u %u %u\n", c, c2, x); if (c == 100) break;
// 		assert(n == m);
// 		assert(c2 == d);
// 		assert(n == n2);
// 	}
// }

// static _hashtable_uint_t hashtable_min_capacity(_hashtable_uint_t num_entries,
// 							      const struct _hashtable_info *info)
// {
// 	return (num_entries / info->threshold) * 10 +
// 		((num_entries % info->threshold) * 10 + info->threshold - 1) / info->threshold;
// }

static _hashtable_uint_t hashtable_max_entries(_hashtable_uint_t capacity,
					       const struct _hashtable_info *info)
{
	return (capacity / 10) * info->threshold + (capacity % 10) * info->threshold / 10;
}

static _hashtable_idx_t hashtable_hash_to_index(const struct _hashtable *table,
						_hashtable_hash_t hash)
{
	_hashtable_idx_t h = hash;

#if 1
	// this is really bad for bad hash functions
	return h & (table->capacity - 1);
#elif 0
	// this helps with bad hash functions but hurts performance for integer keys with identity hash
	return (11 * h) & (table->capacity - 1);
#else
	// this helps a lot with bad hash functions but is even more expensive
	const size_t shift_amount = __builtin_clzll(table->capacity) + 1;
	// const size_t shift_amount = table->hash_to_index_shift;
	h ^= h >> shift_amount;
	h *= sizeof(h) == 8 ? 11400714819323198485llu : 2654435769;
	// h *= sizeof(h) == 8 ? 7046029254386353131llu : 1640531527;
	return h >> shift_amount;
#endif
}

#define HASHTABLE_EMPTY_HASH ((_hashtable_hash_t)-1)
#define HASHTABLE_TOMBSTONE_HASH ((_hashtable_hash_t)-2)
#define HASHTABLE_MAX_VALID_HASH ((_hashtable_hash_t)-3)

typedef struct _hashtable_metadata {
	_hashtable_hash_t _hash;
} hashtable_metadata_t;

static inline _hashtable_hash_t hashtable_sanitize_hash(_hashtable_hash_t hash)
{
	hash += hash > HASHTABLE_MAX_VALID_HASH ? 2 : 0;
	return hash;
}

static inline bool hashtable_metadata_is_vacant(const hashtable_metadata_t *m)
{
	return m->_hash > HASHTABLE_MAX_VALID_HASH;
}

static inline bool hashtable_metadata_is_empty(const hashtable_metadata_t *m)
{
	return m->_hash == HASHTABLE_EMPTY_HASH;
}

static inline bool hashtable_metadata_is_tombstone(const hashtable_metadata_t *m)
{
	return m->_hash == HASHTABLE_TOMBSTONE_HASH;
}

static inline _hashtable_hash_t hashtable_metadata_get_hash(const hashtable_metadata_t *m)
{
	// assert(m->_hash >= HASHTABLE_MIN_VALID_HASH);
	return m->_hash;
}

static inline void hashtable_metadata_set_empty(hashtable_metadata_t *m)
{
	m->_hash = HASHTABLE_EMPTY_HASH;
}

static inline void hashtable_metadata_set_tombstone(hashtable_metadata_t *m)
{
	m->_hash = HASHTABLE_TOMBSTONE_HASH;
}

static inline void hashtable_metadata_set_hash(hashtable_metadata_t *m, _hashtable_hash_t hash)
{
	// assert(hash >= HASHTABLE_MIN_VALID_HASH);
	m->_hash = hash;
}

struct _hashtable_probe_iter {
	_hashtable_idx_t index;
	// _hashtable_idx_t start;
	_hashtable_uint_t increment;
	_hashtable_uint_t mask;
};

static struct _hashtable_probe_iter hashtable_probe_iter_start(const struct _hashtable *table,
							       _hashtable_hash_t hash)
{
	_hashtable_idx_t start = hashtable_hash_to_index(table, hash);
	struct _hashtable_probe_iter iter = {
		.index = start,
		// .start = start,
		.increment = 0,
		.mask = table->capacity - 1,
	};
	return iter;
}

static void hashtable_probe_iter_advance(struct _hashtable_probe_iter *iter)
{
	// http://www.chilton-computing.org.uk/acl/literature/reports/p012.htm
	iter->increment++;
	iter->index = (iter->index + iter->increment) & iter->mask;
	// iter->index = (iter->start + ((iter->increment + 1) * iter->increment) / 2) & iter->mask;
}

static _hashtable_uint_t hashtable_metadata_offset(_hashtable_uint_t capacity,
						   const struct _hashtable_info *info)
{
	return capacity * info->entry_size;
}

static hashtable_metadata_t *hashtable_metadata(const struct _hashtable *table, _hashtable_idx_t index,
						const struct _hashtable_info *info)
{
	(void)info;
	return &table->metadata[index];
}

static void hashtable_realloc_storage(struct _hashtable *table, const struct _hashtable_info *info)
{
	assert((table->capacity & (table->capacity - 1)) == 0);
	_hashtable_uint_t size = info->entry_size + sizeof(hashtable_metadata_t);
	assert(((_hashtable_uint_t)-1) / size >= table->capacity);
	size *= table->capacity;
	if (size == 0) {
		if (table->storage) {
			free(table->storage);
			table->storage = NULL;
		}
	} else {
		table->storage = realloc(table->storage, size);
	}
	if (unlikely(!table->storage && table->capacity != 0)) {
		abort();
	}
	if (table->storage) {
		table->metadata = (hashtable_metadata_t *)(table->storage +
							   hashtable_metadata_offset(table->capacity, info));
	} else {
		table->metadata = NULL;
	}
	table->max_entries = hashtable_max_entries(table->capacity, info);
}

void _hashtable_init(struct _hashtable *table, _hashtable_uint_t capacity,
		     const struct _hashtable_info *info)
{
	capacity = hashtable_round_capacity(capacity);
	table->storage = NULL;
	table->capacity = capacity;
	table->num_entries = 0;
	table->num_tombstones = 0;
	hashtable_realloc_storage(table, info);
	for (_hashtable_uint_t i = 0; i < capacity; i++) {
		hashtable_metadata_t *m = hashtable_metadata(table, i, info);
		hashtable_metadata_set_empty(m);
	}
}

void _hashtable_destroy(struct _hashtable *table)
{
	free(table->storage);
	memset(table, 0, sizeof(*table));
}

bool _hashtable_lookup_bucket(const struct _hashtable *table, const void *key, _hashtable_hash_t hash,
			      struct _hashtable_bucket *bucket, const struct _hashtable_info *info)
{
	if (unlikely(table->capacity == 0)) {
		return false;
	}
#ifdef __HASHTABLE_PROFILING
	_hashtable_uint_t search_length = 0;
#endif
	hash = hashtable_sanitize_hash(hash);
	for (struct _hashtable_probe_iter iter = hashtable_probe_iter_start(table, hash);;
	     hashtable_probe_iter_advance(&iter)) {
#ifdef __HASHTABLE_PROFILING
		search_length++;
#endif
		_hashtable_idx_t index = iter.index;
		hashtable_metadata_t *m = hashtable_metadata(table, index, info);
		void *entry = _hashtable_entry(table, index, info);
		if (hashtable_metadata_is_empty(m)) {
			break;
		}
		if (hash == hashtable_metadata_get_hash(m) && info->keys_match(key, entry)) {
			bucket->metadata = m;
			bucket->entry = entry;
#ifdef __HASHTABLE_PROFILING
			num_lookups_found++;
			lookup_found_search_length += search_length;
#endif
			return true;
		}
	}
#ifdef __HASHTABLE_PROFILING
	num_lookups_notfound++;
	lookup_notfound_search_length += search_length;
#endif
	bucket->metadata = NULL;
	bucket->entry = NULL;
	return false;
}

_hashtable_idx_t _hashtable_get_next(const struct _hashtable *table, _hashtable_idx_t start,
				     const struct _hashtable_info *info)
{
	for (_hashtable_idx_t index = start; index < table->capacity; index++) {
		hashtable_metadata_t *m = hashtable_metadata(table, index, info);
		if (!hashtable_metadata_is_vacant(m)) {
			return index;
		}
	}
	return table->capacity;
}

static bool hashtable_slot_needs_rehash(uint32_t *bitmap, _hashtable_idx_t index)
{
	return !(bitmap[index / 32] & (1u << (index % 32)));
}

static void hashtable_slot_clear_needs_rehash(uint32_t *bitmap, _hashtable_idx_t index)
{
	bitmap[index / 32] |= 1u << (index % 32);
}

static bool hashtable_insert_during_resize(struct _hashtable *table, _hashtable_hash_t *phash,
					   void *entry, uint32_t *bitmap,
					   const struct _hashtable_info *info)
{
	for (struct _hashtable_probe_iter iter = hashtable_probe_iter_start(table, *phash);;
	     hashtable_probe_iter_advance(&iter)) {
		_hashtable_idx_t index = iter.index;
		hashtable_metadata_t *m = hashtable_metadata(table, index, info);
		if (hashtable_metadata_is_vacant(m)) {
			hashtable_slot_clear_needs_rehash(bitmap, index);
			hashtable_metadata_set_hash(m, *phash);
			memcpy(_hashtable_entry(table, index, info), entry, info->entry_size);
			return false;
		}

		if (hashtable_slot_needs_rehash(bitmap, index)) {
			hashtable_slot_clear_needs_rehash(bitmap, index);
			// if (_hashtable_hash_to_index(table, m->hash) == index) {
			// 	continue;
			// }
			void *tmp_entry = alloca(info->entry_size);
			_hashtable_hash_t tmp_hash = hashtable_metadata_get_hash(m);
			memcpy(tmp_entry, _hashtable_entry(table, index, info), info->entry_size);

			hashtable_metadata_set_hash(m, *phash);
			memcpy(_hashtable_entry(table, index, info), entry, info->entry_size);

			*phash = tmp_hash;
			memcpy(entry, tmp_entry, info->entry_size);

			return true;
		}
	}
}

static void hashtable_resize_common(struct _hashtable *table, _hashtable_uint_t old_capacity,
				    const struct _hashtable_info *info)
{
	size_t max_capacity = old_capacity > table->capacity ? old_capacity : table->capacity;
	size_t bitmap_size = (max_capacity + 31) / 32 * sizeof(uint32_t);
	uint32_t *bitmap, *bitmap_to_free = NULL;
	if (bitmap_size <= 1024) {
		bitmap = alloca(bitmap_size);
		memset(bitmap, 0, bitmap_size);
	} else {
		bitmap = calloc(1, bitmap_size);
		if (unlikely(!bitmap)) {
			abort();
		}
		bitmap_to_free = bitmap;
	}

	void *entry = alloca(info->entry_size);
#ifdef __HASHTABLE_PROFILING
	_hashtable_uint_t num_rehashed = 0;
#endif
	for (_hashtable_idx_t index = 0; index < old_capacity; index++) {
		hashtable_metadata_t *m = hashtable_metadata(table, index, info);
		if (hashtable_metadata_is_vacant(m)) {
			hashtable_metadata_set_empty(m);
			continue;
		}
		if (!hashtable_slot_needs_rehash(bitmap, index)) {
			continue;
		}
		_hashtable_hash_t hash = hashtable_metadata_get_hash(m);
		_hashtable_idx_t optimal_index = hashtable_hash_to_index(table, hash);
		if (optimal_index == index) {
			hashtable_slot_clear_needs_rehash(bitmap, index);
#ifdef __HASHTABLE_PROFILING
			num_rehashed++;
#endif
			continue;
		}
		hashtable_metadata_set_empty(m);
		memcpy(entry, _hashtable_entry(table, index, info), info->entry_size);

		bool need_rehash;
		do {
			need_rehash = hashtable_insert_during_resize(table, &hash, entry, bitmap, info);
#ifdef __HASHTABLE_PROFILING
			num_rehashed++;
#endif
		} while (need_rehash);
	}

	if (bitmap_to_free) {
		free(bitmap_to_free);
	}
}

static void hashtable_shrink(struct _hashtable *table, _hashtable_uint_t new_capacity,
			     const struct _hashtable_info *info)
{
	if (new_capacity == 0) {
		assert(table->num_entries == 0);
		free(table->storage);
		memset(table, 0, sizeof(*table));
		return;
	}
	assert(new_capacity <= table->capacity && new_capacity > table->num_entries);
	_hashtable_uint_t old_capacity = table->capacity;
	table->capacity = new_capacity;
	table->num_tombstones = 0;

	hashtable_resize_common(table, old_capacity, info);

	size_t new_metadata_offset = hashtable_metadata_offset(table->capacity, info);
	hashtable_metadata_t *new_metadata = (hashtable_metadata_t *)(table->storage + new_metadata_offset);
	memmove(new_metadata, table->metadata, old_capacity * sizeof(table->metadata[0]));
	hashtable_realloc_storage(table, info);
}

static void hashtable_grow(struct _hashtable *table, _hashtable_uint_t new_capacity,
			   const struct _hashtable_info *info)
{
	assert(new_capacity >= table->capacity && new_capacity > table->num_entries);

	_hashtable_uint_t old_capacity = table->capacity;
	table->capacity = new_capacity;
	table->num_tombstones = 0;
	hashtable_realloc_storage(table, info);
	size_t old_metadata_offset = hashtable_metadata_offset(old_capacity, info);
	hashtable_metadata_t *old_metadata = (hashtable_metadata_t *)(table->storage + old_metadata_offset);

	/* Need to use memmove in case the metadata is bigger than the entries:
	 * eeeeemmmmmmmmmm
	 * eeeeeeeeeemmmmmmmmmmmmmmmmmmmm
	 */
	memmove(table->metadata, old_metadata, old_capacity * sizeof(table->metadata[0]));
	for (_hashtable_uint_t i = old_capacity; i < table->capacity; i++) {
		hashtable_metadata_t *m = hashtable_metadata(table, i, info);
		hashtable_metadata_set_empty(m);
	}

	hashtable_resize_common(table, old_capacity, info);
}

void _hashtable_resize(struct _hashtable *table, _hashtable_uint_t new_capacity,
		       const struct _hashtable_info *info)
{
	if (new_capacity == 0) {
		if (table->capacity == 0) {
			return;
		}
		new_capacity = table->num_entries;
	}
	new_capacity = hashtable_round_capacity(new_capacity);
	while (hashtable_max_entries(new_capacity, info) < table->num_entries) {
		new_capacity *= 2;
	}
	if (new_capacity < table->capacity || new_capacity == 0) {
		hashtable_shrink(table, new_capacity, info);
	} else {
		hashtable_grow(table, new_capacity, info);
	}
}

static _attr_always_inline bool hashtable_insert(bool check_key, struct _hashtable *table,
						 const void *key, _hashtable_hash_t hash,
						 struct _hashtable_bucket *bucket,
						 const struct _hashtable_info *info)
{
#ifdef __HASHTABLE_PROFILING
	num_inserts++;
#endif
	if (table->num_entries + table->num_tombstones >= table->max_entries) {
		_hashtable_uint_t new_capacity = table->capacity == 0 ? 8 : 2 * table->capacity;
		hashtable_grow(table, new_capacity, info);
	}

	hash = hashtable_sanitize_hash(hash);

	hashtable_metadata_t *insert_bucket_metadata = NULL;
	void *insert_bucket_entry;
	for (struct _hashtable_probe_iter iter = hashtable_probe_iter_start(table, hash);;
	     hashtable_probe_iter_advance(&iter)) {
		_hashtable_idx_t index = iter.index;
		hashtable_metadata_t *m = hashtable_metadata(table, index, info);
		void *e = _hashtable_entry(table, index, info);
		if (hashtable_metadata_is_vacant(m)) {
			if (!check_key) {
				insert_bucket_metadata = m;
				insert_bucket_entry = e;
				break;
			}
			if (!insert_bucket_metadata) {
				insert_bucket_metadata = m;
				insert_bucket_entry = e;
			}
			if (hashtable_metadata_is_empty(m)) {
				break;
			}
		}

		if (check_key && hash == hashtable_metadata_get_hash(m) && info->keys_match(key, e)) {
			bucket->metadata = m;
			bucket->entry = e;
			return false;
		}

#ifdef __HASHTABLE_PROFILING
		if (_hashtable_hash_to_index(table, hash) == _hashtable_hash_to_index(table, m->hash)) {
			collisions2++;
		} else {
			collisions1++;
		}
#endif
	}
	// gcc generates much better code out of this than out of an if statement, clang doesn't care
	table->num_tombstones -= hashtable_metadata_is_tombstone(insert_bucket_metadata);
	hashtable_metadata_set_hash(insert_bucket_metadata, hash);
	bucket->metadata = insert_bucket_metadata;
	bucket->entry = insert_bucket_entry;
	table->num_entries++;
	return true;
}

void _hashtable_insert_bucket_unchecked(struct _hashtable *table, _hashtable_hash_t hash,
					struct _hashtable_bucket *bucket,
					const struct _hashtable_info *info)
{
	hashtable_insert(false, table, NULL, hash, bucket, info);
}

bool _hashtable_lookup_bucket_for_insertion(struct _hashtable *table, const void *key, _hashtable_hash_t hash,
					    struct _hashtable_bucket *bucket,
					    const struct _hashtable_info *info)
{
	return hashtable_insert(true, table, key, hash, bucket, info);
}

void _hashtable_bucket_remove_entry(struct _hashtable *table, struct _hashtable_bucket *bucket,
				    const struct _hashtable_info *info)
{
	if (unlikely(hashtable_metadata_is_vacant(bucket->metadata))) {
		return;
	}
	hashtable_metadata_set_tombstone(bucket->metadata);
	table->num_entries--;
	table->num_tombstones++;
	if (table->num_entries < table->capacity / 8) {
		hashtable_shrink(table, table->capacity / 4, info);
	} else if (table->num_tombstones > table->capacity / 2) {
		hashtable_grow(table, table->capacity, info);
	}
	bucket->metadata = NULL;
	bucket->entry = NULL;
}

void _hashtable_clear(struct _hashtable *table, const struct _hashtable_info *info)
{
	for (_hashtable_uint_t i = 0; i < table->capacity; i++) {
		hashtable_metadata_t *m = hashtable_metadata(table, i, info);
		hashtable_metadata_set_empty(m);
	}
	table->num_entries = 0;
	table->num_tombstones = 0;
}
