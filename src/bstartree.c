/*
 * Copyright (C) 2024 Fabian Hügel
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <alloca.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include "bstartree.h"
#include "compiler.h"

static unsigned int bstartree_root_max_items(const struct bstartree_info *info)
{
	return 2 * info->min_items;
}

static void *bstartree_node_item(struct _bstartree_node *node, unsigned int idx,
				 const struct bstartree_info *info)
{
	return node->data + info->key_alignment_offset + idx * info->item_size;
}

void *_bstartree_debug_node_item(struct _bstartree_node *node, unsigned int idx,
				 const struct bstartree_info *info)
{
	return bstartree_node_item(node, idx, info);
}

static struct _bstartree_node **bstartree_node_children(struct _bstartree_node *node,
							const struct bstartree_info *info)
{
	size_t max_items = node->root ? bstartree_root_max_items(info) : info->max_items;
	char *children = (char *)bstartree_node_item(node, max_items, info);
	children += node->root ? info->root_children_alignment_offset : info->children_alignment_offset;
	// assert((uintptr_t)children % sizeof(void *) == 0);
	return (struct _bstartree_node **)children;
}

static struct _bstartree_node *bstartree_node_get_child(struct _bstartree_node *node, unsigned int idx,
							const struct bstartree_info *info)
{
	return bstartree_node_children(node, info)[idx];
}

struct _bstartree_node *_bstartree_debug_node_get_child(struct _bstartree_node *node, unsigned int idx,
							const struct bstartree_info *info)
{
	return bstartree_node_get_child(node, idx, info);
}

static void bstartree_node_set_child(struct _bstartree_node *node, unsigned int idx,
				     struct _bstartree_node *child, const struct bstartree_info *info)
{
	bstartree_node_children(node, info)[idx] = child;
}

static void bstartree_node_copy_child(struct _bstartree_node *dest_node, unsigned int dest_idx,
				      struct _bstartree_node *src_node, unsigned int src_idx,
				      const struct bstartree_info *info)
{
	bstartree_node_set_child(dest_node, dest_idx, bstartree_node_get_child(src_node, src_idx, info), info);
}

static void bstartree_node_copy_children(struct _bstartree_node *dest_node, unsigned int dest_idx,
					 struct _bstartree_node *src_node, unsigned int src_idx,
					 unsigned int num_children, const struct bstartree_info *info)
{
	memcpy(bstartree_node_children(dest_node, info) + dest_idx,
	       bstartree_node_children(src_node, info) + src_idx,
	       num_children * sizeof(struct _bstartree_node *));
}

static void bstartree_node_get_item(void *item, struct _bstartree_node *node, unsigned int idx,
				    const struct bstartree_info *info)
{
	memcpy(item, bstartree_node_item(node, idx, info), info->item_size);
}

static void bstartree_node_set_item(struct _bstartree_node *node, unsigned int idx, const void *item,
				    const struct bstartree_info *info)
{
	memcpy(bstartree_node_item(node, idx, info), item, info->item_size);
}

static void bstartree_node_copy_items(struct _bstartree_node *dest_node, unsigned int dest_idx,
				      struct _bstartree_node *src_node, unsigned int src_idx,
				      unsigned int num_items, const struct bstartree_info *info)
{
	memcpy(bstartree_node_item(dest_node, dest_idx, info),
	       bstartree_node_item(src_node, src_idx, info),
	       num_items * info->item_size);
}

static void bstartree_node_copy_item(struct _bstartree_node *dest_node, unsigned int dest_idx,
				     struct _bstartree_node *src_node, unsigned int src_idx,
				     const struct bstartree_info *info)
{
	bstartree_node_set_item(dest_node, dest_idx, bstartree_node_item(src_node, src_idx, info), info);
}

static bool bstartree_node_search(struct _bstartree_node *node, const void *key, unsigned int *ret_idx,
				  const struct bstartree_info *info)
{
	unsigned int start = 0;
	unsigned int end = node->num_items;
	while (start + info->linear_search_threshold < end) {
		unsigned int mid = (start + end) / 2; // start + end should never overflow
		int cmp = info->cmp(key, bstartree_node_item(node, mid, info));
		if (cmp == 0) {
			*ret_idx = mid;
			return true;
		} else if (cmp > 0) {
			start = mid + 1;
		} else {
			end = mid;
		}
	}

	while (start < end) {
		int cmp = info->cmp(key, bstartree_node_item(node, start, info));
		if (cmp == 0) {
			*ret_idx = start;
			return true;
		}
		if (cmp < 0) {
			break;
		}
		start++;
		end--;
		cmp = info->cmp(key, bstartree_node_item(node, end, info));
		if (cmp == 0) {
			*ret_idx = end;
			return true;
		}
		if (cmp > 0) {
			start = end + 1;
			break;
		}
	}
	*ret_idx = start;
	return false;
}

void *_bstartree_iter_start(struct _bstartree_iter *iter, const struct _bstartree *tree, bool rightmost,
			    const struct bstartree_info *info)
{
	iter->tree = tree;
	iter->level = -1;

	if (tree->height == 0) {
		return NULL;
	}

	struct _bstartree_node *node = tree->root;
	struct _bstartree_pos *pos;
	for (unsigned int level = tree->height; level-- > 0;) {
		pos = &iter->path[level];
		pos->idx = rightmost ? node->num_items : 0;
		pos->node = node;
		node = level != 0 ? bstartree_node_get_child(node, pos->idx, info) : NULL;
	}
	iter->level = 0;
	if (rightmost) {
		pos->idx--;
	}
	return bstartree_node_item(pos->node, pos->idx, info);
}

void *_bstartree_iter_next(struct _bstartree_iter *iter, const struct bstartree_info *info)
{
	if (iter->level >= iter->tree->height) {
		return NULL;
	}
	struct _bstartree_pos *pos = &iter->path[iter->level];
	pos->idx++;
	// descend into the leftmost child of the right child
	while (iter->level != 0) {
		struct _bstartree_node *child = bstartree_node_get_child(pos->node, pos->idx, info);
		pos = &iter->path[--iter->level];
		pos->node = child;
		pos->idx = 0;
	}
	while (pos->idx >= pos->node->num_items) {
		if (++iter->level == iter->tree->height) {
			return NULL;
		}
		pos = &iter->path[iter->level];
	}
	return bstartree_node_item(pos->node, pos->idx, info);
}

void *_bstartree_iter_prev(struct _bstartree_iter *iter, const struct bstartree_info *info)
{
	if (iter->level >= iter->tree->height) {
		return NULL;
	}
	struct _bstartree_pos *pos = &iter->path[iter->level];
	// descend into the rightmost child of the left child
	while (iter->level != 0) {
		struct _bstartree_node *child = bstartree_node_get_child(pos->node, pos->idx, info);
		pos = &iter->path[--iter->level];
		pos->node = child;
		pos->idx = child->num_items;
	}
	while (pos->idx == 0) {
		if (++iter->level == iter->tree->height) {
			return NULL;
		}
		pos = &iter->path[iter->level];
	}
	pos->idx--;
	return bstartree_node_item(pos->node, pos->idx, info);
}

void *_bstartree_iter_start_at(struct _bstartree_iter *iter, const struct _bstartree *tree, void *key,
			       enum bstartree_iter_start_at_mode mode, const struct bstartree_info *info)
{
	iter->tree = tree;
	iter->level = tree->height - 1;

	if (tree->height == 0) {
		return NULL;
	}

	struct _bstartree_node *node = tree->root;
	struct _bstartree_pos *pos;
	for (;;) {
		pos = &iter->path[iter->level];
		pos->node = node;
		if (bstartree_node_search(node, key, &pos->idx, info)) {
			if (mode == BSTARTREE_ITER_LOWER_BOUND_EXCLUSIVE) {
				return _bstartree_iter_next(iter, info);
			} else if (mode == BSTARTREE_ITER_UPPER_BOUND_EXCLUSIVE) {
				return _bstartree_iter_prev(iter, info);
			}
			break;
		}
		if (iter->level == 0) {
			switch (mode) {
			case BSTARTREE_ITER_FIND_KEY:
				return NULL;
			case BSTARTREE_ITER_LOWER_BOUND_INCLUSIVE:
			case BSTARTREE_ITER_LOWER_BOUND_EXCLUSIVE:
				if (pos->idx == pos->node->num_items) {
					return _bstartree_iter_next(iter, info);
				}
				break;
			case BSTARTREE_ITER_UPPER_BOUND_INCLUSIVE:
			case BSTARTREE_ITER_UPPER_BOUND_EXCLUSIVE:
				return _bstartree_iter_prev(iter, info);
			}
			break;
		}
		node = bstartree_node_get_child(node, pos->idx, info);
		iter->level--;
	}
	return bstartree_node_item(pos->node, pos->idx, info);
}

static size_t bstartree_node_size(bool leaf, bool root, const struct bstartree_info *info)
{
	size_t max_items = root ? bstartree_root_max_items(info) : info->max_items;
	size_t size = sizeof(struct _bstartree_node);
	size += info->key_alignment_offset;
	size += max_items * info->item_size;
	if (!leaf || root) {
		size += root ? info->root_children_alignment_offset : info->children_alignment_offset;
		size += (max_items + 1) * sizeof(struct _bstartree_node *);
	}
	return size;
}

static struct _bstartree_node *bstartree_new_node(bool leaf, bool root, const struct bstartree_info *info)
{
	struct _bstartree_node *node;
	size_t size = bstartree_node_size(leaf, root, info);
	node = malloc(size);
	node->num_items = 0;
	node->root = root;
	// node->leaf = leaf;
	return node;
}

void _bstartree_init(struct _bstartree *tree)
{
	memset(tree, 0, sizeof(*tree));
}

void _bstartree_destroy(struct _bstartree *tree, const struct bstartree_info *info)
{
	if (tree->height == 0) {
		return;
	}
	struct _bstartree_pos path[__BSTARTREE_MAX_HEIGHT];
	struct _bstartree_node *node = tree->root;
	for (unsigned int level = tree->height; level-- > 0;) {
		path[level].idx = 0;
		path[level].node = node;
		node = level != 0 ? bstartree_node_get_child(node, 0, info) : NULL;
	}

	unsigned int level = 0;
	for (;;) {
		// start at leaf
		struct _bstartree_pos *pos = &path[level];
		// free the leaf and go up, if we are at the end of the current node free it and keep going up
		do {
			if (info->destroy_item) {
				for (unsigned int i = 0; i < pos->node->num_items; i++) {
					info->destroy_item(bstartree_node_item(pos->node, i, info));
				}
			}
			free(pos->node);
			if (++level == tree->height) {
				tree->root = NULL;
				tree->height = 0;
				return;
			}
			pos = &path[level];
		} while (pos->idx >= pos->node->num_items);
		// descend into the leftmost child of the right child
		pos->idx++;
		do {
			struct _bstartree_node *child = bstartree_node_get_child(pos->node, pos->idx, info);
			pos = &path[--level];
			pos->node = child;
			pos->idx = 0;
		} while (level != 0);
	}
}

void *_bstartree_find(const struct _bstartree *tree, const void *key, const struct bstartree_info *info)
{
	if (tree->height == 0) {
		return NULL;
	}
	struct _bstartree_node *node = tree->root;
	unsigned int level = tree->height - 1;
	for (;;) {
		unsigned int idx;
		if (bstartree_node_search(node, key, &idx, info)) {
			return bstartree_node_item(node, idx, info);
		}
		if (level-- == 0) {
			break;
		}
		node = bstartree_node_get_child(node, idx, info);
	}
	return NULL;
}

void *_bstartree_get_leftmost_rightmost(const struct _bstartree *tree, bool leftmost,
					const struct bstartree_info *info)
{
	if (tree->height == 0) {
		return NULL;
	}
	struct _bstartree_node *node = tree->root;
	unsigned int level = tree->height;
	while (--level != 0) {
		unsigned int idx = leftmost ? 0 : node->num_items;
		node = bstartree_node_get_child(node, idx, info);
	}
	unsigned int idx = leftmost ? 0 : node->num_items - 1;
	return bstartree_node_item(node, idx, info);
}

static void bstartree_node_shift_items_right(struct _bstartree_node *node, unsigned int idx,
					     const struct bstartree_info *info)
{
	memmove(bstartree_node_item(node, idx + 1, info),
		bstartree_node_item(node, idx, info),
		(node->num_items - idx) * info->item_size);
}

static void bstartree_node_shift_children_right(struct _bstartree_node *node, unsigned int idx,
						const struct bstartree_info *info)
{
	memmove(bstartree_node_children(node, info) + idx + 1,
		bstartree_node_children(node, info) + idx,
		(node->num_items + 1 - idx) * sizeof(struct _bstartree_node *));
}

static void bstartree_node_shift_items_left(struct _bstartree_node *node, unsigned int idx,
					    const struct bstartree_info *info)
{
	memmove(bstartree_node_item(node, idx, info),
		bstartree_node_item(node, idx + 1, info),
		(node->num_items - idx - 1) * info->item_size);
}

static void bstartree_node_shift_children_left(struct _bstartree_node *node, unsigned int idx,
					       const struct bstartree_info *info)
{
	memmove(bstartree_node_children(node, info) + idx,
		bstartree_node_children(node, info) + idx + 1,
		(node->num_items - idx) * sizeof(struct _bstartree_node *));
}

static void bstartree_node_insert_unchecked(struct _bstartree_node *node, unsigned int idx,
					    void *item, struct _bstartree_node *right,
					    const struct bstartree_info *info)
{
	// assert(node->num_items < (node->root ? bstartree_root_max_items(info) : info->max_items));
	bstartree_node_shift_items_right(node, idx, info);
	bstartree_node_set_item(node, idx, item, info);
	if (right) {
		bstartree_node_shift_children_right(node, idx + 1, info);
		bstartree_node_set_child(node, idx + 1, right, info);
	}
	node->num_items++;
}

static void bstartree_move_item_left(struct _bstartree_node *left, struct _bstartree_node *right,
				     struct _bstartree_node *parent, unsigned int idx_in_parent,
				     bool leaf, const struct bstartree_info *info)
{
	// assert(left == bstartree_node_get_child(parent, idx_in_parent, info));
	// assert(right == bstartree_node_get_child(parent, idx_in_parent + 1, info));
	// assert(left->num_items < info->max_items);
	// assert(right->num_items > info->min_items);

	bstartree_node_copy_item(left, left->num_items, parent, idx_in_parent, info);
	bstartree_node_copy_item(parent, idx_in_parent, right, 0, info);
	bstartree_node_shift_items_left(right, 0, info);
	if (!leaf) {
		bstartree_node_copy_child(left, left->num_items + 1, right, 0, info);
		bstartree_node_shift_children_left(right, 0, info);
	}
	right->num_items--;
	left->num_items++;
}

static void bstartree_move_item_right(struct _bstartree_node *left, struct _bstartree_node *right,
				      struct _bstartree_node *parent, unsigned int idx_in_parent,
				      bool leaf, const struct bstartree_info *info)
{
	// assert(left == bstartree_node_get_child(parent, idx_in_parent, info));
	// assert(right == bstartree_node_get_child(parent, idx_in_parent + 1, info));
	// assert(right->num_items < info->max_items);
	// assert(left->num_items > info->min_items);

	bstartree_node_shift_items_right(right, 0, info);
	bstartree_node_copy_item(right, 0, parent, idx_in_parent, info);
	bstartree_node_copy_item(parent, idx_in_parent, left, left->num_items - 1, info);
	if (!leaf) {
		bstartree_node_shift_children_right(right, 0, info);
		bstartree_node_copy_child(right, 0, left, left->num_items, info);
	}
	right->num_items++;
	left->num_items--;
}

static void bstartree_merge(struct _bstartree_node *left, struct _bstartree_node *mid,
			    struct _bstartree_node *right, struct _bstartree_node *parent,
			    unsigned int idx_in_parent, bool leaf, const struct bstartree_info *info)
{
	// assert(left == bstartree_node_get_child(parent, idx_in_parent - 1, info));
	// assert(mid == bstartree_node_get_child(parent, idx_in_parent, info));
	// assert(right == bstartree_node_get_child(parent, idx_in_parent + 1, info));
	// assert(left->num_items <= info->min_items);
	// assert(mid->num_items <= info->min_items);
	// assert(right->num_items <= info->min_items);
	// assert(left->num_items + mid->num_items + right->num_items == 3 * info->min_items - 1);

	unsigned int new_left_num_items = 3 * info->min_items / 2;
	unsigned int new_mid_num_items = 3 * info->min_items - new_left_num_items;

	bstartree_node_copy_item(left, left->num_items, parent, idx_in_parent - 1, info);
	unsigned int n = new_left_num_items - (left->num_items + 1);
	// assert(mid->num_items > n);
	bstartree_node_copy_items(left, left->num_items + 1, mid, 0, n, info);
	bstartree_node_copy_item(parent, idx_in_parent - 1, mid, n, info);
	n++;
	memmove(bstartree_node_item(mid, 0, info),
		bstartree_node_item(mid, n, info),
		(mid->num_items - n) * info->item_size);
	// assert(mid->num_items >= n);
	n = mid->num_items - n;
	bstartree_node_copy_item(mid, n, parent, idx_in_parent, info);
	n++;
	// assert(n + right->num_items == new_mid_num_items);
	bstartree_node_copy_items(mid, n, right, 0, right->num_items, info);

	bstartree_node_shift_items_left(parent, idx_in_parent, info);
	bstartree_node_shift_children_left(parent, idx_in_parent + 1, info);

	if (!leaf) {
		n = new_left_num_items - left->num_items;
		bstartree_node_copy_children(left, left->num_items + 1, mid, 0, n, info);
		memmove(bstartree_node_children(mid, info),
			bstartree_node_children(mid, info) + n,
			(mid->num_items + 1 - n) * sizeof(struct _bstartree_node *));
		n = mid->num_items + 1 - n;
		bstartree_node_copy_children(mid, n, right, 0, right->num_items + 1, info);
		// assert(n + right->num_items + 1 == new_mid_num_items + 1);
	}

	parent->num_items--;
	left->num_items = new_left_num_items;
	mid->num_items = new_mid_num_items;
	free(right);
}

static void bstartree_merge_into_root(struct _bstartree *tree, bool leaf, const struct bstartree_info *info)
{
	struct _bstartree_node *root = tree->root;
	struct _bstartree_node *left = bstartree_node_get_child(root, 0, info);
	struct _bstartree_node *right = bstartree_node_get_child(root, 1, info);
	// assert(root->num_items == 1);
	// assert(left->num_items <= info->min_items);
	// assert(right->num_items <= info->min_items);
	// assert(left->num_items + right->num_items == 2 * info->min_items - 1);
	// assert(left->leaf == right->leaf);

	bstartree_node_copy_item(root, left->num_items, root, 0, info);
	bstartree_node_copy_items(root, 0, left, 0, left->num_items, info);
	bstartree_node_copy_items(root, left->num_items + 1, right, 0, right->num_items, info);

	if (!leaf) {
		bstartree_node_copy_children(root, 0, left, 0, left->num_items + 1, info);
		bstartree_node_copy_children(root, left->num_items + 1, right, 0, right->num_items + 1, info);
	}

	root->num_items = 2 * info->min_items;
	// root->leaf = leaf;
	tree->height--;
	free(left);
	free(right);
}

bool _bstartree_delete(struct _bstartree *tree, enum _bstartree_deletion_mode mode, const void *key,
		       void *ret_item, const struct bstartree_info *info)
{
	if (tree->height == 0) {
		return false;
	}
	struct _bstartree_node *node = tree->root;
	unsigned int level = tree->height - 1;
	struct _bstartree_pos path[__BSTARTREE_MAX_HEIGHT];
	unsigned int idx;
	bool leaf = false;
	for (;;) {
		leaf = level == 0;
		bool found = false;
		switch (mode) {
		case __BSTARTREE_DELETE_KEY:
			found = bstartree_node_search(node, key, &idx, info);
			if (leaf && !found) {
				return false;
			}
			break;
		case __BSTARTREE_DELETE_MIN:
			idx = 0;
			break;
		case __BSTARTREE_DELETE_MAX:
			idx = leaf ? node->num_items - 1 : node->num_items;
			break;
		}
		// TODO maybe move most of this code into the switch
		if (leaf) {
			break;
		}
		if (found) {
			// we found the key in an internal node
			// now return it and then replace it with the max value of the left substartree
			if (ret_item) {
				bstartree_node_get_item(ret_item, node, idx, info);
			} else if (info->destroy_item) {
				info->destroy_item(bstartree_node_item(node, idx, info));
			}
			ret_item = bstartree_node_item(node, idx, info);
			mode = __BSTARTREE_DELETE_MAX;
		}
		path[level].idx = idx;
		path[level].node = node;
		level--;
		node = bstartree_node_get_child(node, idx, info);
	}

	// we are at the leaf and have found the item to delete
	// return the item and rebalance

	if (ret_item) {
		bstartree_node_get_item(ret_item, node, idx, info);
	} else if (info->destroy_item) {
		info->destroy_item(bstartree_node_item(node, idx, info));
	}
	bstartree_node_shift_items_left(node, idx, info);
	// assert(node->num_items != 0);
	node->num_items--;

	for (leaf = true; ++level != tree->height; leaf = false) {
		if (node->num_items >= info->min_items) {
			return true;
		}

		node = path[level].node;
		idx = path[level].idx;

		struct _bstartree_node *left = NULL;
		struct _bstartree_node *mid = bstartree_node_get_child(node, idx, info);
		struct _bstartree_node *right = NULL;
		if (idx != 0) {
			left = bstartree_node_get_child(node, idx - 1, info);
		}
		if (idx != node->num_items) {
			right = bstartree_node_get_child(node, idx + 1, info);
		}

		// assert(left || right);

		if (left && left->num_items > info->min_items) {
			bstartree_move_item_right(left, mid, node, idx - 1, leaf, info);
			continue;
		}

		if (right && right->num_items > info->min_items) {
			bstartree_move_item_left(mid, right, node, idx, leaf, info);
			continue;
		}

		if (level == tree->height - 1u && node->num_items == 1) {
			// assert(node == tree->root);
			bstartree_merge_into_root(tree, leaf, info);
			continue;
		}

		// assert(info->min_items >= 2);

		if (!left) {
			// assert(idx == 0);
			// assert(node->num_items >= 2);
			struct _bstartree_node *rightright = bstartree_node_get_child(node, idx + 2, info);
			if (rightright->num_items > info->min_items) {
				bstartree_move_item_left(right, rightright, node, idx + 1, leaf, info);
				bstartree_move_item_left(mid, right, node, idx, leaf, info);
				continue;
			}
			left = mid;
			mid = right;
			right = rightright;
			idx++;
		} else if (!right) {
			// assert(idx == node->num_items);
			// assert(idx >= 2);
			struct _bstartree_node *leftleft = bstartree_node_get_child(node, idx - 2, info);
			if (leftleft->num_items > info->min_items) {
				bstartree_move_item_right(leftleft, left, node, idx - 2, leaf, info);
				bstartree_move_item_right(left, mid, node, idx - 1, leaf, info);
				continue;
			}
			right = mid;
			mid = left;
			left = leftleft;
			idx--;
		}

		bstartree_merge(left, mid, right, node, idx, leaf, info);
	}

	// assert(node == tree->root);

	if (node->num_items == 0) {
		tree->root = NULL;
		// assert(tree->height == 1);
		tree->height = 0;
		free(node);
	}

	return true;
}

static bool bstartree_node_spill_left(struct _bstartree_node *left_sibling,
				      struct _bstartree_node *right_sibling, unsigned int idx,
				      void *item, struct _bstartree_node *right,
				      struct _bstartree_node *parent, unsigned int idx_in_parent,
				      const struct bstartree_info *info)
{
	if (left_sibling->num_items == info->max_items) {
		return false;
	}
	if (idx == 0) {
		bstartree_node_insert_unchecked(left_sibling, left_sibling->num_items,
						bstartree_node_item(parent, idx_in_parent - 1, info),
						right ? bstartree_node_get_child(right_sibling, 0, info) : NULL,
						info);
		bstartree_node_set_item(parent, idx_in_parent - 1, item, info);
		if (right) {
			bstartree_node_set_child(right_sibling, 0, right, info);
		}
		return true;
	}
	bstartree_move_item_left(left_sibling, right_sibling, parent, idx_in_parent - 1, !right, info);
	bstartree_node_insert_unchecked(right_sibling, idx - 1, item, right, info);
	return true;
}

static bool bstartree_node_spill_right(struct _bstartree_node *left_sibling,
				       struct _bstartree_node *right_sibling, unsigned int idx,
				       void *item, struct _bstartree_node *right,
				       struct _bstartree_node *parent, unsigned int idx_in_parent,
				       const struct bstartree_info *info)
{
	if (right_sibling->num_items == info->max_items) {
		return false;
	}
	if (idx == info->max_items) {
		bstartree_node_shift_items_right(right_sibling, 0, info);
		bstartree_node_set_item(right_sibling, 0,
					bstartree_node_item(parent, idx_in_parent, info),
					info);
		if (right) {
			bstartree_node_shift_children_right(right_sibling, 0, info);
			bstartree_node_set_child(right_sibling, 0, right, info);
		}
		right_sibling->num_items++;
		bstartree_node_set_item(parent, idx_in_parent, item, info);
		return true;
	}
	bstartree_move_item_right(left_sibling, right_sibling, parent, idx_in_parent, !right, info);
	bstartree_node_insert_unchecked(left_sibling, idx, item, right, info);
	return true;
}

static struct _bstartree_node *bstartree_node_split_and_insert(struct _bstartree_node *left_sibling,
							       struct _bstartree_node *right_sibling,
							       unsigned int idx, void *item,
							       struct _bstartree_node *right,
							       struct _bstartree_node *parent,
							       unsigned int idx_in_parent,
							       const struct bstartree_info *info)
{
	// assert(left_sibling == bstartree_node_get_child(parent, idx_in_parent, info));
	// assert(left_sibling->num_items == info->max_items);
	// assert(right_sibling->num_items == info->max_items);
	struct _bstartree_node *new_node = bstartree_new_node(!right, false, info);
	left_sibling->num_items = 2 * info->max_items - 2 * info->min_items; // + 2 - 2
	right_sibling->num_items = info->min_items;
	new_node->num_items = info->min_items;
	// assert(left_sibling->num_items >= info->min_items);
	// assert(left_sibling->num_items <= info->max_items);

	struct _bstartree_node *item_insertion_node;
	struct _bstartree_node *right_insertion_node;
	unsigned int right_insertion_idx;
	// unsigned int left_num_children = left_sibling->num_items + 1;
	unsigned int right_num_children = right_sibling->num_items + 1;
	unsigned int new_num_children = new_node->num_items + 1;
	if (idx < left_sibling->num_items) {
		left_sibling->num_items--;
		item_insertion_node = left_sibling;
		right_insertion_node = left_sibling;
		right_insertion_idx = idx + 1;
		// left_num_children--;
	} else if (idx == left_sibling->num_items) {
		idx = idx_in_parent;
		item_insertion_node = parent;
		right_insertion_node = right_sibling;
		right_insertion_idx = 0;
		right_num_children--;
	} else {
		idx -= left_sibling->num_items + 1;
		if (idx < right_sibling->num_items) {
			right_sibling->num_items--;
			item_insertion_node = right_sibling;
			right_insertion_node = right_sibling;
			right_insertion_idx = idx + 1;
			right_num_children--;
		} else if (idx == right_sibling->num_items) {
			idx = -1;
			item_insertion_node = NULL;
			right_insertion_node = new_node;
			right_insertion_idx = 0;
			new_num_children--;
		} else {
			idx -= right_sibling->num_items + 1;
			// assert(idx < new_node->num_items);
			new_node->num_items--;
			item_insertion_node = new_node;
			right_insertion_node = new_node;
			right_insertion_idx = idx + 1;
			new_num_children--;
		}
	}

	void *temp = alloca(info->item_size);
	unsigned int n = info->max_items - new_node->num_items;
	bstartree_node_copy_items(new_node, 0, right_sibling, n, new_node->num_items, info);
	// assert(n > 0);
	if (item_insertion_node) {
		n--;
		bstartree_node_get_item(temp, right_sibling, n, info);
	}
	memmove(bstartree_node_item(right_sibling, right_sibling->num_items - n, info),
		bstartree_node_item(right_sibling, 0, info),
		n * info->item_size);
	n = right_sibling->num_items - n;
	if (n > 0) {
		n--;
		bstartree_node_copy_item(right_sibling, n, parent, idx_in_parent, info);
		bstartree_node_copy_items(right_sibling, 0, left_sibling, info->max_items - n, n, info);
	}
	n = info->max_items - n;
	if (n != left_sibling->num_items && item_insertion_node != parent) {
		n--;
		bstartree_node_copy_item(parent, idx_in_parent, left_sibling, n, info);
	}
	// assert(n == left_sibling->num_items);
	if (item_insertion_node) {
		if (item_insertion_node != parent) {
			bstartree_node_shift_items_right(item_insertion_node, idx, info);
		}
		bstartree_node_set_item(item_insertion_node, idx, item, info);
		memcpy(item, temp, info->item_size);
	}

	if (right) {
		n = info->max_items + 1 - new_num_children;
		bstartree_node_copy_children(new_node, 0, right_sibling, n, new_num_children, info);
		memmove(bstartree_node_children(right_sibling, info) + right_num_children - n,
			bstartree_node_children(right_sibling, info),
			n * sizeof(struct _bstartree_node *));
		n = right_num_children - n;
		bstartree_node_copy_children(right_sibling, 0, left_sibling, info->max_items + 1 - n,
					     n, info);
		n = info->max_items + 1 - n;
		// assert(n == left_num_children);
		bstartree_node_shift_children_right(right_insertion_node, right_insertion_idx, info);
		bstartree_node_set_child(right_insertion_node, right_insertion_idx, right, info);
	}
	if (item_insertion_node && item_insertion_node != parent) {
		item_insertion_node->num_items++;
	}

	return new_node;
}

static void bstartree_node_split_and_insert_root(struct _bstartree *tree, unsigned int idx, void *item,
						 struct _bstartree_node *right, bool bulk_loading,
						 const struct bstartree_info *info)
{
	const unsigned int root_max_items = bstartree_root_max_items(info);
	struct _bstartree_node *root = tree->root;
	// assert(root->num_items == root_max_items);
	struct _bstartree_node *left_child = bstartree_new_node(!right, false, info);
	struct _bstartree_node *right_child = bstartree_new_node(!right, false, info);

	if (bulk_loading) {
		left_child->num_items = info->max_items;
	} else {
		left_child->num_items = root_max_items / 2;
	}
	right_child->num_items = root_max_items - left_child->num_items;
	root->num_items = 1;

	if (idx < left_child->num_items) {
		left_child->num_items--;
		bstartree_node_copy_items(left_child, 0, root, 0, left_child->num_items, info);
		bstartree_node_copy_item(root, 0, root, left_child->num_items, info);
		bstartree_node_copy_items(right_child, 0, root, left_child->num_items + 1,
					  right_child->num_items, info);

		if (right) {
			bstartree_node_copy_children(left_child, 0, root, 0, left_child->num_items + 1, info);
			bstartree_node_copy_children(right_child, 0, root, left_child->num_items + 1,
						     right_child->num_items + 1, info);
		}

		bstartree_node_insert_unchecked(left_child, idx, item, right, info);
	} else if (idx > left_child->num_items) {
		idx -= left_child->num_items + 1;
		right_child->num_items--;
		bstartree_node_copy_items(left_child, 0, root, 0, left_child->num_items, info);
		bstartree_node_copy_item(root, 0, root, left_child->num_items, info);
		bstartree_node_copy_items(right_child, 0, root, left_child->num_items + 1,
					  right_child->num_items, info);

		if (right) {
			bstartree_node_copy_children(left_child, 0, root, 0, left_child->num_items + 1, info);
			bstartree_node_copy_children(right_child, 0, root, left_child->num_items + 1,
						     right_child->num_items + 1, info);
		}

		bstartree_node_insert_unchecked(right_child, idx, item, right, info);
	} else {
		bstartree_node_copy_items(left_child, 0, root, 0, left_child->num_items, info);
		bstartree_node_copy_items(right_child, 0, root, left_child->num_items,
					  right_child->num_items, info);

		if (right) {
			bstartree_node_copy_children(left_child, 0, root, 0, left_child->num_items + 1, info);
			bstartree_node_copy_children(right_child, 0, root, left_child->num_items + 1,
						     right_child->num_items, info);

			// right_child->num_items is 0 only when bulk_loading and max_items == 4
			if (right_child->num_items != 0) {
				right_child->num_items--;
				bstartree_node_shift_children_right(right_child, 0, info);
				right_child->num_items++;
			}
			bstartree_node_set_child(right_child, 0, right, info);
		}

		bstartree_node_set_item(root, 0, item, info);
	}

	// assert((unsigned int)left_child->num_items + right_child->num_items + root->num_items == root_max_items + 1);

	bstartree_node_set_child(root, 0, left_child, info);
	bstartree_node_set_child(root, 1, right_child, info);
	// root->leaf = false;
	tree->height++;
}

static void bstartree_insert_and_rebalance(struct _bstartree *tree, void *item,
					   struct _bstartree_pos path[static __BSTARTREE_MAX_HEIGHT],
					   const struct bstartree_info *info)
{
	struct _bstartree_node *right = NULL;
	unsigned int idx = path[0].idx;
	for (unsigned int level = 0; level < tree->height - 1u; level++) {
		struct _bstartree_node *node = path[level].node;

		// assert(idx <= node->num_items);
		if (node->num_items < info->max_items) {
			bstartree_node_insert_unchecked(node, idx, item, right, info);
			return;
		}

		struct _bstartree_node *parent = path[level + 1].node;
		unsigned int idx_in_parent = path[level + 1].idx;
		struct _bstartree_node *left_sibling = NULL;
		struct _bstartree_node *right_sibling = NULL;
		if (idx_in_parent != 0) {
			left_sibling = bstartree_node_get_child(parent, idx_in_parent - 1, info);
			if (bstartree_node_spill_left(left_sibling, node, idx, item, right,
						      parent, idx_in_parent, info)) {
				return;
			}
		}
		if (idx_in_parent != parent->num_items) {
			right_sibling = bstartree_node_get_child(parent, idx_in_parent + 1, info);
			if (bstartree_node_spill_right(node, right_sibling, idx, item, right,
						       parent, idx_in_parent, info)) {
				return;
			}
		}
		if (left_sibling) {
			right_sibling = node;
			idx += info->max_items + 1;
			idx_in_parent--;
		} else {
			left_sibling = node;
		}
		right = bstartree_node_split_and_insert(left_sibling, right_sibling, idx, item, right,
							parent, idx_in_parent, info);
		idx = idx_in_parent + 1;
	}
	if (tree->root->num_items < bstartree_root_max_items(info)) {
		bstartree_node_insert_unchecked(tree->root, idx, item, right, info);
		return;
	}
	bstartree_node_split_and_insert_root(tree, idx, item, right, false, info);
}

bool _bstartree_insert(struct _bstartree *tree, void *item, bool update, const struct bstartree_info *info)
{
	if (tree->height == 0) {
		tree->root = bstartree_new_node(true, true, info);
		tree->height = 1;
	}
	struct _bstartree_node *node = tree->root;
	struct _bstartree_pos path[__BSTARTREE_MAX_HEIGHT];
	unsigned int level = tree->height - 1;
	unsigned int idx;
	for (;;) {
		if (bstartree_node_search(node, item, &idx, info)) {
			if (update) {
				if (info->destroy_item) {
					info->destroy_item(bstartree_node_item(node, idx, info));
				}
				bstartree_node_set_item(node, idx, item, info);
			}
			return false;
		}
		path[level].idx = idx;
		path[level].node = node;
		if (level-- == 0) {
			break;
		}
		node = bstartree_node_get_child(node, idx, info);
	}
	bstartree_insert_and_rebalance(tree, item, path, info);
	return true;
}

bool _bstartree_insert_sequential(struct _bstartree *tree, void *item, const struct bstartree_info *info)
{
	if (tree->height == 0) {
		return _bstartree_insert(tree, item, false, info);
	}
	struct _bstartree_node *node = tree->root;
	struct _bstartree_pos path[__BSTARTREE_MAX_HEIGHT];
	unsigned int level = tree->height - 1;
	unsigned int idx;
	for (;;) {
		idx = node->num_items;
		if (unlikely(info->cmp(item, bstartree_node_item(node, idx - 1, info)) <= 0)) {
			return _bstartree_insert(tree, item, false, info);
		}
		path[level].idx = idx;
		path[level].node = node;
		if (level-- == 0) {
			break;
		}
		node = bstartree_node_get_child(node, idx, info);
	}
	bstartree_insert_and_rebalance(tree, item, path, info);
	return true;
}

void _bstartree_bulk_load_start(struct _bstartree_bulk_load_ctx *ctx)
{
	_bstartree_init(&ctx->tree);
}

void _bstartree_bulk_load_next(struct _bstartree_bulk_load_ctx *ctx, void *item,
			       const struct bstartree_info *info)
{
	if (ctx->tree.height == 0) {
		struct _bstartree_node *root = bstartree_new_node(true, true, info);
		ctx->tree.root = root;
		ctx->tree.height = 1;
		ctx->path[0].node = root;
		ctx->path[0].idx = 0;
	}
	unsigned int level;
	struct _bstartree_node *right = NULL;
	for (level = 0; level < ctx->tree.height - 1u; level++) {
		unsigned int idx = ctx->path[level].idx++;
		struct _bstartree_node *node = ctx->path[level].node;

		if (node->num_items < info->max_items) {
			bstartree_node_insert_unchecked(node, idx, item, right, info);
			return;
		}

		struct _bstartree_node *new_node = bstartree_new_node(!right, false, info);
		if (right) {
			bstartree_node_set_child(new_node, 0, right, info);
		}
		right = new_node;
		ctx->path[level].node = right;
		ctx->path[level].idx = 0;
	}
	unsigned int idx = ctx->path[level].idx++;
	// assert(ctx->path[level].node == ctx->tree.root);
	if (ctx->tree.root->num_items < bstartree_root_max_items(info)) {
		bstartree_node_insert_unchecked(ctx->tree.root, idx, item, right, info);
		return;
	}
	bstartree_node_split_and_insert_root(&ctx->tree, idx, item, right, true, info);
	ctx->path[level + 1].idx = 1;
	ctx->path[level + 1].node = ctx->tree.root;
	right = bstartree_node_get_child(ctx->tree.root, 1, info);
	ctx->path[level].node = right;
	ctx->path[level].idx = right->num_items;
}

static void bstartree_move_items_right(struct _bstartree_node *left, struct _bstartree_node *right,
				       struct _bstartree_node *parent, unsigned int idx_in_parent,
				       unsigned int num_items, bool leaf, const struct bstartree_info *info)
{
	// assert(right->num_items + num_items <= info->max_items);
	// assert(left->num_items >= num_items);
	unsigned int n = num_items;
	memmove(bstartree_node_item(right, n, info),
		bstartree_node_item(right, 0, info),
		right->num_items * info->item_size);
	n--;
	bstartree_node_copy_item(right, n, parent, idx_in_parent, info);
	bstartree_node_copy_items(right, 0, left, left->num_items - n, n, info);
	n = left->num_items - n - 1;
	bstartree_node_copy_item(parent, idx_in_parent, left, n, info);
	if (!leaf) {
		memmove(bstartree_node_children(right, info) + num_items,
			bstartree_node_children(right, info),
			(right->num_items + 1) * sizeof(struct _bstartree_node *));
		bstartree_node_copy_children(right, 0, left, left->num_items + 1 - num_items,
					     num_items, info);
	}
	left->num_items -= num_items;
	right->num_items += num_items;
}

void _bstartree_bulk_load_end(struct _bstartree_bulk_load_ctx *ctx, const struct bstartree_info *info)
{
	if (ctx->tree.height < 2) {
		return;
	}
	struct _bstartree_node *parent = ctx->tree.root;
	struct _bstartree_node *node;
	for (unsigned int level = ctx->tree.height - 1; level-- > 0; parent = node) {
		node = ctx->path[level].node;
		if (node->num_items >= info->min_items) {
			continue;
		}
		// assert(parent->num_items != 0);
		unsigned int idx_in_parent = parent->num_items - 1;
		struct _bstartree_node *left = bstartree_node_get_child(parent, idx_in_parent, info);
		// assert(left->num_items == info->max_items);
		bstartree_move_items_right(left, node, parent, idx_in_parent,
					   info->min_items - node->num_items,
					   level == 0, info);

		if (left->num_items >= info->min_items) {
			continue;
		}
		// assert(idx_in_parent != 0);
		idx_in_parent--;
		struct _bstartree_node *leftleft = bstartree_node_get_child(parent, idx_in_parent, info);
		// assert(leftleft->num_items == info->max_items);
		bstartree_move_items_right(leftleft, left, parent, idx_in_parent,
					   info->min_items - left->num_items,
					   level == 0, info);
	}
}

static struct _bstartree_node *bstartree_node_copy(struct _bstartree_node *node, unsigned int level, bool root,
						   const struct bstartree_info *info)
{
	struct _bstartree_node *copy = bstartree_new_node(level == 0, root, info);
	memcpy(bstartree_node_item(copy, 0, info),
	       bstartree_node_item(node, 0, info),
	       node->num_items * info->item_size);
	copy->num_items = node->num_items;
	if (level == 0) {
		return copy;
	}
	for (unsigned int i = 0; i < node->num_items + 1u; i++) {
		struct _bstartree_node *child = bstartree_node_get_child(node, i, info);
		struct _bstartree_node *child_copy = bstartree_node_copy(child, level - 1, false, info);
		bstartree_node_set_child(copy, i, child_copy, info);
	}
	return copy;
}

struct _bstartree _bstartree_debug_copy(const struct _bstartree *tree, const struct bstartree_info *info)
{
	unsigned int height = tree->height;
	struct _bstartree copy;
	copy.height = height;
	copy.root = height == 0 ? NULL : bstartree_node_copy(tree->root, height - 1, true, info);
	return copy;
}

size_t _bstartree_debug_node_size(bool leaf, bool root, const struct bstartree_info *info)
{
	return bstartree_node_size(leaf, root, info);
}
