/*
 * Copyright (C) 2024 Fabian Hügel
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include "bplustree.h"
#include "compiler.h"

#define BPLUSTREE_MAX_HEIGHT 48

struct _bplustree_pos {
	struct _bplustree_node *node;
	unsigned int idx;
};

static void *bplustree_node_key(struct _bplustree_node *node, unsigned int idx,
				const struct bplustree_info *info)
{
	return node->data + info->key_alignment_offset + idx * info->key_size;
}

static void *bplustree_node_value(struct _bplustree_node *node, unsigned int idx,
				  const struct bplustree_info *info)
{
	return node->data + info->key_alignment_offset + info->max_items * info->key_size +
		info->value_alignment_offset + idx * info->value_size;
}

static struct _bplustree_leaf_link *bplustree_leaf_link(struct _bplustree_node *node,
							const struct bplustree_info *info)
{
	(void)info;
	return (struct _bplustree_leaf_link *)node - 1;
}

static struct _bplustree_node *bplustree_leaf_link_to_node(struct _bplustree_leaf_link *link,
							   const struct bplustree_info *info)
{
	(void)info;
	return (struct _bplustree_node *)(link + 1);
}

static struct _bplustree_node **bplustree_node_children(struct _bplustree_node *node,
							const struct bplustree_info *info)
{
	char *children = (char *)bplustree_node_value(node, 0, info);
	// assert((uintptr_t)children % sizeof(void *) == 0);
	return (struct _bplustree_node **)children;
}

static struct _bplustree_node *bplustree_node_get_child(struct _bplustree_node *node, unsigned int idx,
							const struct bplustree_info *info)
{
	return bplustree_node_children(node, info)[idx];
}

static void bplustree_node_set_child(struct _bplustree_node *node, unsigned int idx,
				     struct _bplustree_node *child, const struct bplustree_info *info)
{
	bplustree_node_children(node, info)[idx] = child;
}

static void bplustree_node_copy_child(struct _bplustree_node *dest_node, unsigned int dest_idx,
				      struct _bplustree_node *src_node, unsigned int src_idx,
				      const struct bplustree_info *info)
{
	bplustree_node_set_child(dest_node, dest_idx, bplustree_node_get_child(src_node, src_idx, info), info);
}

static void bplustree_node_get_key(void *key, struct _bplustree_node *node, unsigned int idx,
				   const struct bplustree_info *info)
{
	memcpy(key, bplustree_node_key(node, idx, info), info->key_size);
}

static void bplustree_node_set_key(struct _bplustree_node *node, unsigned int idx, const void *key,
				   const struct bplustree_info *info)
{
	memcpy(bplustree_node_key(node, idx, info), key, info->key_size);
}

static void bplustree_node_copy_key(struct _bplustree_node *dest_node, unsigned int dest_idx,
				    struct _bplustree_node *src_node, unsigned int src_idx,
				    const struct bplustree_info *info)
{
	bplustree_node_set_key(dest_node, dest_idx, bplustree_node_key(src_node, src_idx, info), info);
}

static void bplustree_node_get_value(void *value, struct _bplustree_node *node, unsigned int idx,
				     const struct bplustree_info *info)
{
	memcpy(value, bplustree_node_value(node, idx, info), info->value_size);
}

static void bplustree_node_set_value(struct _bplustree_node *node, unsigned int idx, const void *value,
				     const struct bplustree_info *info)
{
	memcpy(bplustree_node_value(node, idx, info), value, info->value_size);
}

static void bplustree_node_copy_value(struct _bplustree_node *dest_node, unsigned int dest_idx,
				      struct _bplustree_node *src_node, unsigned int src_idx,
				      const struct bplustree_info *info)
{
	bplustree_node_set_value(dest_node, dest_idx, bplustree_node_value(src_node, src_idx, info), info);
}

static bool bplustree_node_search(struct _bplustree_node *node, const void *key, unsigned int *ret_idx,
				  const struct bplustree_info *info)
{
	unsigned int start = 0;
	unsigned int end = node->num_items;
	while (start + info->linear_search_threshold < end) {
		unsigned int mid = (start + end) / 2; // start + end should never overflow
		int cmp = info->cmp(key, bplustree_node_key(node, mid, info));
		if (cmp == 0) {
			*ret_idx = mid;
			return true;
		} else if (cmp > 0) {
			start = mid + 1;
		} else {
			end = mid;
		}
	}

	while (start < end) {
		int cmp = info->cmp(key, bplustree_node_key(node, start, info));
		if (cmp == 0) {
			*ret_idx = start;
			return true;
		}
		if (cmp < 0) {
			break;
		}
		start++;
		end--;
		cmp = info->cmp(key, bplustree_node_key(node, end, info));
		if (cmp == 0) {
			*ret_idx = end;
			return true;
		}
		if (cmp > 0) {
			start = end + 1;
			break;
		}
	}
	*ret_idx = start;
	return false;
}

_bplustree_kv_t _bplustree_iter_start(struct _bplustree_iter *iter, const struct _bplustree *tree,
				      bool rightmost, const struct bplustree_info *info)
{
	iter->tree = tree;

	if (tree->height == 0) {
		iter->node = NULL;
		iter->idx = 0;
		return (_bplustree_kv_t) { NULL, NULL };
	}

	iter->node = bplustree_leaf_link_to_node(rightmost ? tree->list->prev : tree->list->next, info);
	iter->idx = rightmost ? iter->node->num_items - 1 : 0;
	return (_bplustree_kv_t) {
		bplustree_node_key(iter->node, iter->idx, info),
		bplustree_node_value(iter->node, iter->idx, info),
	};
}

_bplustree_kv_t _bplustree_iter_next(struct _bplustree_iter *iter, const struct bplustree_info *info)
{
	if (!iter->node) {
		return (_bplustree_kv_t) { NULL, NULL };
	}
	iter->idx++;
	if (iter->idx >= iter->node->num_items) {
		iter->idx = 0;
		struct _bplustree_leaf_link *next = bplustree_leaf_link(iter->node, info)->next;
		if (next == iter->tree->list) {
			iter->node = NULL;
			return (_bplustree_kv_t) { NULL, NULL };
		}
		iter->node = bplustree_leaf_link_to_node(next, info);
	}
	return (_bplustree_kv_t) {
		bplustree_node_key(iter->node, iter->idx, info),
		bplustree_node_value(iter->node, iter->idx, info),
	};
}

_bplustree_kv_t _bplustree_iter_prev(struct _bplustree_iter *iter, const struct bplustree_info *info)
{
	if (!iter->node) {
		return (_bplustree_kv_t) { NULL, NULL };
	}
	if (iter->idx == 0) {
		struct _bplustree_leaf_link *prev = bplustree_leaf_link(iter->node, info)->prev;
		if (prev == iter->tree->list) {
			iter->node = NULL;
			return (_bplustree_kv_t) { NULL, NULL };
		}
		iter->node = bplustree_leaf_link_to_node(prev, info);
		iter->idx = iter->node->num_items;
	}
	iter->idx--;
	return (_bplustree_kv_t) {
		bplustree_node_key(iter->node, iter->idx, info),
		bplustree_node_value(iter->node, iter->idx, info),
	};
}

_bplustree_kv_t _bplustree_iter_start_at(struct _bplustree_iter *iter, const struct _bplustree *tree,
					 void *key, enum bplustree_iter_start_at_mode mode,
					 const struct bplustree_info *info)
{
	iter->tree = tree;

	if (tree->height == 0) {
		iter->node = NULL;
		iter->idx = 0;
		return (_bplustree_kv_t) { NULL, NULL };
	}

	struct _bplustree_node *node = tree->root;
	unsigned int idx;
	bool found;
	size_t level = tree->height;
	for (;;) {
		found = bplustree_node_search(node, key, &idx, info);
		if (--level == 0) {
			break;
		}
		node = bplustree_node_get_child(node, idx + found, info);
	}
	switch (mode) {
	case BPLUSTREE_ITER_FIND_KEY:
		if (!found) {
			return (_bplustree_kv_t) { NULL, NULL };
		}
		iter->node = node;
		iter->idx = idx;
		break;
	case BPLUSTREE_ITER_LOWER_BOUND_EXCLUSIVE:
		idx += found;
		_attr_fallthrough;
	case BPLUSTREE_ITER_LOWER_BOUND_INCLUSIVE:
		iter->node = node;
		iter->idx = idx;
		if (idx == node->num_items) {
			return _bplustree_iter_next(iter, info);
		}
		break;
	case BPLUSTREE_ITER_UPPER_BOUND_EXCLUSIVE:
		iter->node = node;
		iter->idx = idx;
		return _bplustree_iter_prev(iter, info);
	case BPLUSTREE_ITER_UPPER_BOUND_INCLUSIVE:
		iter->node = node;
		iter->idx = idx;
		if (!found) {
			return _bplustree_iter_prev(iter, info);
		}
		break;
	}
	return (_bplustree_kv_t) {
		bplustree_node_key(iter->node, iter->idx, info),
		bplustree_node_value(iter->node, iter->idx, info),
	};
}

static size_t bplustree_node_size(bool leaf, const struct bplustree_info *info)
{
	size_t size = sizeof(struct _bplustree_node);
	size += info->key_alignment_offset;
	size += info->max_items * info->key_size;
	size += info->value_alignment_offset;
	if (leaf) {
		size += info->max_items * info->value_size;
		size += sizeof(struct _bplustree_leaf_link);
	} else {
		size += (info->max_items + 1) * sizeof(struct _bplustree_node *);
	}
	return size;
}

static struct _bplustree_node *bplustree_new_node(bool leaf, const struct bplustree_info *info)
{
	struct _bplustree_node *node;
	size_t size = bplustree_node_size(leaf, info);
	char *m = malloc(size);
	if (leaf) {
		node = (struct _bplustree_node *)(m + sizeof(struct _bplustree_leaf_link));
	} else {
		node = (struct _bplustree_node *)m;
	}
	node->num_items = 0;
	return node;
}

static void bplustree_free_node(struct _bplustree_node *node, bool leaf, const struct bplustree_info *info)
{
	if (leaf) {
		free(bplustree_leaf_link(node, info));
	} else {
		free(node);
	}
}

void _bplustree_init(struct _bplustree *tree)
{
	memset(tree, 0, sizeof(*tree));
}

void _bplustree_destroy(struct _bplustree *tree, const struct bplustree_info *info)
{
	if (tree->height == 0) {
		return;
	}

	free(tree->list);
	tree->list = NULL;

	struct _bplustree_pos path[BPLUSTREE_MAX_HEIGHT];
	struct _bplustree_node *node = tree->root;
	for (unsigned int level = tree->height; level-- > 0;) {
		path[level].idx = 0;
		path[level].node = node;
		node = level != 0 ? bplustree_node_get_child(node, 0, info) : NULL;
	}

	unsigned int level = 0;
	for (;;) {
		// start at leaf
		struct _bplustree_pos *pos = &path[level];
		// free the leaf and go up, if we are at the end of the current node free it and keep going up
		do {
			bool leaf = level == 0;
			if (leaf && info->destroy_key) {
				for (unsigned int i = 0; i < pos->node->num_items; i++) {
					info->destroy_key(bplustree_node_key(pos->node, i, info));
				}
			}
			if (leaf && info->destroy_value) {
				for (unsigned int i = 0; i < pos->node->num_items; i++) {
					info->destroy_value(bplustree_node_value(pos->node, i, info));
				}
			}
			bplustree_free_node(pos->node, leaf, info);
			if (++level == tree->height) {
				tree->root = NULL;
				tree->height = 0;
				return;
			}
			pos = &path[level];
		} while (pos->idx >= pos->node->num_items);
		// descend into the leftmost child of the right child
		pos->idx++;
		do {
			struct _bplustree_node *child = bplustree_node_get_child(pos->node, pos->idx, info);
			pos = &path[--level];
			pos->node = child;
			pos->idx = 0;
		} while (level != 0);
	}
}

_bplustree_kv_t _bplustree_find(const struct _bplustree *tree, const void *key,
				const struct bplustree_info *info)
{
	if (tree->height == 0) {
		return (_bplustree_kv_t) { NULL, NULL };
	}
	struct _bplustree_node *node = tree->root;
	unsigned int level = tree->height;
	void *k = NULL;
	void *v = NULL;
	for (;;) {
		unsigned int idx;
		bool found = bplustree_node_search(node, key, &idx, info);
		if (--level == 0) {
			if (found) {
				k = bplustree_node_key(node, idx, info);
				v = bplustree_node_value(node, idx, info);
			}
			break;
		}
		idx += found;
		node = bplustree_node_get_child(node, idx, info);
	}
	return (_bplustree_kv_t) { k, v };
}

_bplustree_kv_t _bplustree_get_leftmost_rightmost(const struct _bplustree *tree, bool leftmost,
						  const struct bplustree_info *info)
{
	if (tree->height == 0) {
		return (_bplustree_kv_t) { NULL, NULL };
	}
	struct _bplustree_leaf_link *link = leftmost ? tree->list->next : tree->list->prev;
	struct _bplustree_node *node = bplustree_leaf_link_to_node(link, info);
	unsigned int idx = leftmost ? 0 : node->num_items - 1;
	return (_bplustree_kv_t) {
		bplustree_node_key(node, idx, info),
		bplustree_node_value(node, idx, info)
	};
}

static void bplustree_node_shift_keys_right(struct _bplustree_node *node, unsigned int idx,
					    const struct bplustree_info *info)
{
	memmove(bplustree_node_key(node, idx + 1, info),
		bplustree_node_key(node, idx, info),
		(node->num_items - idx) * info->key_size);
}

static void bplustree_node_shift_children_right(struct _bplustree_node *node, unsigned int idx,
						const struct bplustree_info *info)
{
	memmove(bplustree_node_children(node, info) + idx + 1,
		bplustree_node_children(node, info) + idx,
		(node->num_items + 1 - idx) * sizeof(struct _bplustree_node *));
}

static void bplustree_node_shift_values_right(struct _bplustree_node *node, unsigned int idx,
					      const struct bplustree_info *info)
{
	memmove(bplustree_node_value(node, idx + 1, info),
		bplustree_node_value(node, idx, info),
		(node->num_items - idx) * info->value_size);
}

static void bplustree_node_shift_keys_left(struct _bplustree_node *node, unsigned int idx,
					   const struct bplustree_info *info)
{
	memmove(bplustree_node_key(node, idx, info),
		bplustree_node_key(node, idx + 1, info),
		(node->num_items - idx - 1) * info->key_size);
}

static void bplustree_node_shift_children_left(struct _bplustree_node *node, unsigned int idx,
					       const struct bplustree_info *info)
{
	memmove(bplustree_node_children(node, info) + idx,
		bplustree_node_children(node, info) + idx + 1,
		(node->num_items - idx) * sizeof(struct _bplustree_node *));
}

static void bplustree_node_shift_values_left(struct _bplustree_node *node, unsigned int idx,
					     const struct bplustree_info *info)
{
	memmove(bplustree_node_value(node, idx, info),
		bplustree_node_value(node, idx + 1, info),
		(node->num_items - idx - 1) * info->value_size);
}

bool _bplustree_delete(struct _bplustree *tree, enum _bplustree_deletion_mode mode, const void *key,
		       void *ret_key, void *ret_value, const struct bplustree_info *info)
{
	if (tree->height == 0) {
		return false;
	}
	struct _bplustree_node *node = tree->root;
	unsigned int level = tree->height - 1;
	struct _bplustree_pos path[BPLUSTREE_MAX_HEIGHT];
	unsigned int idx;
	bool leaf = false;
	int internal_node_key_found_level = -1;
	for (;;) {
		leaf = level == 0;
		bool found = false;
		switch (mode) {
		case __BPLUSTREE_DELETE_KEY:
			found = bplustree_node_search(node, key, &idx, info);
			if (leaf && !found) {
				return false;
			}
			if (!leaf && found) {
				// assert(internal_node_key_found_level < 0);
				internal_node_key_found_level = level;
			}
			break;
		case __BPLUSTREE_DELETE_MIN:
			idx = 0;
			break;
		case __BPLUSTREE_DELETE_MAX:
			idx = leaf ? node->num_items - 1 : node->num_items;
			break;
		}
		if (leaf) {
			break;
		}
		idx += found;
		path[level].idx = idx;
		path[level].node = node;
		level--;
		node = bplustree_node_get_child(node, idx, info);
	}

	if (internal_node_key_found_level >= 0) {
		struct _bplustree_node *internal_node = path[internal_node_key_found_level].node;
		unsigned int internal_idx = path[internal_node_key_found_level].idx - 1;
		// assert(info->cmp(bplustree_node_key(internal_node, internal_idx, info), key) == 0);

		// if the compiler constant folds 'info', it can remove the branch below if info->min_items > 1
		compiler_assume(node->num_items >= info->min_items);
		if (node->num_items > 1) {
			bplustree_node_copy_key(internal_node, internal_idx, node, 1, info);
		} else {
			// there are no more keys in this node so try to take the first key from the next leaf
			struct _bplustree_leaf_link *next_link = bplustree_leaf_link(node, info)->next;
			if (next_link != tree->list) {
				struct _bplustree_node *next_node = bplustree_leaf_link_to_node(next_link, info);
				bplustree_node_copy_key(internal_node, internal_idx, next_node, 0, info);
			} else {
				// There is no next node which means 'node' is the rightmost node and
				// it is now empty. Since each key in an internal node is the same
				// as its in-order successor, 'internal_node' is the parent of 'node'.
				// And since 'node' will get merged with its left sibling the key in
				// 'internal_node' at 'internal_idx' will be deleted during the merge

				// assert(bplustree_node_get_child(internal_node, internal_idx + 1, info) == node);
			}
		}
	}

	if (ret_key) {
		bplustree_node_get_key(ret_key, node, idx, info);
	} else if (info->destroy_key) {
		info->destroy_key(bplustree_node_key(node, idx, info));
	}
	if (ret_value) {
		bplustree_node_get_value(ret_value, node, idx, info);
	} else if (info->destroy_value) {
		info->destroy_value(bplustree_node_value(node, idx, info));
	}
	bplustree_node_shift_keys_left(node, idx, info);
	bplustree_node_shift_values_left(node, idx, info);
	// assert(node->num_items != 0);
	node->num_items--;

	while (++level != tree->height) {
		if (node->num_items >= info->min_items) {
			return true;
		}

		node = path[level].node;
		idx = path[level].idx;

		if (idx == node->num_items) {
			idx--;
		}
		struct _bplustree_node *left = bplustree_node_get_child(node, idx, info);
		struct _bplustree_node *right = bplustree_node_get_child(node, idx + 1, info);

		// when merging, internal nodes also take a key from the parent, leaf nodes don't,
		// hence the "+ !leaf"
		if (left->num_items + right->num_items + !leaf <= info->max_items) {
			if (!leaf) {
				bplustree_node_copy_key(left, left->num_items, node, idx, info);
				left->num_items++;
			}
			bplustree_node_shift_keys_left(node, idx, info);
			bplustree_node_shift_children_left(node, idx + 1, info);
			node->num_items--;
			memcpy(bplustree_node_key(left, left->num_items, info),
			       bplustree_node_key(right, 0, info),
			       right->num_items * info->key_size);
			if (leaf) {
				memcpy(bplustree_node_value(left, left->num_items, info),
				       bplustree_node_value(right, 0, info),
				       right->num_items * info->value_size);

				struct _bplustree_leaf_link *left_link = bplustree_leaf_link(left, info);
				struct _bplustree_leaf_link *next = bplustree_leaf_link(right, info)->next;
				left_link->next = next;
				next->prev = left_link;
			} else {
				memcpy(bplustree_node_children(left, info) + left->num_items,
				       bplustree_node_children(right, info),
				       (right->num_items + 1) * sizeof(struct _bplustree_node *));
			}
			left->num_items += right->num_items;

			bplustree_free_node(right, leaf, info);
		} else if (left->num_items > right->num_items) {
			bplustree_node_shift_keys_right(right, 0, info);
			if (leaf) {
				bplustree_node_copy_key(right, 0, left, left->num_items - 1, info);

				bplustree_node_shift_values_right(right, 0, info);
				bplustree_node_copy_value(right, 0, left, left->num_items - 1, info);

				bplustree_node_copy_key(node, idx, right, 0, info);
			} else {
				bplustree_node_copy_key(right, 0, node, idx, info);
				bplustree_node_copy_key(node, idx, left, left->num_items - 1, info);

				bplustree_node_shift_children_right(right, 0, info);
				bplustree_node_copy_child(right, 0, left, left->num_items, info);
			}
			left->num_items--;
			right->num_items++;
		} else {
			if (leaf) {
				bplustree_node_copy_key(left, left->num_items, right, 0, info);
				bplustree_node_shift_keys_left(right, 0, info);

				bplustree_node_copy_key(node, idx, right, 0, info);

				bplustree_node_copy_value(left, left->num_items, right, 0, info);
				bplustree_node_shift_values_left(right, 0, info);
			} else {
				bplustree_node_copy_key(left, left->num_items, node, idx, info);
				bplustree_node_copy_key(node, idx, right, 0, info);
				bplustree_node_shift_keys_left(right, 0, info);

				bplustree_node_copy_child(left, left->num_items + 1, right, 0, info);
				bplustree_node_shift_children_left(right, 0, info);
			}
			right->num_items--;
			left->num_items++;
		}

		leaf = false;
	}

	// assert(node == tree->root);

	if (node->num_items == 0) {
		tree->root = NULL;
		if (leaf) {
			free(tree->list);
			tree->list = NULL;
		} else {
			tree->root = bplustree_node_get_child(node, 0, info);
		}
		bplustree_free_node(node, tree->height == 1, info);
		tree->height--;
	}

	return true;
}

static struct _bplustree_node *bplustree_leaf_insert(struct _bplustree_node *node,
						     unsigned int idx, void *key, void *value,
						     const struct bplustree_info *info)
{
	struct _bplustree_node *insert_node = node;
	struct _bplustree_node *new_node = NULL;
	if (node->num_items == info->max_items) {
		new_node = bplustree_new_node(true, info);
		if (idx <= info->min_items) {
			node->num_items = info->min_items;
			new_node->num_items = info->max_items - info->min_items;
		} else {
			node->num_items = info->max_items - info->min_items;
			new_node->num_items = info->min_items;

			idx -= info->max_items - info->min_items;
			insert_node = new_node;
		}

		memcpy(bplustree_node_key(new_node, 0, info),
		       bplustree_node_key(node, node->num_items, info),
		       new_node->num_items * info->key_size);

		memcpy(bplustree_node_value(new_node, 0, info),
		       bplustree_node_value(node, node->num_items, info),
		       new_node->num_items * info->value_size);

		struct _bplustree_leaf_link *node_link = bplustree_leaf_link(node, info);
		struct _bplustree_leaf_link *new_link = bplustree_leaf_link(new_node, info);
		struct _bplustree_leaf_link *next_link = node_link->next;
		new_link->prev = node_link;
		new_link->next = next_link;
		next_link->prev = new_link;
		node_link->next = new_link;
	}
	bplustree_node_shift_keys_right(insert_node, idx, info);
	bplustree_node_set_key(insert_node, idx, key, info);
	bplustree_node_shift_values_right(insert_node, idx, info);
	bplustree_node_set_value(insert_node, idx, value, info);
	insert_node->num_items++;
	return new_node;
}

/* key will be inserted and then set to the median */
static struct _bplustree_node *bplustree_node_split_and_insert(struct _bplustree_node *node, unsigned int idx,
							       void **key, struct _bplustree_node *right,
							       bool bulk_loading,
							       const struct bplustree_info *info)
{
	// assert(node->num_items == info->max_items);
	// assert(right);
	struct _bplustree_node *new_node = bplustree_new_node(false, info);
	if (bulk_loading) {
		// assert(idx == info->max_items);
		if (right) {
			bplustree_node_set_child(new_node, 0, right, info);
		}
		return new_node;
	}
	node->num_items = info->min_items;
	new_node->num_items = info->max_items - info->min_items;
	if (idx < node->num_items) {
		memcpy(bplustree_node_key(new_node, 0, info),
		       bplustree_node_key(node, node->num_items, info),
		       new_node->num_items * info->key_size);
		bplustree_node_shift_keys_right(node, idx, info);
		bplustree_node_set_key(node, idx, *key, info);

		// the median got shifted right by one
		*key = bplustree_node_key(node, node->num_items, info); // return median

		memcpy(bplustree_node_children(new_node, info),
		       bplustree_node_children(node, info) + node->num_items,
		       (new_node->num_items + 1) * sizeof(struct _bplustree_node *));
		bplustree_node_shift_children_right(node, idx + 1, info);
		bplustree_node_set_child(node, idx + 1, right, info);
	} else if (idx == node->num_items) {
		// key is already set to median
		memcpy(bplustree_node_key(new_node, 0, info),
		       bplustree_node_key(node, node->num_items, info),
		       new_node->num_items * info->key_size);

		memcpy(bplustree_node_children(new_node, info) + 1,
		       bplustree_node_children(node, info) + node->num_items + 1,
		       new_node->num_items * sizeof(struct _bplustree_node *));
		bplustree_node_set_child(new_node, 0, right, info);
	} else {
		idx -= node->num_items + 1;
		// it is not worth splitting the memcpys here in two to avoid the shifts
		memcpy(bplustree_node_key(new_node, 0, info),
		       bplustree_node_key(node, node->num_items + 1, info),
		       (new_node->num_items - 1) * info->key_size);
		new_node->num_items--; // Hack: we have only inserted new_node->num_items - 1 items so far
		bplustree_node_shift_keys_right(new_node, idx, info);
		new_node->num_items++;
		bplustree_node_set_key(new_node, idx, *key, info);

		*key = bplustree_node_key(node, node->num_items, info); // return median

		memcpy(bplustree_node_children(new_node, info),
		       bplustree_node_children(node, info) + node->num_items + 1,
		       new_node->num_items * sizeof(struct _bplustree_node *));
		new_node->num_items--; // Hack
		bplustree_node_shift_children_right(new_node, idx + 1, info);
		new_node->num_items++;
		bplustree_node_set_child(new_node, idx + 1, right, info);
	}

	return new_node;
}

static void bplustree_internal_insert_and_rebalance(struct _bplustree *tree, struct _bplustree_node *right,
						    struct _bplustree_pos path[static BPLUSTREE_MAX_HEIGHT],
						    bool bulk_loading, const struct bplustree_info *info)
{
	void *key = bplustree_node_key(right, 0, info);
	struct _bplustree_node *node = path[0].node;
	for (unsigned int level = 1; level < tree->height; level++) {
		unsigned int idx = path[level].idx++;
		node = path[level].node;

		if (node->num_items < info->max_items) {
			bplustree_node_shift_keys_right(node, idx, info);
			bplustree_node_set_key(node, idx, key, info);
			bplustree_node_shift_children_right(node, idx + 1, info);
			bplustree_node_set_child(node, idx + 1, right, info);
			node->num_items++;
			return;
		}

		right = bplustree_node_split_and_insert(node, idx, &key, right, bulk_loading, info);
		path[level].node = right;
		path[level].idx = right->num_items;
	}
	struct _bplustree_node *new_root = bplustree_new_node(false, info);
	bplustree_node_set_key(new_root, 0, key, info);
	new_root->num_items = 1;
	bplustree_node_set_child(new_root, 0, node, info);
	bplustree_node_set_child(new_root, 1, right, info);
	tree->root = new_root;
	path[tree->height].idx = 1;
	path[tree->height].node = tree->root;
	tree->height++;
}

static void bplustree_insert_and_rebalance(struct _bplustree *tree, void *key, void *value,
					   struct _bplustree_pos path[static BPLUSTREE_MAX_HEIGHT],
					   const struct bplustree_info *info)
{
	struct _bplustree_node *right = bplustree_leaf_insert(path[0].node, path[0].idx, key, value, info);
	if (!right) {
		return;
	}
	bplustree_internal_insert_and_rebalance(tree, right, path, false, info);
}

bool _bplustree_insert(struct _bplustree *tree, void *key, void *value, bool update,
		       const struct bplustree_info *info)
{
	if (tree->height == 0) {
		tree->root = bplustree_new_node(true, info);
		// assert(!tree->list);
		free(tree->list);
		tree->list = malloc(sizeof(*tree->list));
		struct _bplustree_leaf_link *root_link = bplustree_leaf_link(tree->root, info);
		root_link->prev = tree->list;
		root_link->next = tree->list;
		tree->list->prev = root_link;
		tree->list->next = root_link;
		tree->height = 1;
		bplustree_node_set_key(tree->root, 0, key, info);
		bplustree_node_set_value(tree->root, 0, value, info);
		tree->root->num_items = 1;
		return true;
	}
	struct _bplustree_node *node = tree->root;
	struct _bplustree_pos path[BPLUSTREE_MAX_HEIGHT];
	unsigned int level = tree->height - 1;
	unsigned int idx;
	bool found;
	for (;;) {
		found = bplustree_node_search(node, key, &idx, info);
		if (found && !update) {
			return false;
		}
		path[level].node = node;
		if (level == 0) {
			path[level].idx = idx;
			break;
		}
		// if we find the key we go to the right subtree, which means incrementing idx by 1
		idx += found;
		path[level].idx = idx;
		level--;
		node = bplustree_node_get_child(node, idx, info);
	}
	if (found) {
		// assert(update);
		if (info->destroy_key) {
			info->destroy_key(bplustree_node_key(node, idx, info));
		}
		if (info->destroy_value) {
			info->destroy_value(bplustree_node_value(node, idx, info));
		}
		bplustree_node_set_key(node, idx, key, info);
		bplustree_node_set_value(node, idx, value, info);
		return false;
	}
	bplustree_insert_and_rebalance(tree, key, value, path, info);
	return true;
}

bool _bplustree_insert_sequential(struct _bplustree *tree, void *key, void *value,
				  const struct bplustree_info *info)
{
	if (tree->height == 0) {
		return _bplustree_insert(tree, key, value, false, info);
	}
	struct _bplustree_node *node = tree->root;
	struct _bplustree_pos path[BPLUSTREE_MAX_HEIGHT];
	unsigned int level = tree->height - 1;
	unsigned int idx;
	for (;;) {
		idx = node->num_items;
		if (unlikely(info->cmp(key, bplustree_node_key(node, idx - 1, info)) <= 0)) {
			return _bplustree_insert(tree, key, value, false, info);
		}
		path[level].idx = idx;
		path[level].node = node;
		if (level-- == 0) {
			break;
		}
		node = bplustree_node_get_child(node, idx, info);
	}
	bplustree_insert_and_rebalance(tree, key, value, path, info);
	return true;
}

void _bplustree_bulk_load_start(struct _bplustree_bulk_load_ctx *ctx)
{
	_bplustree_init(&ctx->tree);
	ctx->list = NULL;
	ctx->node = NULL;
}

void _bplustree_bulk_load_next(struct _bplustree_bulk_load_ctx *ctx, void *key, void *value,
			       const struct bplustree_info *info)
{
	if (!ctx->list) {
		ctx->list = malloc(sizeof(*ctx->list));
		ctx->list->next = ctx->list;
		ctx->list->prev = ctx->list;
	}
	if (!ctx->node) {
		ctx->node = bplustree_new_node(true, info);
		struct _bplustree_leaf_link *link = bplustree_leaf_link(ctx->node, info);
		link->prev = ctx->list->prev;
		link->next = ctx->list;
		ctx->list->prev->next = link;
		ctx->list->prev = link;
	}
	bplustree_node_set_key(ctx->node, ctx->node->num_items, key, info);
	bplustree_node_set_value(ctx->node, ctx->node->num_items, value, info);
	ctx->node->num_items++;
	if (ctx->node->num_items == info->max_items) {
		ctx->node = NULL;
	}
}

void _bplustree_bulk_load_end(struct _bplustree_bulk_load_ctx *ctx, const struct bplustree_info *info)
{
	if (!ctx->list) {
		return;
	}
	struct _bplustree_pos path[BPLUSTREE_MAX_HEIGHT];
	struct _bplustree_leaf_link *link = ctx->list->next;
	path[0].node = bplustree_leaf_link_to_node(link, info);
	ctx->tree.root = path[0].node;
	ctx->tree.list = ctx->list;
	ctx->tree.height = 1;
	for (link = link->next;
	     link != ctx->list;
	     link = link->next) {
		struct _bplustree_node *node = bplustree_leaf_link_to_node(link, info);
		bplustree_internal_insert_and_rebalance(&ctx->tree, node, path, true, info);
		path[0].node = node;
	}
	struct _bplustree_node *parent = ctx->tree.root;
	for (unsigned int level = ctx->tree.height - 1; level-- > 0;) {
		struct _bplustree_node *node = path[level].node;
		if (node->num_items >= info->min_items) {
			parent = node;
			continue;
		}
		unsigned int idx_in_parent = parent->num_items;
		// assert(idx_in_parent != 0);
		struct _bplustree_node *left = bplustree_node_get_child(parent, idx_in_parent - 1, info);
		// assert(left->num_items == info->max_items);
		unsigned int n = info->min_items - node->num_items;
		memmove(bplustree_node_key(node, n, info),
			bplustree_node_key(node, 0, info),
			node->num_items * info->key_size);
		if (level != 0) {
			n--;
			bplustree_node_copy_key(node, n, parent, idx_in_parent - 1, info);
		}
		memcpy(bplustree_node_key(node, 0, info),
		       bplustree_node_key(left, left->num_items - n, info),
		       n * info->key_size);
		if (level != 0) {
			n = left->num_items - n - 1;
			bplustree_node_copy_key(parent, idx_in_parent - 1, left, n, info);
		} else {
			bplustree_node_copy_key(parent, idx_in_parent - 1, node, 0, info);
		}
		if (level == 0) {
			unsigned int n = info->min_items - node->num_items;
			memmove(bplustree_node_value(node, n, info),
				bplustree_node_value(node, 0, info),
				node->num_items * info->value_size);
			memcpy(bplustree_node_value(node, 0, info),
			       bplustree_node_value(left, left->num_items - n, info),
			       n * info->value_size);
		} else {
			unsigned int n = info->min_items - node->num_items;
			memmove(bplustree_node_children(node, info) + n,
				bplustree_node_children(node, info),
				(node->num_items + 1) * sizeof(struct _bplustree_node *));
			memcpy(bplustree_node_children(node, info),
			       bplustree_node_children(left, info) + left->num_items + 1 - n,
			       n * sizeof(struct _bplustree_node *));
		}
		left->num_items -= info->min_items - node->num_items;
		node->num_items = info->min_items;
		parent = node;
	}
}

void *_bplustree_debug_node_key(struct _bplustree_node *node, unsigned int idx,
				const struct bplustree_info *info)
{
	return bplustree_node_key(node, idx, info);
}

void *_bplustree_debug_node_value(struct _bplustree_node *node, unsigned int idx,
				  const struct bplustree_info *info)
{
	return bplustree_node_value(node, idx, info);
}

struct _bplustree_node *_bplustree_debug_node_get_child(struct _bplustree_node *node, unsigned int idx,
							const struct bplustree_info *info)
{
	return bplustree_node_get_child(node, idx, info);
}

static struct _bplustree_node *bplustree_node_copy(struct _bplustree_node *node, unsigned int level,
						   struct _bplustree_leaf_link *list,
						   const struct bplustree_info *info)
{
	struct _bplustree_node *copy = bplustree_new_node(level == 0, info);
	memcpy(bplustree_node_key(copy, 0, info),
	       bplustree_node_key(node, 0, info),
	       node->num_items * info->key_size);
	copy->num_items = node->num_items;
	if (level == 0) {
		memcpy(bplustree_node_value(copy, 0, info),
		       bplustree_node_value(node, 0, info),
		       node->num_items * info->value_size);
		struct _bplustree_leaf_link *link = bplustree_leaf_link(copy, info);
		list->prev->next = link;
		link->prev = list->prev;
		link->next = list;
		list->prev = link;
		return copy;
	}
	for (unsigned int i = 0; i < node->num_items + 1u; i++) {
		struct _bplustree_node *child = bplustree_node_get_child(node, i, info);
		struct _bplustree_node *child_copy = bplustree_node_copy(child, level - 1, list, info);
		bplustree_node_set_child(copy, i, child_copy, info);
	}
	return copy;
}

struct _bplustree _bplustree_debug_copy(const struct _bplustree *tree, const struct bplustree_info *info)
{
	unsigned int height = tree->height;
	struct _bplustree copy;
	copy.height = height;
	copy.root = NULL;
	if (height != 0) {
		copy.list = malloc(sizeof(*copy.list));
		copy.list->next = copy.list;
		copy.list->prev = copy.list;
		copy.root = bplustree_node_copy(tree->root, height - 1, copy.list, info);
	}
	return copy;
}

size_t _bplustree_debug_node_size(bool leaf, bool root, const struct bplustree_info *info)
{
	(void)root;
	return bplustree_node_size(leaf, info);
}
