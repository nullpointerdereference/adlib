/*
 * Copyright (C) 2024 Fabian Hügel
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <stdbool.h>
#include <stddef.h>
#include <limits.h>
#include "compiler.h"

struct _bstartree_node {
	unsigned short num_items;
	bool root;
	// bool leaf;
	unsigned char data[];
	/* char padding[info->key_alignment_offset] */
	/* if (root) { */
	/*   item_t items[2 * info->min_items]; */
	/*   char padding[info->root_children_alignment_offset] */
	/*   struct _bstartree_node *children[2 * info->min_items + 1]; */
	/* } else { */
	/*   item_t items[info->max_items]; */
	/*   char padding[leaf ? 0 : info->children_alignment_offset] */
	/*   struct _bstartree_node *children[leaf ? 0 : info->max_items + 1]; */
	/* } */
};

struct _bstartree {
	struct _bstartree_node *root;
	unsigned char height; // 0 means root is NULL, 1 means root is leaf
};

#define __BSTARTREE_MAX_HEIGHT 48

struct _bstartree_pos {
	struct _bstartree_node *node;
	unsigned int idx;
};

struct _bstartree_iter {
	const struct _bstartree *tree;
	unsigned int level;
	struct _bstartree_pos path[__BSTARTREE_MAX_HEIGHT];
};

struct _bstartree_bulk_load_ctx {
	struct _bstartree tree;
	struct _bstartree_pos path[__BSTARTREE_MAX_HEIGHT];
};

struct bstartree_info {
	size_t item_size;
	unsigned short max_items;
	unsigned short min_items; // dont really need to store this
	unsigned char key_alignment_offset;
	unsigned char children_alignment_offset;
	unsigned char root_children_alignment_offset;
	unsigned char linear_search_threshold;
	int (*cmp)(const void *a, const void *b);
	void (*destroy_item)(void *item);
};

enum bstartree_iter_start_at_mode {
	BSTARTREE_ITER_FIND_KEY,
	BSTARTREE_ITER_LOWER_BOUND_INCLUSIVE,
	BSTARTREE_ITER_LOWER_BOUND_EXCLUSIVE,
	BSTARTREE_ITER_UPPER_BOUND_INCLUSIVE,
	BSTARTREE_ITER_UPPER_BOUND_EXCLUSIVE,
};

enum _bstartree_deletion_mode {
	__BSTARTREE_DELETE_MIN,
	__BSTARTREE_DELETE_MAX,
	__BSTARTREE_DELETE_KEY,
};

// use a simple heuristic to determine the threshold at which linear search becomes faster than binary search
// TODO add float and double?
#define __BSTARTREE_LINEAR_SEARCH_THRESHOLD(type) _Generic(*(type *)0,	\
							   char : 32,	\
							   unsigned char : 32, \
							   unsigned short : 32, \
							   unsigned int : 32, \
							   unsigned long : 32, \
							   unsigned long long : 32, \
							   signed char : 32, \
							   signed short : 32, \
							   signed int : 32, \
							   signed long : 32, \
							   signed long long : 32, \
							   char *: 8,	\
							   const char *:  8, \
							   default: 0)

#define BSTARTREE_EMPTY {{.root = NULL, .height = 0}}

_Static_assert(_Alignof(max_align_t) >= 2 * sizeof(void *),
	       "malloc memory needs to be aligned to 2 * sizeof(void *) to guarantee proper alignment");

#define DEFINE_BSTARTREE_SET(name, key_type, key_destructor, max_items_per_node, ...) \
	typedef key_type name##_key_t;					\
	typedef void (*name##_key_destructor)(name##_key_t key);	\
									\
	struct name {							\
		struct _bstartree _impl;				\
	};								\
									\
	static int _##name##_compare(const void *_a, const void *_b)	\
	{								\
		const name##_key_t a = *(const name##_key_t *)_a;	\
		const name##_key_t b = *(const name##_key_t *)_b;	\
		return (__VA_ARGS__);					\
	}								\
									\
	static void _##name##_destroy_item(void *item)			\
	{								\
		name##_key_destructor destructor = (key_destructor);	\
		if (destructor) {					\
			destructor(*(name##_key_t *)item);		\
		}							\
	}								\
									\
	_Static_assert((max_items_per_node) >= 3, "use an AVL or RB tree for 1 item per node");	\
	_Static_assert((max_items_per_node) <= USHRT_MAX, "cannot have more than USHRT_MAX items per node"); \
	_Static_assert(_Alignof(name##_key_t) <= 2 * sizeof(void *), "cannot guarantee alignment > 2 * sizeof(void *)"); \
									\
	static constexpr_or_const unsigned char _##name##_key_alignment_offset = (_Alignof(name##_key_t) - (offsetof(struct _bstartree_node, data) % _Alignof(name##_key_t))) % _Alignof(name##_key_t); \
	static constexpr_or_const unsigned char _##name##_children_alignment_offset = (_Alignof(void *) - ((offsetof(struct _bstartree_node, data) + _##name##_key_alignment_offset + (max_items_per_node) * sizeof(name##_key_t)) % _Alignof(void *))) % _Alignof(void *); \
	static constexpr_or_const unsigned char _##name##_min_items = 2 * (max_items_per_node) / 3; \
	static constexpr_or_const unsigned char _##name##_root_children_alignment_offset = (_Alignof(void *) - ((offsetof(struct _bstartree_node, data) + _##name##_key_alignment_offset + 2 * _##name##_min_items * sizeof(name##_key_t)) % _Alignof(void *))) % _Alignof(void *); \
									\
	static _Alignas(32) const struct bstartree_info name##_info = {	\
		.max_items = (max_items_per_node),			\
		.min_items = _##name##_min_items,			\
		.item_size = sizeof(name##_key_t),			\
		.key_alignment_offset = _##name##_key_alignment_offset, \
		.children_alignment_offset = _##name##_children_alignment_offset, \
		.root_children_alignment_offset = _##name##_root_children_alignment_offset, \
		.linear_search_threshold = __BSTARTREE_LINEAR_SEARCH_THRESHOLD(name##_key_t), \
		.cmp = _##name##_compare,				\
		.destroy_item = (key_destructor) ? _##name##_destroy_item : NULL, \
	};								\
									\
	typedef struct _bstartree_iter name##_iter_t;			\
									\
	static _attr_unused const name##_key_t *name##_iter_start_leftmost(name##_iter_t *iter, \
									   const struct name *tree) \
	{								\
		return _bstartree_iter_start(iter, &tree->_impl, false, &name##_info); \
	}								\
									\
	static _attr_unused const name##_key_t *name##_iter_start_rightmost(name##_iter_t *iter, \
									    const struct name *tree) \
	{								\
		return _bstartree_iter_start(iter, &tree->_impl, true, &name##_info); \
	}								\
									\
	static _attr_unused const name##_key_t *name##_iter_start_at(name##_iter_t *iter, \
								     const struct name *tree, \
								     name##_key_t key, \
								     enum bstartree_iter_start_at_mode mode) \
	{								\
		return _bstartree_iter_start_at(iter, &tree->_impl, &key, mode, &name##_info); \
	}								\
									\
	static _attr_unused const name##_key_t *name##_iter_next(name##_iter_t *iter) \
	{								\
		return _bstartree_iter_next(iter, &name##_info);	\
	}								\
									\
	static _attr_unused const name##_key_t *name##_iter_prev(name##_iter_t *iter) \
	{								\
		return _bstartree_iter_prev(iter, &name##_info);	\
	}								\
									\
	static _attr_unused void name##_init(struct name *tree)		\
	{								\
		_bstartree_init(&tree->_impl);				\
	}								\
									\
	static _attr_unused void name##_destroy(struct name *tree)	\
	{								\
		_bstartree_destroy(&tree->_impl, &name##_info);		\
	}								\
									\
	static _attr_unused const name##_key_t *name##_find(const struct name *tree, name##_key_t key) \
	{								\
		return (const name##_key_t *)_bstartree_find(&tree->_impl, &key, &name##_info); \
	}								\
									\
	static _attr_unused const name##_key_t *name##_get_leftmost(const struct name *tree) \
	{								\
		return _bstartree_get_leftmost_rightmost(&tree->_impl, true, &name##_info); \
	}								\
									\
	static _attr_unused const name##_key_t *name##_get_rightmost(const struct name *tree) \
	{								\
		return _bstartree_get_leftmost_rightmost(&tree->_impl, false, &name##_info); \
	}								\
									\
	static _attr_unused bool name##_delete(struct name *tree, name##_key_t key, name##_key_t *ret_key) \
	{								\
		return _bstartree_delete(&tree->_impl, __BSTARTREE_DELETE_KEY, &key, (void *)ret_key, &name##_info); \
	}								\
									\
	static _attr_unused bool name##_delete_min(struct name *tree, name##_key_t *ret_key) \
	{								\
		return _bstartree_delete(&tree->_impl, __BSTARTREE_DELETE_MIN, NULL, (void *)ret_key, &name##_info); \
	}								\
									\
	static _attr_unused bool name##_delete_max(struct name *tree, name##_key_t *ret_key) \
	{								\
		return _bstartree_delete(&tree->_impl, __BSTARTREE_DELETE_MAX, NULL, (void *)ret_key, &name##_info); \
	}								\
									\
	static _attr_unused bool name##_insert(struct name *tree, name##_key_t key) \
	{								\
		return _bstartree_insert(&tree->_impl, &key, false, &name##_info); \
	}								\
									\
	/* TODO does this even make sense for a bstartree set? */	\
	/* I guess you can use this to destruct and replace a key... */	\
	static _attr_unused bool name##_set(struct name *tree, name##_key_t key) \
	{								\
		return _bstartree_insert(&tree->_impl, &key, true, &name##_info); \
	}								\
									\
	static _attr_unused bool name##_insert_sequential(struct name *tree, name##_key_t key) \
	{								\
		return _bstartree_insert_sequential(&tree->_impl, &key, &name##_info); \
	}								\
									\
	typedef struct _bstartree_bulk_load_ctx name##_bulk_load_ctx_t;	\
									\
	static _attr_unused name##_bulk_load_ctx_t name##_bulk_load_start(void) \
	{								\
		name##_bulk_load_ctx_t ctx;				\
		_bstartree_bulk_load_start(&ctx);			\
		return ctx;						\
	}								\
									\
	static _attr_unused void name##_bulk_load_next(name##_bulk_load_ctx_t *ctx, name##_key_t key) \
	{								\
		_bstartree_bulk_load_next(ctx, &key, &name##_info);	\
	}								\
									\
	static _attr_unused struct name name##_bulk_load_end(name##_bulk_load_ctx_t *ctx) \
	{								\
		_bstartree_bulk_load_end(ctx, &name##_info);		\
		return (struct name) { ._impl = ctx->tree };		\
	}								\
	_Static_assert(1, "allow semicolon without warning")

#define __BSTARTREE_MAP_RETURN_KEY_AND_VALUE	\
	if (!item) {				\
		return NULL;			\
	}					\
	if (ret_key) {				\
		*ret_key = item->key;		\
	}					\
	return &item->value

#define __BSTARTREE_MAP_DELETE_RETURN			\
	if (found) {					\
		if (ret_key) {				\
			*ret_key = item.key;		\
		}					\
		if (ret_value) {			\
			*ret_value = item.value;	\
		}					\
	}						\
	return found

#define DEFINE_BSTARTREE_MAP(name, key_type, value_type, key_destructor, value_destructor, max_items_per_node, ...) \
	typedef key_type name##_key_t;					\
	typedef value_type name##_value_t;				\
	typedef void (*name##_key_destructor)(name##_key_t key);	\
	typedef void (*name##_value_destructor)(name##_value_t value);	\
	typedef struct { name##_key_t key; name##_value_t value; } _##name##_item_t; \
									\
	struct name {							\
		struct _bstartree _impl;				\
	};								\
									\
	static int _##name##_compare(const void *_a, const void *_b)	\
	{								\
		const name##_key_t a = *(const name##_key_t *)_a;	\
		const name##_key_t b = *(const name##_key_t *)_b;	\
		return (__VA_ARGS__);					\
	}								\
									\
	static void _##name##_destroy_item(void *_item)			\
	{								\
		name##_key_destructor destroy_key = (key_destructor);	\
		name##_value_destructor destroy_value = (value_destructor); \
		_##name##_item_t *item = _item;				\
		if (destroy_key) {					\
			destroy_key(item->key);				\
		}							\
		if (destroy_value) {					\
			destroy_value(item->value);			\
		}							\
	}								\
									\
	_Static_assert((max_items_per_node) >= 3, "use an AVL or RB tree for 1 item per node");	\
	_Static_assert((max_items_per_node) <= USHRT_MAX, "cannot have more than USHRT_MAX items per node"); \
	_Static_assert(_Alignof(_##name##_item_t) <= 2 * sizeof(void *), "cannot guarantee alignment > 2 * sizeof(void *)"); \
									\
	static constexpr_or_const unsigned char _##name##_key_alignment_offset = (_Alignof(_##name##_item_t) - (offsetof(struct _bstartree_node, data) % _Alignof(_##name##_item_t))) % _Alignof(_##name##_item_t); \
	static constexpr_or_const unsigned char _##name##_children_alignment_offset = (_Alignof(void *) - ((offsetof(struct _bstartree_node, data) + _##name##_key_alignment_offset + (max_items_per_node) * sizeof(_##name##_item_t)) % _Alignof(void *))) % _Alignof(void *); \
	static constexpr_or_const unsigned char _##name##_min_items = 2 * (max_items_per_node) / 3; \
	static constexpr_or_const unsigned char _##name##_root_children_alignment_offset = (_Alignof(void *) - ((offsetof(struct _bstartree_node, data) + _##name##_key_alignment_offset + 2 * _##name##_min_items * sizeof(_##name##_item_t)) % _Alignof(void *))) % _Alignof(void *); \
									\
	static _Alignas(32) const struct bstartree_info name##_info = {	\
		.max_items = (max_items_per_node),			\
		.min_items = _##name##_min_items,			\
		.item_size = sizeof(_##name##_item_t),			\
		.key_alignment_offset = _##name##_key_alignment_offset, \
		.children_alignment_offset = _##name##_children_alignment_offset, \
		.root_children_alignment_offset = _##name##_root_children_alignment_offset, \
		.linear_search_threshold = __BSTARTREE_LINEAR_SEARCH_THRESHOLD(name##_key_t), \
		.cmp = _##name##_compare,				\
		.destroy_item = ((key_destructor) || (value_destructor)) ? _##name##_destroy_item : NULL, \
	};								\
									\
	typedef struct _bstartree_iter name##_iter_t;			\
									\
	static _attr_unused name##_value_t *name##_iter_start_leftmost(name##_iter_t *iter, \
								       const struct name *tree, \
								       name##_key_t *ret_key) \
	{								\
		_##name##_item_t *item = _bstartree_iter_start(iter, &tree->_impl, false, &name##_info); \
		__BSTARTREE_MAP_RETURN_KEY_AND_VALUE;			\
	}								\
									\
	static _attr_unused name##_value_t *name##_iter_start_rightmost(name##_iter_t *iter, \
									const struct name *tree, \
									name##_key_t *ret_key) \
	{								\
		_##name##_item_t *item = _bstartree_iter_start(iter, &tree->_impl, true, &name##_info); \
		__BSTARTREE_MAP_RETURN_KEY_AND_VALUE;			\
	}								\
									\
	static _attr_unused name##_value_t *name##_iter_start_at(name##_iter_t *iter, const struct name *tree, \
								 name##_key_t key, name##_key_t *ret_key, \
								 enum bstartree_iter_start_at_mode mode) \
	{								\
		_##name##_item_t *item = _bstartree_iter_start_at(iter, &tree->_impl, &key, mode, &name##_info); \
		__BSTARTREE_MAP_RETURN_KEY_AND_VALUE;			\
	}								\
									\
	static _attr_unused name##_value_t *name##_iter_next(name##_iter_t *iter, name##_key_t *ret_key) \
	{								\
		_##name##_item_t *item = _bstartree_iter_next(iter, &name##_info); \
		__BSTARTREE_MAP_RETURN_KEY_AND_VALUE;			\
	}								\
									\
	static _attr_unused name##_value_t *name##_iter_prev(name##_iter_t *iter, name##_key_t *ret_key) \
	{								\
		_##name##_item_t *item = _bstartree_iter_prev(iter, &name##_info); \
		__BSTARTREE_MAP_RETURN_KEY_AND_VALUE;			\
	}								\
									\
	static _attr_unused void name##_init(struct name *tree)		\
	{								\
		_bstartree_init(&tree->_impl);				\
	}								\
									\
	static _attr_unused void name##_destroy(struct name *tree)	\
	{								\
		_bstartree_destroy(&tree->_impl, &name##_info);		\
	}								\
									\
	static _attr_unused name##_value_t *name##_find(const struct name *tree, name##_key_t key) \
	{								\
		_##name##_item_t *item = _bstartree_find(&tree->_impl, &key, &name##_info); \
		return item ? &item->value : NULL;			\
	}								\
									\
	static _attr_unused name##_value_t *name##_get_leftmost(const struct name *tree, name##_key_t *ret_key) \
	{								\
		_##name##_item_t *item = _bstartree_get_leftmost_rightmost(&tree->_impl, true, &name##_info); \
		__BSTARTREE_MAP_RETURN_KEY_AND_VALUE;			\
	}								\
									\
	static _attr_unused name##_value_t *name##_get_rightmost(const struct name *tree, name##_key_t *ret_key) \
	{								\
		_##name##_item_t *item = _bstartree_get_leftmost_rightmost(&tree->_impl, false, &name##_info); \
		__BSTARTREE_MAP_RETURN_KEY_AND_VALUE;			\
	}								\
									\
	static _attr_unused bool name##_delete(struct name *tree, name##_key_t key, name##_key_t *ret_key, \
					       name##_value_t *ret_value) \
	{								\
		_##name##_item_t item;					\
		bool found = _bstartree_delete(&tree->_impl, __BSTARTREE_DELETE_KEY, &key, \
					       (ret_key || ret_value) ? &item : NULL, &name##_info); \
		__BSTARTREE_MAP_DELETE_RETURN;				\
	}								\
									\
	static _attr_unused bool name##_delete_min(struct name *tree, name##_key_t *ret_key, \
						   name##_value_t *ret_value) \
	{								\
		_##name##_item_t item;					\
		bool found = _bstartree_delete(&tree->_impl, __BSTARTREE_DELETE_MIN, NULL, \
					       (ret_key || ret_value) ? &item : NULL, &name##_info); \
		__BSTARTREE_MAP_DELETE_RETURN;				\
	}								\
									\
	static _attr_unused bool name##_delete_max(struct name *tree, name##_key_t *ret_key, \
						   name##_value_t *ret_value) \
	{								\
		_##name##_item_t item;					\
		bool found = _bstartree_delete(&tree->_impl, __BSTARTREE_DELETE_MAX, NULL, \
					       (ret_key || ret_value) ? &item : NULL, &name##_info); \
		__BSTARTREE_MAP_DELETE_RETURN;				\
	}								\
									\
	static _attr_unused bool name##_insert(struct name *tree, name##_key_t key, name##_value_t value) \
	{								\
		return _bstartree_insert(&tree->_impl, &(_##name##_item_t){.key = key, .value = value}, false, \
					 &name##_info);			\
	}								\
									\
	static _attr_unused bool name##_set(struct name *tree, name##_key_t key, name##_value_t value) \
	{								\
		return _bstartree_insert(&tree->_impl, &(_##name##_item_t){.key = key, .value = value}, true, \
					 &name##_info);			\
	}								\
									\
	static _attr_unused bool name##_insert_sequential(struct name *tree, name##_key_t key, \
							  name##_value_t value) \
	{								\
		return _bstartree_insert_sequential(&tree->_impl, &(_##name##_item_t){.key = key, .value = value}, \
						    &name##_info);	\
	}								\
									\
	typedef struct _bstartree_bulk_load_ctx name##_bulk_load_ctx_t;	\
									\
	static _attr_unused name##_bulk_load_ctx_t name##_bulk_load_start(void) \
	{								\
		name##_bulk_load_ctx_t ctx;				\
		_bstartree_bulk_load_start(&ctx);			\
		return ctx;						\
	}								\
									\
	static _attr_unused void name##_bulk_load_next(name##_bulk_load_ctx_t *ctx, name##_key_t key, \
						       name##_value_t value) \
	{								\
		_bstartree_bulk_load_next(ctx, &(_##name##_item_t){.key = key, .value = value}, \
					  &name##_info);		\
	}								\
									\
	static _attr_unused struct name name##_bulk_load_end(name##_bulk_load_ctx_t *ctx) \
	{								\
		_bstartree_bulk_load_end(ctx, &name##_info);		\
		return (struct name) { ._impl = ctx->tree };		\
	}								\
	_Static_assert(1, "allow semicolon without warning")

void *_bstartree_iter_start(struct _bstartree_iter *iter, const struct _bstartree *tree, bool rightmost,
			    const struct bstartree_info *info);
void *_bstartree_iter_next(struct _bstartree_iter *iter, const struct bstartree_info *info);
void *_bstartree_iter_prev(struct _bstartree_iter *iter, const struct bstartree_info *info);
void *_bstartree_iter_start_at(struct _bstartree_iter *iter, const struct _bstartree *tree, void *key,
			       enum bstartree_iter_start_at_mode mode, const struct bstartree_info *info);
void _bstartree_init(struct _bstartree *tree);
void _bstartree_destroy(struct _bstartree *tree, const struct bstartree_info *info);
void *_bstartree_find(const struct _bstartree *tree, const void *key, const struct bstartree_info *info);
void *_bstartree_get_leftmost_rightmost(const struct _bstartree *tree, bool leftmost, const struct bstartree_info *info) _attr_pure;
bool _bstartree_delete(struct _bstartree *tree, enum _bstartree_deletion_mode mode, const void *key, void *ret_item,
		       const struct bstartree_info *info);
bool _bstartree_insert(struct _bstartree *tree, void *item, bool update, const struct bstartree_info *info);
bool _bstartree_insert_sequential(struct _bstartree *tree, void *item, const struct bstartree_info *info);
void _bstartree_bulk_load_start(struct _bstartree_bulk_load_ctx *ctx);
void _bstartree_bulk_load_next(struct _bstartree_bulk_load_ctx *ctx, void *item,
			       const struct bstartree_info *info);
void _bstartree_bulk_load_end(struct _bstartree_bulk_load_ctx *ctx, const struct bstartree_info *info);

#ifdef __ADLIB_TESTS__
void *_bstartree_debug_node_item(struct _bstartree_node *node, unsigned int idx, const struct bstartree_info *info);
struct _bstartree_node *_bstartree_debug_node_get_child(struct _bstartree_node *node, unsigned int idx,
							const struct bstartree_info *info);
struct _bstartree _bstartree_debug_copy(const struct _bstartree *tree, const struct bstartree_info *info);
size_t _bstartree_debug_node_size(bool leaf, bool root, const struct bstartree_info *info);
#endif
