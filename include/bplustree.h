/*
 * Copyright (C) 2024 Fabian Hügel
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <stdbool.h>
#include <stddef.h>
#include <limits.h>
#include "compiler.h"

struct _bplustree_leaf_link {
	struct _bplustree_leaf_link *prev;
	struct _bplustree_leaf_link *next;
};

struct _bplustree_node {
	/* struct _bplustree_leaf_link link[leaf ? 1 : 0]; */
	unsigned short num_items;
	unsigned char data[];
	/* char padding[info->key_alignment_offset] */
	/* key_t keys[info->max_items]; */
	/* char padding[info->value_alignment_offset] */
	/* struct _bplustree_node *children[leaf ? 0 : info->max_items + 1]; */
	/* value_t *values[leaf ? info->max_items : 0]; */
};

struct _bplustree {
	struct _bplustree_node *root;
	unsigned char height; // 0 means root is NULL, 1 means root is leaf
	struct _bplustree_leaf_link *list;
};

struct _bplustree_iter {
	const struct _bplustree *tree;
	struct _bplustree_node *node;
	unsigned int idx;
};

struct _bplustree_bulk_load_ctx {
	struct _bplustree tree;
	struct _bplustree_leaf_link *list;
	struct _bplustree_node *node;
};

struct bplustree_info {
	size_t key_size;
	size_t value_size;
	unsigned short max_items;
	unsigned short min_items; // dont really need to store this
	unsigned char key_alignment_offset;
	unsigned char value_alignment_offset;
	unsigned short linear_search_threshold;
	int (*cmp)(const void *a, const void *b);
	void (*destroy_key)(void *key);
	void (*destroy_value)(void *value);
};

typedef struct _bplustree_key_and_value {
	const void *key;
	void *value;
} _bplustree_kv_t;

enum bplustree_iter_start_at_mode {
	BPLUSTREE_ITER_FIND_KEY,
	BPLUSTREE_ITER_LOWER_BOUND_INCLUSIVE,
	BPLUSTREE_ITER_LOWER_BOUND_EXCLUSIVE,
	BPLUSTREE_ITER_UPPER_BOUND_INCLUSIVE,
	BPLUSTREE_ITER_UPPER_BOUND_EXCLUSIVE,
};

enum _bplustree_deletion_mode {
	__BPLUSTREE_DELETE_MIN,
	__BPLUSTREE_DELETE_MAX,
	__BPLUSTREE_DELETE_KEY,
};

// use a simple heuristic to determine the threshold at which linear search becomes faster than binary search
// TODO add float and double?
#define __BPLUSTREE_LINEAR_SEARCH_THRESHOLD(type) _Generic(*(type *)0,	\
							   char : 32,	\
							   unsigned char : 32, \
							   unsigned short : 32, \
							   unsigned int : 32, \
							   unsigned long : 32, \
							   unsigned long long : 32, \
							   signed char : 32, \
							   signed short : 32, \
							   signed int : 32, \
							   signed long : 32, \
							   signed long long : 32, \
							   char *: 8,	\
							   const char *:  8, \
							   default: 0)

#define BPLUSTREE_EMPTY {{.root = NULL, .height = 0, .list = NULL}}

_Static_assert(_Alignof(max_align_t) >= 2 * sizeof(void *),
	       "malloc memory needs to be aligned to 2 * sizeof(void *) to guarantee proper alignment");

#define DEFINE_BPLUSTREE_SET(name, key_type, key_destructor, max_items_per_node, ...) \
	typedef key_type name##_key_t;					\
	typedef void (*name##_key_destructor)(name##_key_t key);	\
									\
	struct name {							\
		struct _bplustree _impl;				\
	};								\
									\
	static int _##name##_compare(const void *_a, const void *_b)	\
	{								\
		const name##_key_t a = *(const name##_key_t *)_a;	\
		const name##_key_t b = *(const name##_key_t *)_b;	\
		return (__VA_ARGS__);					\
	}								\
									\
	static void _##name##_destroy_key(void *_key)			\
	{								\
		name##_key_destructor destroy_key = (key_destructor);	\
		name##_key_t *key = _key;				\
		if (destroy_key) {					\
			destroy_key(*key);				\
		}							\
	}								\
									\
	_Static_assert((max_items_per_node) >= 2, "use an AVL or RB tree for 1 item per node");	\
	_Static_assert((max_items_per_node) <= USHRT_MAX, "cannot have more than USHRT_MAX items per node"); \
	_Static_assert(_Alignof(name##_key_t) <= 2 * sizeof(void *), "cannot guarantee alignment > 2 * sizeof(void *)"); \
									\
	static constexpr_or_const unsigned char _##name##_key_alignment_offset = (_Alignof(name##_key_t) - (sizeof(struct _bplustree_node) % _Alignof(name##_key_t))) % _Alignof(name##_key_t); \
	static constexpr_or_const unsigned char _##name##_value_alignment_offset = (_Alignof(void *) - ((sizeof(struct _bplustree_node) + _##name##_key_alignment_offset + (max_items_per_node) * sizeof(name##_key_t)) % _Alignof(void *))) % _Alignof(void *); \
									\
	static _Alignas(64) const struct bplustree_info name##_info = {	\
		.max_items = (max_items_per_node),			\
		.min_items = (max_items_per_node) / 2,			\
		.key_size = sizeof(name##_key_t),			\
		.value_size = 0,					\
		.key_alignment_offset = _##name##_key_alignment_offset, \
		.value_alignment_offset = _##name##_value_alignment_offset, \
		.linear_search_threshold = __BPLUSTREE_LINEAR_SEARCH_THRESHOLD(name##_key_t), \
		.cmp = _##name##_compare,				\
		.destroy_key = (key_destructor) ? _##name##_destroy_key : NULL, \
		.destroy_value = NULL,					\
	};								\
									\
	typedef struct _bplustree_iter name##_iter_t;			\
									\
	static _attr_unused const name##_key_t *name##_iter_start_leftmost(name##_iter_t *iter, \
									   const struct name *tree) \
	{								\
		return _bplustree_iter_start(iter, &tree->_impl, false, &name##_info).key; \
	}								\
									\
	static _attr_unused const name##_key_t *name##_iter_start_rightmost(name##_iter_t *iter, \
									    const struct name *tree) \
	{								\
		return _bplustree_iter_start(iter, &tree->_impl, true, &name##_info).key; \
	}								\
									\
	static _attr_unused const name##_key_t *name##_iter_start_at(name##_iter_t *iter, \
								     const struct name *tree, \
								     name##_key_t key, \
								     enum bplustree_iter_start_at_mode mode) \
	{								\
		return _bplustree_iter_start_at(iter, &tree->_impl, &key, mode, &name##_info).key; \
	}								\
									\
	static _attr_unused const name##_key_t *name##_iter_next(name##_iter_t *iter) \
	{								\
		return _bplustree_iter_next(iter, &name##_info).key;	\
	}								\
									\
	static _attr_unused const name##_key_t *name##_iter_prev(name##_iter_t *iter) \
	{								\
		return _bplustree_iter_prev(iter, &name##_info).key;	\
	}								\
									\
	static _attr_unused void name##_init(struct name *tree)		\
	{								\
		_bplustree_init(&tree->_impl);				\
	}								\
									\
	static _attr_unused void name##_destroy(struct name *tree)	\
	{								\
		_bplustree_destroy(&tree->_impl, &name##_info);		\
	}								\
									\
	static _attr_unused const name##_key_t *name##_find(const struct name *tree, name##_key_t key) \
	{								\
		return _bplustree_find(&tree->_impl, &key, &name##_info).key; \
	}								\
									\
	static _attr_unused const name##_key_t *name##_get_leftmost(const struct name *tree) \
	{								\
		return _bplustree_get_leftmost_rightmost(&tree->_impl, true, &name##_info).key; \
	}								\
									\
	static _attr_unused const name##_key_t *name##_get_rightmost(const struct name *tree) \
	{								\
		return _bplustree_get_leftmost_rightmost(&tree->_impl, false, &name##_info).key; \
	}								\
									\
	static _attr_unused bool name##_delete(struct name *tree, name##_key_t key, name##_key_t *ret_key) \
	{								\
		return _bplustree_delete(&tree->_impl, __BPLUSTREE_DELETE_KEY, &key, ret_key, NULL, &name##_info); \
	}								\
									\
	static _attr_unused bool name##_delete_min(struct name *tree, name##_key_t *ret_key) \
	{								\
		return _bplustree_delete(&tree->_impl, __BPLUSTREE_DELETE_MIN, NULL, ret_key, NULL, &name##_info); \
	}								\
									\
	static _attr_unused bool name##_delete_max(struct name *tree, name##_key_t *ret_key) \
	{								\
		return _bplustree_delete(&tree->_impl, __BPLUSTREE_DELETE_MAX, NULL, ret_key, NULL, &name##_info); \
	}								\
									\
	static _attr_unused bool name##_insert(struct name *tree, name##_key_t key) \
	{								\
		/* pass &key as the value pointer to avoid undefined behavior with memcpy(..., value, 0) */ \
		return _bplustree_insert(&tree->_impl, &key, &key, false, &name##_info); \
	}								\
									\
	/* TODO does this even make sense for a bplustree set? */	\
	/* I guess you can use this to destruct and replace a key... */	\
	static _attr_unused bool name##_set(struct name *tree, name##_key_t key) \
	{								\
		return _bplustree_insert(&tree->_impl, &key, &key /* dummy value */, true, &name##_info); \
	}								\
									\
	static _attr_unused bool name##_insert_sequential(struct name *tree, name##_key_t key) \
	{								\
		return _bplustree_insert_sequential(&tree->_impl, &key, &key /* dummy value */, \
						    &name##_info);	\
	}								\
									\
	typedef struct _bplustree_bulk_load_ctx name##_bulk_load_ctx_t;	\
									\
	static _attr_unused name##_bulk_load_ctx_t name##_bulk_load_start(void) \
	{								\
		name##_bulk_load_ctx_t ctx;				\
		_bplustree_bulk_load_start(&ctx);			\
		return ctx;						\
	}								\
									\
	static _attr_unused void name##_bulk_load_next(name##_bulk_load_ctx_t *ctx, name##_key_t key) \
	{								\
		_bplustree_bulk_load_next(ctx, &key, &key /* dummy value */, &name##_info); \
	}								\
									\
	static _attr_unused struct name name##_bulk_load_end(name##_bulk_load_ctx_t *ctx) \
	{								\
		_bplustree_bulk_load_end(ctx, &name##_info);		\
		return (struct name) { ._impl = ctx->tree };		\
	}								\
	_Static_assert(1, "allow semicolon without warning")

#define _BPLUSTREE_MAP_RETURN_KEY_AND_VALUE		\
	if (ret_key && kv.key) {			\
		*ret_key = *(typeof(ret_key))kv.key;	\
	}						\
	return kv.value

#define DEFINE_BPLUSTREE_MAP(name, key_type, value_type, key_destructor, value_destructor, max_items_per_node, ...) \
	typedef key_type name##_key_t;					\
	typedef value_type name##_value_t;				\
	typedef void (*name##_key_destructor)(name##_key_t key);	\
	typedef void (*name##_value_destructor)(name##_value_t value);	\
									\
	struct name {							\
		struct _bplustree _impl;				\
	};								\
									\
	static int _##name##_compare(const void *_a, const void *_b)	\
	{								\
		const name##_key_t a = *(const name##_key_t *)_a;	\
		const name##_key_t b = *(const name##_key_t *)_b;	\
		return (__VA_ARGS__);					\
	}								\
									\
	static void _##name##_destroy_key(void *_key)			\
	{								\
		name##_key_destructor destroy_key = (key_destructor);	\
		name##_key_t *key = _key;				\
		if (destroy_key) {					\
			destroy_key(*key);				\
		}							\
	}								\
									\
	static void _##name##_destroy_value(void *_value)		\
	{								\
		name##_value_destructor destroy_value = (value_destructor); \
		name##_value_t *value = _value;				\
		if (destroy_value) {					\
			destroy_value(*value);				\
		}							\
	}								\
									\
	_Static_assert((max_items_per_node) >= 2, "use an AVL or RB tree for 1 item per node");	\
	_Static_assert((max_items_per_node) <= USHRT_MAX, "cannot have more than USHRT_MAX items per node"); \
	_Static_assert(_Alignof(name##_key_t) <= 2 * sizeof(void *), "cannot guarantee alignment > 2 * sizeof(void *)"); \
	_Static_assert(_Alignof(name##_value_t) <= 2 * sizeof(void *), "cannot guarantee alignment > 2 * sizeof(void *)"); \
									\
	static constexpr_or_const unsigned char _##name##_value_align = _Alignof(name##_value_t) < _Alignof(void *) ? _Alignof(void *) : _Alignof(name##_value_t); \
	static constexpr_or_const unsigned char _##name##_key_alignment_offset = (_Alignof(name##_key_t) - (sizeof(struct _bplustree_node) % _Alignof(name##_key_t))) % _Alignof(name##_key_t); \
	static constexpr_or_const unsigned char _##name##_value_alignment_offset = (_##name##_value_align - ((sizeof(struct _bplustree_node) + _##name##_key_alignment_offset + (max_items_per_node) * sizeof(name##_key_t)) % _##name##_value_align)) % _##name##_value_align; \
									\
	static _Alignas(64) const struct bplustree_info name##_info = {	\
		.max_items = (max_items_per_node),			\
		.min_items = (max_items_per_node) / 2,			\
		.key_size = sizeof(name##_key_t),			\
		.value_size = sizeof(name##_value_t),			\
		.key_alignment_offset = _##name##_key_alignment_offset,	\
		.value_alignment_offset = _##name##_value_alignment_offset, \
		.linear_search_threshold = __BPLUSTREE_LINEAR_SEARCH_THRESHOLD(name##_key_t), \
		.cmp = _##name##_compare,				\
		.destroy_key = (key_destructor) ? _##name##_destroy_key : NULL, \
		.destroy_value = (value_destructor) ? _##name##_destroy_value : NULL, \
	};								\
									\
	typedef struct _bplustree_iter name##_iter_t;			\
									\
	static _attr_unused name##_value_t *name##_iter_start_leftmost(name##_iter_t *iter, \
								       const struct name *tree, \
								       name##_key_t *ret_key) \
	{								\
		_bplustree_kv_t kv = _bplustree_iter_start(iter, &tree->_impl, false, &name##_info); \
		_BPLUSTREE_MAP_RETURN_KEY_AND_VALUE;			\
	}								\
									\
	static _attr_unused name##_value_t *name##_iter_start_rightmost(name##_iter_t *iter, \
									const struct name *tree, \
									name##_key_t *ret_key) \
	{								\
		_bplustree_kv_t kv = _bplustree_iter_start(iter, &tree->_impl, true, &name##_info); \
		_BPLUSTREE_MAP_RETURN_KEY_AND_VALUE;			\
	}								\
									\
	static _attr_unused name##_value_t *name##_iter_start_at(name##_iter_t *iter, const struct name *tree, \
								 name##_key_t key, name##_key_t *ret_key, \
								 enum bplustree_iter_start_at_mode mode) \
	{								\
		_bplustree_kv_t kv = _bplustree_iter_start_at(iter, &tree->_impl, &key, mode, &name##_info); \
		_BPLUSTREE_MAP_RETURN_KEY_AND_VALUE;			\
	}								\
									\
	static _attr_unused name##_value_t *name##_iter_next(name##_iter_t *iter, name##_key_t *ret_key) \
	{								\
		_bplustree_kv_t kv = _bplustree_iter_next(iter, &name##_info); \
		_BPLUSTREE_MAP_RETURN_KEY_AND_VALUE;			\
	}								\
									\
	static _attr_unused name##_value_t *name##_iter_prev(name##_iter_t *iter, name##_key_t *ret_key) \
	{								\
		_bplustree_kv_t kv = _bplustree_iter_prev(iter, &name##_info); \
		_BPLUSTREE_MAP_RETURN_KEY_AND_VALUE;			\
	}								\
									\
	static _attr_unused void name##_init(struct name *tree)		\
	{								\
		_bplustree_init(&tree->_impl);				\
	}								\
									\
	static _attr_unused void name##_destroy(struct name *tree)	\
	{								\
		_bplustree_destroy(&tree->_impl, &name##_info);		\
	}								\
									\
	static _attr_unused name##_value_t *name##_find(const struct name *tree, name##_key_t key) \
	{								\
		_bplustree_kv_t kv = _bplustree_find(&tree->_impl, &key, &name##_info); \
		return kv.value;					\
	}								\
									\
	static _attr_unused name##_value_t *name##_get_leftmost(const struct name *tree, name##_key_t *ret_key) \
	{								\
		_bplustree_kv_t kv = _bplustree_get_leftmost_rightmost(&tree->_impl, true, &name##_info); \
		_BPLUSTREE_MAP_RETURN_KEY_AND_VALUE;			\
	}								\
									\
	static _attr_unused name##_value_t *name##_get_rightmost(const struct name *tree, name##_key_t *ret_key) \
	{								\
		_bplustree_kv_t kv = _bplustree_get_leftmost_rightmost(&tree->_impl, false, &name##_info); \
		_BPLUSTREE_MAP_RETURN_KEY_AND_VALUE;			\
	}								\
									\
	static _attr_unused bool name##_delete(struct name *tree, name##_key_t key, name##_key_t *ret_key, \
					       name##_value_t *ret_value) \
	{								\
		return _bplustree_delete(&tree->_impl, __BPLUSTREE_DELETE_KEY, &key, \
					 ret_key, ret_value, &name##_info); \
	}								\
									\
	static _attr_unused bool name##_delete_min(struct name *tree, name##_key_t *ret_key, \
						   name##_value_t *ret_value) \
	{								\
		return _bplustree_delete(&tree->_impl, __BPLUSTREE_DELETE_MIN, NULL, \
					 ret_key, ret_value, &name##_info); \
	}								\
									\
	static _attr_unused bool name##_delete_max(struct name *tree, name##_key_t *ret_key, \
						   name##_value_t *ret_value) \
	{								\
		return _bplustree_delete(&tree->_impl, __BPLUSTREE_DELETE_MAX, NULL, \
					 ret_key, ret_value, &name##_info); \
	}								\
									\
	static _attr_unused bool name##_insert(struct name *tree, name##_key_t key, name##_value_t value) \
	{								\
		return _bplustree_insert(&tree->_impl, &key, &value, false, &name##_info); \
	}								\
									\
	static _attr_unused bool name##_set(struct name *tree, name##_key_t key, name##_value_t value) \
	{								\
		return _bplustree_insert(&tree->_impl, &key, &value, true, &name##_info); \
	}								\
									\
	static _attr_unused bool name##_insert_sequential(struct name *tree, name##_key_t key, \
							  name##_value_t value) \
	{								\
		return _bplustree_insert_sequential(&tree->_impl, &key, &value, &name##_info); \
	}								\
									\
	typedef struct _bplustree_bulk_load_ctx name##_bulk_load_ctx_t;	\
									\
	static _attr_unused name##_bulk_load_ctx_t name##_bulk_load_start(void) \
	{								\
		name##_bulk_load_ctx_t ctx;				\
		_bplustree_bulk_load_start(&ctx);			\
		return ctx;						\
	}								\
									\
	static _attr_unused void name##_bulk_load_next(name##_bulk_load_ctx_t *ctx, name##_key_t key, \
						       name##_value_t value) \
	{								\
		_bplustree_bulk_load_next(ctx, &key, &value, &name##_info); \
	}								\
									\
	static _attr_unused struct name name##_bulk_load_end(name##_bulk_load_ctx_t *ctx) \
	{								\
		_bplustree_bulk_load_end(ctx, &name##_info);		\
		return (struct name) { ._impl = ctx->tree };		\
	}								\
	_Static_assert(1, "allow semicolon without warning")

_bplustree_kv_t _bplustree_iter_start(struct _bplustree_iter *iter, const struct _bplustree *tree,
				      bool rightmost, const struct bplustree_info *info);
_bplustree_kv_t _bplustree_iter_next(struct _bplustree_iter *iter, const struct bplustree_info *info);
_bplustree_kv_t _bplustree_iter_prev(struct _bplustree_iter *iter, const struct bplustree_info *info);
_bplustree_kv_t _bplustree_iter_start_at(struct _bplustree_iter *iter, const struct _bplustree *tree,
					 void *key, enum bplustree_iter_start_at_mode mode,
					 const struct bplustree_info *info);
void _bplustree_init(struct _bplustree *tree);
void _bplustree_destroy(struct _bplustree *tree, const struct bplustree_info *info);
_bplustree_kv_t _bplustree_find(const struct _bplustree *tree, const void *key,
				const struct bplustree_info *info);
_bplustree_kv_t _bplustree_get_leftmost_rightmost(const struct _bplustree *tree, bool leftmost,
						  const struct bplustree_info *info);
bool _bplustree_delete(struct _bplustree *tree, enum _bplustree_deletion_mode mode, const void *key,
		       void *ret_key, void *ret_value, const struct bplustree_info *info);
bool _bplustree_insert(struct _bplustree *tree, void *key, void *value, bool update,
		       const struct bplustree_info *info);
bool _bplustree_insert_sequential(struct _bplustree *tree, void *key, void *value,
				  const struct bplustree_info *info);
void _bplustree_bulk_load_start(struct _bplustree_bulk_load_ctx *ctx);
void _bplustree_bulk_load_next(struct _bplustree_bulk_load_ctx *ctx, void *key, void *value,
			       const struct bplustree_info *info);
void _bplustree_bulk_load_end(struct _bplustree_bulk_load_ctx *ctx, const struct bplustree_info *info);

#ifdef __ADLIB_TESTS__
void *_bplustree_debug_node_key(struct _bplustree_node *node, unsigned int idx,
				const struct bplustree_info *info);
void *_bplustree_debug_node_value(struct _bplustree_node *node, unsigned int idx,
				  const struct bplustree_info *info);
struct _bplustree_node *_bplustree_debug_node_get_child(struct _bplustree_node *node, unsigned int idx,
							const struct bplustree_info *info);
struct _bplustree _bplustree_debug_copy(const struct _bplustree *tree, const struct bplustree_info *info);
size_t _bplustree_debug_node_size(bool leaf, bool root, const struct bplustree_info *info);
#endif
