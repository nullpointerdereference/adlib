/*
 * Copyright (C) 2025 Fabian Hügel
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include "fortify.h"
#include "hashtable.h"

// TODO try setting the hash function per instance (performance?)

#define DEFINE_HASHMAP(name, key_type, value_type, key_destructor, value_destructor, hash_func, ...) \
									\
	typedef key_type _##name##_key_t;				\
	typedef const key_type _##name##_const_key_t;			\
	typedef value_type _##name##_value_t;				\
									\
	struct _##name##_entry {					\
		_##name##_key_t key;					\
		_##name##_value_t value;				\
	};								\
									\
	typedef void (*name##_key_destructor)(_##name##_key_t key);	\
	typedef void (*name##_value_destructor)(_##name##_value_t value); \
									\
	static name##_key_destructor _##name##_destroy_key = (key_destructor); \
	static name##_value_destructor _##name##_destroy_value = (value_destructor); \
									\
	static inline bool _##name##_keys_match(const _##name##_const_key_t a, const _##name##_const_key_t b) \
	{								\
		return (__VA_ARGS__);					\
	}								\
									\
	DEFINE_HASHTABLE(_##name##_impl, _##name##_const_key_t, struct _##name##_entry, 8, _##name##_keys_match(key, entry.key)); \
									\
	struct name {							\
		struct _##name##_impl _impl;				\
		size_t _generation;					\
	};								\
	typedef _##name##_impl_hash_t name##_hash_t;			\
	typedef _##name##_impl_uint_t name##_uint_t;			\
									\
	static inline name##_hash_t _##name##_hash(_##name##_const_key_t key) \
	{								\
		return hash_func(key);					\
	}								\
									\
	static _attr_unused void name##_init_with_capacity(struct name *map, name##_uint_t initial_capacity) \
	{								\
		_##name##_impl_init(&map->_impl, initial_capacity);	\
		map->_generation = 0;					\
	}								\
									\
	static _attr_unused void name##_init(struct name *map)		\
	{								\
		name##_init_with_capacity(map, 0);			\
	}								\
									\
	static void _##name##_destroy_entries(struct name *map)		\
	{								\
		if ((!_##name##_destroy_key && !_##name##_destroy_value) || \
		    _##name##_impl_num_entries(&map->_impl) == 0) {	\
			return;						\
		}							\
		for (_##name##_impl_iter_t iter = _##name##_impl_iter_start(&map->_impl); \
		     _##name##_impl_iter_advance(&iter);) {		\
			struct _##name##_entry *entry = _##name##_impl_iter_entry(&iter); \
			if (_##name##_destroy_key) {			\
				_##name##_destroy_key(entry->key);	\
			}						\
			if (_##name##_destroy_value) {			\
				_##name##_destroy_value(entry->value); \
			}						\
		}							\
	}								\
									\
	static _attr_unused void name##_destroy(struct name *map)	\
	{								\
		_##name##_destroy_entries(map);				\
		_##name##_impl_destroy(&map->_impl);			\
		map->_generation++;					\
	}								\
									\
	static _attr_unused void name##_clear(struct name *map)		\
	{								\
		_##name##_destroy_entries(map);				\
		_##name##_impl_clear(&map->_impl);			\
		map->_generation++;					\
	}								\
									\
	static _attr_unused void name##_resize(struct name *map, name##_uint_t new_capacity) \
	{								\
		_##name##_impl_resize(&map->_impl, new_capacity);	\
		map->_generation++;					\
	}								\
									\
	static _attr_unused name##_uint_t name##_capacity(const struct name *map) \
	{								\
		return _##name##_impl_capacity(&map->_impl);		\
	}								\
									\
	static _attr_unused name##_uint_t name##_num_entries(const struct name *map) \
	{								\
		return _##name##_impl_num_entries(&map->_impl);		\
	}								\
	/*								\
	struct name##_bucket {						\
		struct _##name##_impl_bucket _impl;			\
		const struct name *_map;				\
		size_t _generation;					\
	};								\
									\
	static _attr_unused _##name##_const_key_t name##_bucket_key(const struct name##_bucket *bucket) \
	{								\
		_fortify_check(bucket->_map->_generation == bucket->_generation); \
		return _##name##_impl_bucket_entry(&bucket->_impl)->key; \
	}								\
									\
	static _attr_unused _##name##_value_t *name##_bucket_value(const struct name##_bucket *bucket) \
	{								\
		_fortify_check(bucket->_map->_generation == bucket->_generation); \
		return &_##name##_impl_bucket_entry(&bucket->_impl)->value; \
	}								\
									\
	static _attr_unused bool name##_lookup_bucket(const struct name *map, _##name##_const_key_t key, \
						      struct name##_bucket *bucket) \
	{								\
		bucket->_map = map;					\
		bucket->_generation = map->_generation;			\
		return _##name##_impl_lookup_bucket(&map->_impl, key, _##name##_hash(key), &bucket->_impl); \
	}								\
									\
	static _attr_unused bool name##_lookup_bucket_for_insertion(struct name *map, \
								    _##name##_const_key_t key, \
								    struct name##_bucket *bucket) \
	{								\
		bucket->_map = map;					\
		bucket->_generation = map->_generation++;		\
		return _##name##_impl_lookup_bucket_for_insertion(&map->_impl, key, _##name##_hash(key), \
								  &bucket->_impl); \
	}								\
									\
	static _attr_unused void name##_bucket_remove_entry(struct name *map, struct name##_bucket *bucket) \
	{								\
		_fortify_check(bucket->_map->_generation == bucket->_generation); \
		_##name##_impl_bucket_remove_entry(&map->_impl, &bucket->_impl); \
		map->_generation++;					\
	}								\
	*/								\
	static _attr_unused _##name##_value_t *name##_lookup(const struct name *map, _##name##_const_key_t key) \
	{								\
		struct _##name##_impl_bucket bucket;			\
		bool found = _##name##_impl_lookup_bucket(&map->_impl, key, _##name##_hash(key), &bucket); \
		return found ? &_##name##_impl_bucket_entry(&bucket)->value : NULL; \
	}								\
									\
	struct name##_insert_result {					\
		_##name##_value_t *value;				\
		bool inserted_new_entry;				\
	};								\
									\
	static _attr_unused struct name##_insert_result name##_insert(struct name *map, _##name##_key_t key) \
	{								\
		struct _##name##_impl_bucket bucket;			\
		bool inserted = _##name##_impl_lookup_bucket_for_insertion(&map->_impl, key, \
									   _##name##_hash(key), &bucket); \
		struct _##name##_entry *entry = _##name##_impl_bucket_entry(&bucket); \
		if (inserted) {						\
			entry->key = key;				\
		}							\
		map->_generation++;					\
		return (struct name##_insert_result) {			\
			.value = &entry->value,				\
			.inserted_new_entry = inserted,			\
		};							\
	}								\
									\
	static _attr_unused _##name##_value_t *name##_insert_unchecked(struct name *map, _##name##_key_t key) \
	{								\
		struct _##name##_impl_bucket bucket;			\
		_##name##_impl_insert_bucket_unchecked(&map->_impl, _##name##_hash(key), &bucket); \
		struct _##name##_entry *entry = _##name##_impl_bucket_entry(&bucket); \
		entry->key = key;					\
		map->_generation++;					\
		return &entry->value;					\
	}								\
									\
	static _attr_unused bool name##_remove(struct name *map, _##name##_const_key_t key, \
					       _##name##_key_t *ret_key, \
					       _##name##_value_t *ret_value) \
	{								\
		struct _##name##_impl_bucket bucket;			\
		if (!_##name##_impl_lookup_bucket(&map->_impl, key, _##name##_hash(key), &bucket)) { \
			return false;					\
		}							\
		struct _##name##_entry *entry = _##name##_impl_bucket_entry(&bucket); \
		if (ret_key) {						\
			*ret_key = entry->key;				\
		} else if (_##name##_destroy_key) {			\
			_##name##_destroy_key(entry->key);		\
		}							\
		if (ret_value) {					\
			*ret_value = entry->value;			\
		} else if (_##name##_destroy_value) {			\
			_##name##_destroy_value(entry->value);		\
		}							\
		_##name##_impl_bucket_remove_entry(&map->_impl, &bucket); \
		map->_generation++;					\
		return true;						\
	}								\
									\
	typedef struct name##_iterator {				\
		struct _##name##_impl_iterator _impl;			\
		const struct name *_map;				\
		size_t _generation;					\
	} name##_iter_t;						\
									\
	static _attr_unused struct name##_iterator name##_iter_start(const struct name *map) \
	{								\
		return (struct name##_iterator) {			\
			._impl = _##name##_impl_iter_start(&map->_impl), \
			._map = map,					\
			._generation = map->_generation,		\
		};							\
	}								\
									\
	static _attr_unused bool name##_iter_advance(struct name##_iterator *iter) \
	{								\
		_fortify_check(iter->_map->_generation == iter->_generation); \
		return _##name##_impl_iter_advance(&iter->_impl);	\
	}								\
									\
	static _attr_unused const _##name##_const_key_t *name##_iter_key(struct name##_iterator *iter) \
	{								\
		_fortify_check(iter->_map->_generation == iter->_generation); \
		return (const _##name##_const_key_t *)&_##name##_impl_iter_entry(&iter->_impl)->key; \
	}								\
									\
	static _attr_unused _##name##_value_t *name##_iter_value(struct name##_iterator *iter) \
	{								\
		_fortify_check(iter->_map->_generation == iter->_generation); \
		return &_##name##_impl_iter_entry(&iter->_impl)->value;	\
	}								\
	_Static_assert(1, "allow semicolon without warning")
