/*
 * Copyright (C) 2024 Fabian Hügel
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include "compiler.h"

// #define __HASHTABLE_PROFILING

// TODO documentation (see tests for now)
// TODO split off type and function declarations for headers

#define DEFINE_HASHTABLE(name, key_type, entry_type, THRESHOLD, ...)	\
									\
	typedef key_type _##name##_key_t;				\
	typedef entry_type _##name##_entry_t;				\
									\
	struct name {							\
		struct _hashtable _impl;				\
	};								\
									\
	static bool _##name##_keys_match(const void * _key, const void * _entry) \
	{								\
		const _##name##_key_t key = *(const _##name##_key_t *)_key;		\
		const _##name##_entry_t entry = *(const _##name##_entry_t *)_entry;	\
		return (__VA_ARGS__);					\
	}								\
									\
	_Static_assert(5 <= (THRESHOLD) && (THRESHOLD) <= 9,		\
		       "resize threshold (max load factor) must be an integer in the range of 5 to 9 (50%-90%)"); \
									\
	typedef _hashtable_hash_t name##_hash_t;			\
	typedef _hashtable_uint_t name##_uint_t;			\
									\
	static _Alignas(16) const struct _hashtable_info _##name##_info = { \
		.entry_size = sizeof(_##name##_entry_t),		\
		.threshold = (THRESHOLD),				\
		.keys_match = _##name##_keys_match,			\
	};								\
									\
	static void name##_init(struct name *table, name##_uint_t initial_capacity) \
	{								\
		_hashtable_init(&table->_impl, initial_capacity, &_##name##_info); \
	}								\
									\
	static _attr_unused void name##_destroy(struct name *table)	\
	{								\
		_hashtable_destroy(&table->_impl);			\
	}								\
									\
	static _attr_unused void name##_clear(struct name *table)	\
	{								\
		_hashtable_clear(&table->_impl, &_##name##_info);	\
	}								\
									\
	static _attr_unused void name##_resize(struct name *table, name##_uint_t new_capacity) \
	{								\
		_hashtable_resize(&table->_impl, new_capacity, &_##name##_info); \
	}								\
									\
	static _attr_unused name##_uint_t name##_capacity(const struct name *table) \
	{								\
		return table->_impl.capacity;				\
	}								\
									\
	static _attr_unused name##_uint_t name##_num_entries(const struct name *table) \
	{								\
		return table->_impl.num_entries;			\
	}								\
									\
	typedef struct name##_iterator {				\
		_##name##_entry_t *_entry;				\
		_hashtable_idx_t _index;				\
		const struct name *_table;				\
		size_t _num_entries_visited;				\
	} name##_iter_t;						\
									\
	static _attr_unused bool name##_iter_advance(struct name##_iterator *iter) \
	{								\
		if (iter->_num_entries_visited == iter->_table->_impl.num_entries) { \
			return false;					\
		}							\
		iter->_num_entries_visited++;				\
		iter->_index = _hashtable_get_next(&iter->_table->_impl, iter->_index, &_##name##_info); \
		iter->_entry = _hashtable_entry(&iter->_table->_impl, iter->_index, &_##name##_info); \
		iter->_index++;						\
		return true;						\
	}								\
									\
	static _attr_unused _##name##_entry_t *name##_iter_entry(const struct name##_iterator *iter) \
	{								\
		return iter->_entry;					\
	}								\
									\
	static _attr_unused struct name##_iterator name##_iter_start(const struct name *table) \
	{								\
		return (struct name##_iterator) { ._table = table };	\
	}								\
									\
	struct name##_bucket {						\
		struct _hashtable_bucket _impl;				\
	};								\
									\
	static _attr_unused bool name##_lookup_bucket(const struct name *table, _##name##_key_t key, \
						      name##_hash_t hash, struct name##_bucket *bucket) \
	{								\
		return _hashtable_lookup_bucket(&table->_impl, &key, hash, &bucket->_impl, &_##name##_info); \
	}								\
									\
	static _attr_unused bool name##_lookup_bucket_for_insertion(struct name *table, _##name##_key_t key, \
								    name##_hash_t hash,\
								    struct name##_bucket *bucket) \
	{								\
		return _hashtable_lookup_bucket_for_insertion(&table->_impl, &key, hash, &bucket->_impl, \
							      &_##name##_info);	\
	}								\
									\
	static _attr_unused void name##_insert_bucket_unchecked(struct name *table, name##_hash_t hash, \
								struct name##_bucket *bucket) \
	{								\
		_hashtable_insert_bucket_unchecked(&table->_impl, hash, &bucket->_impl, &_##name##_info); \
	}								\
									\
	static _attr_unused void name##_bucket_remove_entry(struct name *table, struct name##_bucket *bucket) \
	{								\
		_hashtable_bucket_remove_entry(&table->_impl, &bucket->_impl, &_##name##_info); \
	}								\
									\
	static _attr_unused _##name##_entry_t *name##_bucket_entry(const struct name##_bucket *bucket) \
	{								\
		return bucket->_impl.entry;				\
	}								\
	_Static_assert(1, "allow semicolon without warning")



// private API

typedef uint32_t _hashtable_hash_t;
typedef uint32_t _hashtable_uint_t;
typedef _hashtable_uint_t _hashtable_idx_t;

struct _hashtable_info {
	_hashtable_uint_t entry_size;
	_hashtable_uint_t threshold;
	bool (*keys_match)(const void *key, const void *entry);
};

struct _hashtable {
	_hashtable_uint_t num_entries;
	_hashtable_uint_t num_tombstones;
	_hashtable_uint_t max_entries;
	_hashtable_uint_t capacity;
	unsigned char *storage;
	struct _hashtable_metadata *metadata;
};

struct _hashtable_bucket {
	struct _hashtable_metadata *metadata;
	void *entry;
};

void _hashtable_init(struct _hashtable *table, _hashtable_uint_t capacity, const struct _hashtable_info *info);
void _hashtable_destroy(struct _hashtable *table);
_hashtable_idx_t _hashtable_get_next(const struct _hashtable *table, _hashtable_idx_t start,
				     const struct _hashtable_info *info) _attr_pure;
void _hashtable_resize(struct _hashtable *table, _hashtable_uint_t new_capacity,
		       const struct _hashtable_info *info);
void _hashtable_clear(struct _hashtable *table, const struct _hashtable_info *info);

bool _hashtable_lookup_bucket(const struct _hashtable *table, const void *key, _hashtable_hash_t hash,
			      struct _hashtable_bucket *bucket, const struct _hashtable_info *info);
bool _hashtable_lookup_bucket_for_insertion(struct _hashtable *table, const void *key, _hashtable_hash_t hash,
					    struct _hashtable_bucket *bucket,
					    const struct _hashtable_info *info);
void _hashtable_insert_bucket_unchecked(struct _hashtable *table, _hashtable_hash_t hash,
					struct _hashtable_bucket *bucket,
					const struct _hashtable_info *info);
void _hashtable_bucket_remove_entry(struct _hashtable *table, struct _hashtable_bucket *bucket,
				    const struct _hashtable_info *info);

static inline void *_hashtable_entry(const struct _hashtable *table, _hashtable_idx_t index,
			      const struct _hashtable_info *info)
{
	return table->storage + index * info->entry_size;
}
