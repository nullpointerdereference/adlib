/*
 * Copyright (C) 2025 Fabian Hügel
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include "fortify.h"
#include "hashtable.h"

#define DEFINE_HASHSET(name, key_type, key_destructor, hash_func, ...)	\
									\
	typedef key_type _##name##_key_t;				\
	typedef const key_type _##name##_const_key_t;			\
									\
	struct _##name##_entry {					\
		_##name##_key_t key;					\
	};								\
									\
	typedef void (*name##_key_destructor)(_##name##_key_t key);	\
									\
	static name##_key_destructor _##name##_destroy_key = (key_destructor); \
									\
	static inline bool _##name##_keys_match(const _##name##_const_key_t a, const _##name##_const_key_t b) \
	{								\
		return (__VA_ARGS__);					\
	}								\
									\
	DEFINE_HASHTABLE(_##name##_impl, _##name##_const_key_t, struct _##name##_entry, 8, _##name##_keys_match(key, entry.key)); \
									\
	struct name {							\
		struct _##name##_impl _impl;				\
		size_t _generation;					\
	};								\
	typedef _##name##_impl_hash_t name##_hash_t;			\
	typedef _##name##_impl_uint_t name##_uint_t;			\
									\
	static inline name##_hash_t _##name##_hash(_##name##_const_key_t key) \
	{								\
		return hash_func(key);					\
	}								\
									\
	static _attr_unused void name##_init_with_capacity(struct name *set, name##_uint_t initial_capacity) \
	{								\
		_##name##_impl_init(&set->_impl, initial_capacity);	\
		set->_generation = 0;					\
	}								\
									\
	static _attr_unused void name##_init(struct name *set)		\
	{								\
		name##_init_with_capacity(set, 0);			\
	}								\
									\
	static void _##name##_destroy_entries(struct name *set)		\
	{								\
		if (!_##name##_destroy_key || _##name##_impl_num_entries(&set->_impl) == 0) { \
			return;						\
		}							\
		for (_##name##_impl_iter_t iter = _##name##_impl_iter_start(&set->_impl); \
		     _##name##_impl_iter_advance(&iter);) {		\
			_##name##_destroy_key(_##name##_impl_iter_entry(&iter)->key); \
		}							\
	}								\
									\
	static _attr_unused void name##_destroy(struct name *set)	\
	{								\
		_##name##_destroy_entries(set);				\
		_##name##_impl_destroy(&set->_impl);			\
		set->_generation++;					\
	}								\
									\
	static _attr_unused void name##_clear(struct name *set)		\
	{								\
		_##name##_destroy_entries(set);				\
		_##name##_impl_clear(&set->_impl);			\
		set->_generation++;					\
	}								\
									\
	static _attr_unused void name##_resize(struct name *set, name##_uint_t new_capacity) \
	{								\
		_##name##_impl_resize(&set->_impl, new_capacity);	\
		set->_generation++;					\
	}								\
									\
	static _attr_unused name##_uint_t name##_capacity(const struct name *set) \
	{								\
		return _##name##_impl_capacity(&set->_impl);		\
	}								\
									\
	static _attr_unused name##_uint_t name##_num_entries(const struct name *set) \
	{								\
		return _##name##_impl_num_entries(&set->_impl);		\
	}								\
	/*								\
	struct name##_bucket {						\
		struct _##name##_impl_bucket _impl;			\
		const struct name *_set;				\
		size_t _generation;					\
	};								\
									\
	static _attr_unused _##name##_const_key_t name##_bucket_key(const struct name##_bucket *bucket) \
	{								\
		_fortify_check(bucket->_set->_generation == bucket->_generation); \
		return _##name##_impl_bucket_entry(&bucket->_impl)->key; \
	}								\
									\
	static _attr_unused bool name##_lookup_bucket(const struct name *set, _##name##_const_key_t key, \
						      struct name##_bucket *bucket) \
	{								\
		bucket->_set = set;					\
		bucket->_generation = set->_generation;			\
		return _##name##_impl_lookup_bucket(&set->_impl, key, _##name##_hash(key), &bucket->_impl); \
	}								\
									\
	static _attr_unused bool name##_lookup_bucket_for_insertion(struct name *set, \
								    _##name##_const_key_t key, \
								    struct name##_bucket *bucket) \
	{								\
		bucket->_set = set;					\
		bucket->_generation = set->_generation++;		\
		return _##name##_impl_lookup_bucket_for_insertion(&set->_impl, key, _##name##_hash(key), \
								  &bucket->_impl); \
	}								\
									\
	static _attr_unused void name##_bucket_remove_entry(struct name *set, struct name##_bucket *bucket) \
	{								\
		_fortify_check(bucket->_set->_generation == bucket->_generation); \
		_##name##_impl_bucket_remove_entry(&set->_impl, &bucket->_impl); \
		set->_generation++;					\
	}								\
	*/								\
	static _attr_unused const _##name##_const_key_t *name##_lookup(const struct name *set, \
								       _##name##_const_key_t key) \
	{								\
		struct _##name##_impl_bucket bucket;			\
		bool found = _##name##_impl_lookup_bucket(&set->_impl, key, _##name##_hash(key), &bucket); \
		return found ? (const _##name##_const_key_t *)&_##name##_impl_bucket_entry(&bucket)->key : NULL; \
	}								\
									\
	static _attr_unused bool name##_insert(struct name *set, _##name##_key_t key) \
	{								\
		struct _##name##_impl_bucket bucket;			\
		bool new_entry = _##name##_impl_lookup_bucket_for_insertion(&set->_impl, key, \
									    _##name##_hash(key), &bucket); \
		if (new_entry) {					\
			struct _##name##_entry *entry = _##name##_impl_bucket_entry(&bucket); \
			entry->key = key;				\
		}							\
		set->_generation++;					\
		return new_entry;					\
	}								\
									\
	static _attr_unused const _##name##_const_key_t *name##_insert_unchecked(struct name *set, \
										 _##name##_key_t key) \
	{								\
		struct _##name##_impl_bucket bucket;			\
		_##name##_impl_insert_bucket_unchecked(&set->_impl, _##name##_hash(key), &bucket); \
		struct _##name##_entry *entry = _##name##_impl_bucket_entry(&bucket); \
		entry->key = key;					\
		set->_generation++;					\
		return (const _##name##_const_key_t *)&entry->key;	\
	}								\
									\
	static _attr_unused bool name##_remove(struct name *set, _##name##_const_key_t key, \
					       _##name##_key_t *ret_key) \
	{								\
		struct _##name##_impl_bucket bucket;			\
		if (!_##name##_impl_lookup_bucket(&set->_impl, key, _##name##_hash(key), &bucket)) { \
			return false;					\
		}							\
		struct _##name##_entry *entry = _##name##_impl_bucket_entry(&bucket); \
		if (ret_key) {						\
			*ret_key = entry->key;				\
		} else if (_##name##_destroy_key) {			\
			_##name##_destroy_key(entry->key);		\
		}							\
		_##name##_impl_bucket_remove_entry(&set->_impl, &bucket); \
		set->_generation++;					\
		return true;						\
	}								\
									\
	typedef struct name##_iterator {				\
		struct _##name##_impl_iterator _impl;			\
		const struct name *_set;				\
		size_t _generation;					\
	} name##_iter_t;						\
									\
	static _attr_unused struct name##_iterator name##_iter_start(const struct name *set) \
	{								\
		return (struct name##_iterator) {			\
			._impl = _##name##_impl_iter_start(&set->_impl), \
			._set = set,					\
			._generation = set->_generation,		\
		};							\
	}								\
									\
	static _attr_unused bool name##_iter_advance(struct name##_iterator *iter) \
	{								\
		_fortify_check(iter->_set->_generation == iter->_generation); \
		return _##name##_impl_iter_advance(&iter->_impl);	\
	}								\
									\
	static _attr_unused const _##name##_const_key_t *name##_iter_key(struct name##_iterator *iter) \
	{								\
		_fortify_check(iter->_set->_generation == iter->_generation); \
		return (const _##name##_const_key_t *)&_##name##_impl_iter_entry(&iter->_impl)->key; \
	}								\
	_Static_assert(1, "allow semicolon without warning")
